///////////////
// VARIABLES //
///////////////

/**
 * Scripts
 * @type {Array}
 */
var scriptGroups = [
  {
    filename: 'inject',
    label: 'inject',
    files: [
      'lib/js/inject/lib/core-animations.js',
      'lib/js/inject/lib/core-dom.js',
      'lib/js/inject/lib/core-data.js',
      'lib/js/inject/app/core.js',
      'lib/js/inject/app/ui.js',
      'lib/js/inject/app/modnote.js',
      'lib/js/inject/app/realmeye-messages.js',
      'lib/js/inject/app/latest-topics.js',
      'lib/js/inject/inject.js',
    ],
    output: 'src/inject'
  },

  {
    filename: 'background',
    label: 'background',
    files: [
      'lib/js/bg/background.js',
    ],
    output: 'src/bg'
  }
];

/**
 * Styles
 * @type {Array}
 */
var styleGroups = [
  {
    label:      'inject',
    input:      'lib/scss/inject/*.scss',
    input_path: 'lib/scss/inject',
    output:     'src/inject'
  }
];



//////////////
// INCLUDES //
//////////////

// Include core
var gulp       = require('gulp');

// Include plugins
var babel       = require("gulp-babel");
var concat      = require('gulp-concat');
var file        = require('gulp-file');
var gutil       = require('gulp-util');
var please      = require('gulp-pleeease');
var rename      = require('gulp-rename');
var sourcemaps  = require('gulp-sourcemaps');



/////////////
// SCRIPTS //
/////////////

// Minify and compile JS
gulp.task('js', function(){

  gutil.log(gutil.colors.cyan('[Mod Tools]') + ' ' + gutil.colors.green('Minifying JS'));

  // Loop scriptGroup
  scriptGroups.forEach(function(group){

    gutil.log(gutil.colors.cyan('[Mod Tools]') + ' ' + gutil.colors.white('Processing ' + group.label + ' script group'));

    // Development
    gulp.src(
      group.files,
      {
        base: 'lib/js'
      }
    )
    .pipe(sourcemaps.init())
    .on('error', function(e){ return gutil.log(e.message); })
    .pipe(babel({
      'presets': [
        'es2015'
      ],
      'babelrc': false
    }))
    .pipe(concat(group.filename + '.dev.js'))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(group.output));

    // Production
    gulp.src(
      group.files,
      {
        base: 'lib/js'
      }
    )
    .pipe(concat(group.filename + '.js'))
    .on('error', function(e){ return gutil.log(e.message); })
    .pipe(babel({
      'presets': [
        'es2015'
      ],
      'plugins': [
        'transform-remove-console'
      ],
      'babelrc': false,
      'minified': true,
      'comments': false
    }))
    .pipe(gulp.dest(group.output));

  });

});



////////////
// STYLES //
////////////

// Pre and post process with Pleeease
gulp.task('css', function(){

  gutil.log(gutil.colors.cyan('[Mod Tools]') + ' ' + gutil.colors.green('Compiling SCSS'));

  // Loop styleGroups
  styleGroups.forEach(function(group){

    gutil.log(gutil.colors.cyan('[Mod Tools]') + ' ' + gutil.colors.white('Processing ' + group.label + ' style group'));

    // Development
    gulp.src(group.input)
    .pipe(sourcemaps.init())
    .pipe(please({
      autoprefixer:{
        browsers: [
          "last 3 versions",
          "Android 2.3"
        ],
        cascade: true
      },
      mqpacker: true,
      minifier: false,
      sass: {
        includePaths: [group.input_path]
      }
    }))
    .on('error', function(e){ gutil.log(e.message); })
    .pipe(rename({
      suffix: '.dev',
      extname: '.css'
    }))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(group.output));

    // Production
    gulp.src(group.input)
    .pipe(please({
      autoprefixer:{
        browsers: [
          "last 3 versions",
          "Android 2.3"
        ],
        cascade: true
      },
      mqpacker: true,
      minifier: true,
      sass: {
        includePaths: [group.input_path]
      }
    }))
    .on('error', function(e){ gutil.log(e.message); })
    .pipe(rename({
      extname: '.css'
    }))
    .pipe(gulp.dest(group.output));

  });

});



////////////////
// CORE TASKS //
////////////////

// Watch for changes
gulp.task('watch', function() {
  gutil.log(gutil.colors.cyan('[Mod Tools]') + ' ' + gutil.colors.green('Started watch'));

  gulp.watch('lib/js/*.js', ['js']);
  gulp.watch('lib/js/**/*.js', ['js']);
  gulp.watch('lib/scss/**/*.scss', ['css']);
  gulp.watch('lib/scss/*.scss', ['css']);
});

// Compile Task
gulp.task(
  'compile',
  [
    'css',
    'js'
  ]
);

// Default Task
gulp.task(
  'default',
  [
    'css',
    'js',
    'watch'
  ]
);