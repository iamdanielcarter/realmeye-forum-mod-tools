'use strict';

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj; };

///////////////////////
// CORE - ANIMATIONS //
///////////////////////


/**
 * Linear progression
 * @param  {number} t Time elapsed
 * @param  {number} b Starting value
 * @param  {number} c Start end difference
 * @param  {number} d Duration
 * @return {number}   Delta updated value
 */
Math.linearTween = function (t, b, c, d) {
  return c * t / d + b;
};

/**
 * Cubic ease-in progression
 * @param  {number} t Time elapsed
 * @param  {number} b Starting value
 * @param  {number} c Start end difference
 * @param  {number} d Duration
 * @return {number}   Delta updated value
 */
Math.easeIn = function (t, b, c, d) {
  t /= d;
  return c * t * t * t + b;
};

/**
 * Cubic ease-out progression
 * @param  {number} t Time elapsed
 * @param  {number} b Starting value
 * @param  {number} c Start end difference
 * @param  {number} d Duration
 * @return {number}   Delta updated value
 */
Math.easeOut = function (t, b, c, d) {
  t /= d;
  t--;
  return c * (t * t * t + 1) + b;
};

/**
 * Cubic ease-in-out progression
 * @param  {number} t Time elapsed
 * @param  {number} b Starting value
 * @param  {number} c Start end difference
 * @param  {number} d Duration
 * @return {number}   Delta updated value
 */
Math.easeInOut = function (t, b, c, d) {
  t /= d / 2;
  if (t < 1) return c / 2 * t * t * t + b;
  t -= 2;
  return c / 2 * (t * t * t + 2) + b;
};

/**
 * Fade in/out with duration and callback settings
 * @param  {string}   direction in|out
 * @param  {object}   element   DOM node
 * @param  {number}   duration  Animation in ms
 * @param  {string}   easing    Easing timing function linear|easeIn|easeOut|easeInOut
 * @param  {function} fn        Callback (with element as scope)
 * @return {boolean}
 */
function _fade(direction, element, duration, easing, fn) {
  var FROM,
      TO,
      START = new Date().getTime(),
      DIFF,
      _modShift,
      callback;

  // Prevent propagation
  if (element.getAttribute('data-fade')) {
    return false;
  }
  element.setAttribute('data-fade', true);

  // Easing timing function
  switch (easing) {
    case 'linear':
      easing = Math.linearTween;
      break;

    case 'easeIn':
      easing = Math.easeIn;
      break;

    case 'easeInOut':
      easing = Math.easeInOut;
      break;

    default:
      // easeOut
      easing = Math.easeOut;
      break;
  }

  // Set default opacity
  if (!element.style.opacity) {
    element.style.opacity = direction == 'in' ? 0 : 1;
  }

  // Directional defaults
  FROM = 1;
  TO = 0;
  if (direction != 'out') {
    direction = 'in';
    element.style.display = 'block';
    FROM = 0;
    TO = 1;
  }
  DIFF = TO - FROM;

  // Duration
  if (typeof duration !== 'number') {
    duration = 300;
  }
  duration = Math.abs(duration);

  // Callback
  callback = function callback() {
    element.removeAttribute('data-fade');
    if (typeof fn === 'function') {
      fn.call(element);
    }
  };

  // Modifier
  _modShift = function modShift() {
    // Update values
    var CURRENT = new Date().getTime() - START,
        UPDATE = easing(CURRENT, FROM, DIFF, duration);
    element.style.opacity = UPDATE;

    if (UPDATE == TO || CURRENT >= duration) {
      // Done
      if (direction == 'out') {
        // Hide on fade out
        element.style.display = 'none';
      }

      // Reset transitional CSS
      element.style.opacity = '';

      // End
      callback();
      return true;
    }
    requestAnimationFrame(_modShift);
  };

  // Trigger
  _modShift();
}

/**
 * Slide up/down with duration and callback settings
 * @param  {string}   direction up|down
 * @param  {object}   element   DOM node
 * @param  {number}   duration  Animation in ms
 * @param  {string}   easing    Easing timing function linear|easeIn|easeOut|easeInOut
 * @param  {function} fn        Callback (with element as scope)
 * @return {boolean}
 */
function _slide(direction, element, duration, easing, display_remove, fn) {
  var FROM,
      TO,
      START = new Date().getTime(),
      DIFF,
      _modShift2,
      callback;

  // Prevent propagation
  if (element.getAttribute('data-slide')) {
    return false;
  }
  element.setAttribute('data-slide', true);

  // Set initial css
  if (display_remove) {
    element.style.display = 'block';
  }
  element.style.overflow = 'hidden';
  element.style.height = direction == 'up' ? element.scrollHeight + 'px' : 0 + 'px';

  // Directional defaults
  FROM = element.scrollHeight;
  TO = 0;
  if (direction != 'up') {
    direction = 'down';
    FROM = 0;
    TO = element.scrollHeight;
  }
  DIFF = TO - FROM;

  // Duration
  if (typeof duration !== 'number') {
    duration = 300;
  }
  duration = Math.abs(duration);

  // Easing timing function
  switch (easing) {
    case 'linear':
      easing = Math.linearTween;
      break;

    case 'easeIn':
      easing = Math.easeIn;
      break;

    case 'easeInOut':
      easing = Math.easeInOut;
      break;

    default:
      // easeOut
      easing = Math.easeOut;
      break;
  }

  // Callback
  callback = function callback() {
    element.removeAttribute('data-slide');
    if (typeof fn === 'function') {
      fn.call(element);
    }
  };

  // Modifier
  _modShift2 = function modShift() {
    // Update values
    var CURRENT = new Date().getTime() - START,
        UPDATE = easing(CURRENT, FROM, DIFF, duration);
    element.style.height = UPDATE + 'px';

    if (UPDATE == TO || CURRENT >= duration) {
      // Done
      if (direction == 'up') {
        // Hide on slide up
        if (display_remove) {
          element.style.display = 'none';
        }
      }

      // Reset transitional CSS
      element.style.overflow = '';
      if (display_remove) {
        element.style.height = '';
      }

      // End
      callback();
      return true;
    }
    requestAnimationFrame(_modShift2);
  };

  // Trigger
  _modShift2();
}

/**
 * Scroll window to destination with duration and callback settings
 * @param  {mixed}    destination Scroll to pixel value, selector or DOM node
 * @param  {number}   duration    Animation in ms
 * @param  {string}   easing      Easing timing function linear|easeIn|easeOut|easeInOut
 * @param  {function} fn          Callback function
 * @return {boolean}
 */
function _scrollTo(destination, duration, easing, fn) {
  var FROM,
      TO,
      START = new Date().getTime(),
      DIFF,
      _modShift3,
      callback,
      element = document.body;

  // Prevent propagation
  if (element.getAttribute('data-scrolling')) {
    return false;
  }
  element.setAttribute('data-scrolling', true);

  // Destination
  switch (typeof destination === 'undefined' ? 'undefined' : _typeof(destination)) {
    case 'number':
      destination = destination;
      break;

    case 'string':
      var elem = document.querySelector(destination);
      if (!elem) {
        return false;
      }
      destination = elem.getBoundingClientRect().top;
      break;

    case 'object':
      if (!destination.nodeType) {
        return false;
      }
      destination = destination.getBoundingClientRect().top;
      break;

    default:
      return false;
  }

  // Process below max fold
  var _body = document.body,
      _html = document.documentElement,
      _height = Math.max(_body.scrollHeight, _body.offsetHeight, _html.clientHeight, _html.scrollHeight, _html.offsetHeight);
  destination = Math.max(0, Math.min(destination, _height - window.innerHeight));

  // Directional defaults
  FROM = window.scrollY;
  TO = destination;
  DIFF = TO - FROM;

  // Duration
  if (typeof duration !== 'number') {
    duration = 1000;
  }
  duration = Math.abs(duration);

  // Easing timing function
  switch (easing) {
    case 'linear':
      easing = Math.linearTween;
      break;

    case 'easeIn':
      easing = Math.easeIn;
      break;

    case 'easeInOut':
      easing = Math.easeInOut;
      break;

    default:
      // easeOut
      easing = Math.easeOut;
      break;
  }

  // Callback
  callback = function callback() {
    element.removeAttribute('data-scrolling');
    if (typeof fn === 'function') {
      fn.call(element);
    }
  };

  // Modifier
  _modShift3 = function modShift() {
    // Update values
    var CURRENT = new Date().getTime() - START,
        UPDATE = easing(CURRENT, FROM, DIFF, duration);
    window.scrollTo(window.scrollX, UPDATE);

    if (UPDATE == TO || CURRENT >= duration) {
      // Done
      callback();
      return true;
    }
    requestAnimationFrame(_modShift3);
  };

  // Trigger
  _modShift3();
}
'use strict';

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj; };

////////////////
// CORE - DOM //
////////////////


/**
 * DOM object renderer with event hooks
 * @param  {object} map DOM map tag*|attr|css|html|events|nodes
 * @return {object}     DOM object for append/manipulation
 */
function _render(map) {
  var build = function build(map) {
    var elem, css;

    // Init
    if (!map.tag) {
      return false;
    }
    if (map.ns) {
      // Create element with namespace
      elem = document.createElementNS(map.ns, map.tag);
    } else {
      elem = document.createElement(map.tag);
    }

    // Attributes
    if (_typeof(map.attr) === 'object') {
      for (var attribute in map.attr) {
        elem.setAttribute(attribute, map.attr[attribute]);
      }
    }

    // CSS
    if (_typeof(map.css) === 'object') {
      css = '';
      for (var prop in map.css) {
        css += prop + ': ' + map.css[prop] + '; ';
      }
      elem.setAttribute('style', css);
    }

    // Inner HTML
    if (map.html) {
      elem.innerHTML = map.html;
    }

    // Events
    if (_typeof(map.events) === 'object') {
      map.events.forEach(function (_event) {
        var c = function c(e) {
          _event.listener.call(elem, e);
        };
        elem.addEventListener(_event.type, c);
      });
    }

    // Children
    if (map.nodes) {
      map.nodes.forEach(function (node) {
        var child = build(node);
        elem.appendChild(child);
      });
    }

    return elem;
  };

  // Return rendered element DOM tree
  return build(map);
}
'use strict';

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj; };

/////////////////
// CORE - DATA //
/////////////////


/**
 * Flexible AJAX request with callback
 * @param  {object} options url|data|method|async|load
 */
function _xhr(options) {
  var XHR, PARAMS, callback;

  // Throw invalid
  if ((typeof options === 'undefined' ? 'undefined' : _typeof(options)) !== 'object') {
    return false;
  }
  if (typeof options.url !== 'string') {
    return false;
  }

  // Method
  if (typeof options.method !== 'string') {
    // Set default
    options.method = 'GET';
  } else {
    options.method = options.method.toUpperCase();
  }

  // Initialize
  XHR = new XMLHttpRequest();
  XHR.open(options.method, options.url, options.async);

  // Data object to string
  if (_typeof(options.data) === 'object') {
    for (var key in options.data) {
      PARAMS += '&' + encodeURIComponent(key) + '=' + encodeURIComponent(options.data[key]);
    }
    PARAMS = PARAMS.replace(/^&/, '?');
  }

  // Data and headers
  if (options.method == 'POST') {
    XHR.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    //XHR.setRequestHeader("Connection", "closed");
    //XHR.setRequestHeader("Content-length", PARAMS.length);
  }
  if (options.method == 'GET') {
    options.url += PARAMS ? PARAMS : '';
  }

  // Calls callback if available;
  callback = function callback() {
    if (typeof options.load === 'function') {
      options.load.call(this);
    }
  };

  // Asynchronous
  if (typeof options.async !== 'boolean') {
    // Set default
    options.async = true;
  }

  // Execute
  XHR.onreadystatechange = function () {
    if (XHR.readyState == 4 && XHR.status == 200) {
      callback.call(XHR);
    }
  };
  XHR.send(PARAMS);
}
'use strict';

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj; };

var rmt = {};

rmt.init = function () {
  console.log('rmt init');
  console.log(rmt.data);
  rmt.ui.init();
};

/**
 * Get required data and init on ready
 */
rmt.setup = function () {
  // Data obj
  var apiData = {
    site_data: false,
    user_data: false
  };

  // Get site data
  var site_data = new Promise(function (resolve, reject) {

    _xhr({
      url: '/forum/site.json',
      load: function load() {
        var data = JSON.parse(this.responseText);
        if (this.status === 200 && (typeof data === 'undefined' ? 'undefined' : _typeof(data)) === 'object') {
          resolve(data);
        }
      }
    });
  });

  // Get user data
  var user_data = new Promise(function (resolve, reject) {

    _xhr({
      url: '/forum/session/current.json',
      load: function load() {
        var data = JSON.parse(this.responseText);
        if (this.status === 200 && (typeof data === 'undefined' ? 'undefined' : _typeof(data)) === 'object') {
          resolve(data);
        }
      }
    });
  });

  // Resolve AJAX promises
  site_data.then(function (data) {
    apiData.site_data = data;
    start();
  });
  user_data.then(function (data) {
    apiData.user_data = data;
    start();
  });
  var start = function start() {
    if (apiData.site_data && apiData.user_data) {
      // Set data and init when both data sets are returned
      rmt.data = apiData;
      rmt.init();
    }
  };
};
'use strict';

rmt.ui = {
  elems: {},
  display_state: 1
};

/**
 * Initialize UI
 */
rmt.ui.init = function () {
  console.log('rmt ui init');

  // Render main wrap
  rmt.ui.elems._wrap = document.body.appendChild(_render({
    tag: 'div',
    attr: {
      'id': 'rmt__wrap'
    }
  }));

  // Render toggle
  rmt.ui.elems._toggle_display = rmt.ui.elems._wrap.appendChild(_render({
    tag: 'span',
    attr: {
      'id': 'rmt__display-toggle',
      'title': 'Toggle RealmEye Mod Tools'
    },
    events: [{
      type: 'click',
      listener: rmt.ui.events.toggleDisplay
    }]
  }));

  // Render display wrap
  rmt.ui.elems._display_wrap = rmt.ui.elems._wrap.appendChild(_render({
    tag: 'div',
    attr: {
      'id': 'rmt__display-wrap'
    }
  }));

  // Render message output
  rmt.ui.elems._msg = rmt.ui.elems._display_wrap.appendChild(_render({
    tag: 'div',
    attr: {
      'id': 'rmt__msg'
    },
    css: {
      'display': 'none'
    }
  }));

  // Render opt list
  rmt.ui.elems._opt_list = rmt.ui.elems._display_wrap.appendChild(_render({
    tag: 'ul',
    attr: {
      'id': 'rmt__opt-list'
    }
  }));

  /**
   * Init options
   */
  rmt.ui.initOpts();
};

/**
 * Initialize individual options
 */
rmt.ui.initOpts = function () {
  rmt.modnote.init();
  rmt.re_messages.init();
  rmt.latest_topics.init();
};

/////////////////
// UI - EVENTS //
/////////////////
rmt.ui.events = {};

/**
 * Toggles main wrap display
 * @return {[type]} [description]
 */
rmt.ui.events.toggleDisplay = function () {
  if (rmt.ui.display_state) {
    _slide('up', rmt.ui.elems._display_wrap, 300, 'ease-in-out', true, function () {
      rmt.ui.display_state = !rmt.ui.display_state;
      rmt.ui.elems._toggle_display.className = 'rmt-hidden';
    });
  } else {
    _slide('down', rmt.ui.elems._display_wrap, 300, 'ease-in-out', true, function () {
      rmt.ui.display_state = !rmt.ui.display_state;
      rmt.ui.elems._toggle_display.className = '';
    });
  }
};

//////////////
// MESSAGES //
//////////////
rmt.ui.msg = {};

/**
 * Set UI message
 * @param {string} msg
 * @param {string} type
 */
rmt.ui.msg.set = function (msg, type) {
  rmt.ui.elems._msg.className = 'rmt-msg--' + type;
  rmt.ui.elems._msg.style.display = 'block';
  rmt.ui.elems._msg.innerHTML = msg;
};

/**
 * Remove and reset UI message
 */
rmt.ui.msg.remove = function () {
  rmt.ui.elems._msg.className = '', rmt.ui.elems._msg.style.display = 'none';
  rmt.ui.elems._msg.innerHTML = '';
};
'use strict';

/**
 * Mod Note component
 * @type {Object}
 */
rmt.modnote = {

  /**
   * UI state
   */
  display_state: 0,

  /**
   * Mod note strings
   * @type {Object}
   */
  notes: {
    other: {
      string: '\n\n---\n\n***Mod note:** {$preset_note}{$note} ~ {$username}*'
    },
    thread_move: {
      string: '\n\n---\n\n***Mod note:** Moved to [{$category_name}]({$category_url}){$note} ~ {$username}*'
    }
  },

  preset_notes: [{
    label: 'We\'re encouraging completed and extensive ideas. Please work on it and come back!',
    note: 'We\'re encouraging completed and extensive ideas. Please work on it and come back!'
  }, {
    label: '/thread',
    note: '&lt;/thread&gt;'
  }, {
    label: 'Frog',
    note: '[img]http://static.drips.pw/rotmg/wiki/Pets/Demon%20Frog%20Generator.png[/img]'
  }, {
    label: 'ಠ_ಠ',
    note: 'ಠ_ಠ'
  }]

};

/**
 * Initialize Mod Note UI and events
 */
rmt.modnote.init = function () {
  console.log('rmt modnote init');

  // Simplify categories
  var _categories_simple = {};
  rmt.data.site_data.categories.forEach(function (_category) {
    var data = {
      id: _category.id,
      name: _category.name,
      url: _category.topic_url
    };
    // Apply parent ID
    if (_category.parent_category_id) {
      data.parent_id = _category.parent_category_id;
    }
    // Push
    _categories_simple[_category.id] = data;
  });

  // Build category option list
  var category_options = [];
  for (var _id in _categories_simple) {
    // Construct joined name
    var _name = _categories_simple[_id].name,
        no_parent = false,
        last_check = _id;
    while (!no_parent) {
      if (_categories_simple[last_check].parent_id) {
        // Prepend parent category name
        _name = _categories_simple[_categories_simple[last_check].parent_id].name + ' > ' + _name;
        last_check = _categories_simple[last_check].parent_id;
      } else {
        no_parent = true;
      }
    }

    // Append node
    category_options[category_options.length] = {
      tag: 'option',
      attr: {
        'data-name': _name,
        'data-url': _categories_simple[_id].url
      },
      html: _name
    };
  }

  // Build preset note option list
  var preset_notes = [{
    tag: 'option',
    attr: {
      'value': ''
    },
    html: 'No Preset Note'
  }];
  for (var _prop in rmt.modnote.preset_notes) {
    // Append node
    preset_notes[preset_notes.length] = {
      tag: 'option',
      attr: {
        'value': rmt.modnote.preset_notes[_prop].note
      },
      html: rmt.modnote.preset_notes[_prop].label
    };
  }
  console.log(preset_notes);

  // Sort alphabetically
  category_options.sort(function (a, b) {
    if (a.html < b.html) {
      return -1;
    }
    if (a.html > b.html) {
      return 1;
    }
    return 0;
  });

  // Render option
  rmt.ui.elems._modnote = rmt.ui.elems._opt_list.appendChild(_render({
    tag: 'li',
    attr: {
      'id': 'rmt__modnote'
    },
    nodes: [{
      tag: 'h4',
      attr: {
        class: 'rmt__opt-heading'
      },
      html: 'Add Mod Note',
      nodes: [{
        tag: 'span',
        attr: {
          'class': 'rmt-add',
          'title': 'Add Mod Note'
        }
      }],
      events: [{
        type: 'click',
        listener: rmt.modnote.toggleAdd
      }]
    }, {
      tag: 'div',
      attr: {
        'id': 'rmt__modnote__options',
        'class': 'rmt-opt-display'
      },
      css: {
        'display': 'none'
      },
      nodes: [{
        tag: 'form',
        attr: {
          'class': 'rmt__modnote__type',
          'action': '#'
        },
        events: [{
          type: 'submit',
          listener: function listener(e) {
            e.preventDefault();
            return false;
          }
        }],
        nodes: [{
          tag: 'span',
          attr: {
            'class': 'rmt__modnote__label'
          },
          html: 'Mod Note Type:'
        }, {
          tag: 'input',
          attr: {
            'type': 'radio',
            'id': 'rmt__modnote__type-select--thread-move',
            'name': 'rmt__modnote__type-select',
            'value': 'thread-move',
            'class': 'rmt__modnote__type-select',
            'checked': 'checked'
          }
        }, {
          tag: 'label',
          attr: {
            'class': 'rmt__modnote__type-label',
            'for': 'rmt__modnote__type-select--thread-move'
          },
          html: 'Thread Move'
        }, {
          tag: 'input',
          attr: {
            'type': 'radio',
            'id': 'rmt__modnote__type-select--other',
            'name': 'rmt__modnote__type-select',
            'value': 'other',
            'class': 'rmt__modnote__type-select'
          }
        }, {
          tag: 'label',
          attr: {
            'class': 'rmt__modnote__type-label',
            'for': 'rmt__modnote__type-select--other'
          },
          html: 'Other'
        }, {
          tag: 'select',
          attr: {
            'id': 'rmt__modnote__thread-move-loc',
            'class': 'rmt__modnote__optional'
          },
          nodes: category_options
        }, {
          tag: 'select',
          attr: {
            'id': 'rmt__modnote__preset-note',
            'class': 'rmt__modnote__optional'
          },
          nodes: preset_notes
        }, {
          tag: 'input',
          attr: {
            'type': 'text',
            'id': 'rmt__modnote__message',
            'placeholder': 'Mod Note...'
          }
        },, {
          tag: 'button',
          attr: {
            'id': 'rmt__modnote__submit'
          },
          events: [{
            type: 'click',
            listener: rmt.modnote.appendMessage
          }],
          html: 'Add to Edit'
        }]
      }]
    }]
  }));
};

/**
 * Toggle display of UI
 */
rmt.modnote.toggleAdd = function () {
  var _elem = rmt.ui.elems._modnote.querySelector('#rmt__modnote__options');
  if (!_elem) {
    return false;
  }
  if (rmt.modnote.display_state) {
    rmt.ui.msg.remove();
    _slide('up', _elem, 300, 'ease-in-out', true, function () {
      rmt.modnote.display_state = !rmt.modnote.display_state;
      rmt.ui.elems._modnote.className = '';
      // Reset values
      _elem.querySelector('#rmt__modnote__message').value = '';
      rmt.ui.elems._modnote.querySelector('#rmt__modnote__preset-note').options[0].selected = true;;
      rmt.ui.elems._modnote.querySelector('#rmt__modnote__thread-move-loc').options[0].selected = true;;
    });
  } else {
    _slide('down', _elem, 300, 'ease-in-out', true, function () {
      rmt.modnote.display_state = !rmt.modnote.display_state;
      rmt.ui.elems._modnote.className = 'rmt-open';
    });
  }
};

/**
 * Append message to editor based on note type
 */
rmt.modnote.appendMessage = function () {
  // Reset msg
  rmt.ui.msg.remove();

  // Default data
  var data = {
    'username': rmt.data.user_data.current_user.username,
    'note': '',
    'preset_note': ''
  };

  // Get editor
  var target = document.querySelector('#reply-control textarea.d-editor-input');
  if (!target) {
    rmt.ui.msg.set('No comment/topic editor found', 'error');
    return false;
  }

  // Message
  var msg = rmt.ui.elems._modnote.querySelector('#rmt__modnote__message').value.trim();

  // Mod note type
  switch (rmt.ui.elems._modnote.querySelector('input.rmt__modnote__type-select:checked').value) {
    // Other
    case 'other':
      var preset_notes = rmt.ui.elems._modnote.querySelector('#rmt__modnote__preset-note');
      if (preset_notes.selectedIndex !== -1) {
        data['preset_note'] = preset_notes.options[preset_notes.selectedIndex].value + (msg !== '' ? ' ' : '');
      }
      if (msg === '' && preset_notes.selectedIndex === 0) {
        rmt.ui.msg.set('Please enter or select a note', 'error');
        return false;
      }
      data['note'] = msg;
      // Append message to target
      target.value += rmt.modnote.generateMessage(rmt.modnote.notes.other.string, data);
      break;

    // Thread move
    case 'thread-move':
      var category_list = rmt.ui.elems._modnote.querySelector('#rmt__modnote__thread-move-loc');
      if (category_list.selectedIndex === -1) {
        rmt.ui.msg.set('Please select a category', 'error');
        return false;
      }
      var category = category_list.options[category_list.selectedIndex];
      data['category_name'] = category.getAttribute('data-name');
      data['category_url'] = category.getAttribute('data-url');
      // Append note
      data['note'] = (msg !== '' ? '. ' : '') + msg;
      // Append message to target
      target.value += rmt.modnote.generateMessage(rmt.modnote.notes.thread_move.string, data);
      break;
  }

  // Update target
  target.focus();
  setTimeout(function () {
    target.blur();
  }, 16);

  // Close UI
  rmt.modnote.toggleAdd();
};

/**
 * Parse template string and replace with data variables
 * @param  {string} msg_string
 * @param  {object} data
 * @return {string}
 */
rmt.modnote.generateMessage = function (msg_string, data) {
  for (var _search in data) {
    msg_string = msg_string.replace(new RegExp('\\{\\$' + _search + '\\}', 'gm'), data[_search]);
  }
  return msg_string;
};
'use strict';

/**
 * Latest Topics component
 * @type {Object}
 */
rmt.re_messages = {
  lookup_delay: 5000
};

/**
 * Initialize RealmEye Messages UI and events
 */
rmt.re_messages.init = function () {
  console.log('rmt re_messages init');

  // Prep data
  rmt.re_messages.url_base = '/messages-of-player/' + rmt.data.user_data.current_user.username;

  // Render option
  rmt.ui.elems._re_messages = rmt.ui.elems._opt_list.appendChild(_render({
    tag: 'li',
    attr: {
      'id': 'rmt__re_messages'
    },
    nodes: [{
      tag: 'a',
      attr: {
        'href': rmt.re_messages.url_base,
        'target': '_blank'
      },
      nodes: [{
        tag: 'h4',
        attr: {
          class: 'rmt__opt-heading'
        },
        html: 'RealmEye Messages',
        nodes: [{
          tag: 'span',
          attr: {
            id: 'rmt__re_messages__target'
          },
          html: '0'
        }]
      }]
    }]
  }));

  // Start message loop
  if (rmt.data.user_data.current_user.username) {
    rmt.re_messages.getMessageCount(true);
  }
};

/**
 * Fetch message count via AJAX and HTML parse
 * @param  {boolean} loop Loop request
 */
rmt.re_messages.getMessageCount = function (loop) {
  if (typeof loop === 'undefined') {
    loop = false;
  }

  // Init AJAX
  var lookup_messages = new Promise(function (resolve, reject) {

    _xhr({
      url: rmt.re_messages.url_base,
      load: function load() {
        if (this.status === 200) {
          resolve(this.responseText);
        } else {
          if (loop) {
            rmt.re_messages.getMessageCount(loop);
          }
        }
      }
    });
  });

  // Resolve promise
  lookup_messages.then(function (data) {
    var message_count = 0;

    // Match to find button link
    var pattern = new RegExp('(?:<a href="\\/messages-of-player\\/' + rmt.data.user_data.current_user.username + '">Messages\\s)\\((\\d+)\\)', 'im');
    var matches = data.match(pattern);
    if (matches[1] && matches[1].match(/\d+/)) {
      // Count as int
      message_count = matches[1] * 1;
    }

    // Render
    rmt.re_messages.renderMessageCount(message_count);

    // Loop if needed
    if (loop) {
      setTimeout(function () {
        rmt.re_messages.getMessageCount(loop);
      }, rmt.re_messages.lookup_delay);
    }
  });
};

/**
 * Render message to target
 * @param  {number} msg_count
 */
rmt.re_messages.renderMessageCount = function (msg_count) {
  var target = rmt.ui.elems._re_messages.querySelector('#rmt__re_messages__target');
  if (target) {
    // Display count
    target.innerHTML = msg_count;

    // Handle class
    if (msg_count === 0) {
      target.className = '';
    } else {
      target.className = 'rmt__re_messages--unread';
    }
  }
};
'use strict';

/**
 * Latest Topics component
 * @type {Object}
 */
rmt.latest_topics = {};

/**
 * Initialize Latest Topics UI
 */
rmt.latest_topics.init = function () {
  console.log('rmt latest_topics init');

  // Render option
  rmt.ui.elems._latest_topics = rmt.ui.elems._opt_list.appendChild(_render({
    tag: 'li',
    attr: {
      'id': 'rmt__latest_topics'
    },
    nodes: [{
      tag: 'a',
      attr: {
        'href': '/forum/latest'
      },
      nodes: [{
        tag: 'h4',
        attr: {
          class: 'rmt__opt-heading'
        },
        html: 'Latest Topics'
      }]
    }]
  }));
};
"use strict";

rmt.setup();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImluamVjdC9saWIvY29yZS1hbmltYXRpb25zLmpzIiwiaW5qZWN0L2xpYi9jb3JlLWRvbS5qcyIsImluamVjdC9saWIvY29yZS1kYXRhLmpzIiwiaW5qZWN0L2FwcC9jb3JlLmpzIiwiaW5qZWN0L2FwcC91aS5qcyIsImluamVjdC9hcHAvbW9kbm90ZS5qcyIsImluamVjdC9hcHAvcmVhbG1leWUtbWVzc2FnZXMuanMiLCJpbmplY3QvYXBwL2xhdGVzdC10b3BpY3MuanMiLCJpbmplY3QvaW5qZWN0LmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQTtBQUNBO0FBQ0E7OztBQUdBOzs7Ozs7OztBQVFBLEtBQUssV0FBTCxHQUFtQixVQUFTLENBQVQsRUFBWSxDQUFaLEVBQWUsQ0FBZixFQUFrQixDQUFsQixFQUFvQjtBQUNyQyxTQUFPLElBQUUsQ0FBRixHQUFJLENBQUosR0FBUSxDQUFmO0FBQ0QsQ0FGRDs7QUFLQTs7Ozs7Ozs7QUFRQSxLQUFLLE1BQUwsR0FBYyxVQUFTLENBQVQsRUFBWSxDQUFaLEVBQWUsQ0FBZixFQUFrQixDQUFsQixFQUFvQjtBQUNoQyxPQUFLLENBQUw7QUFDQSxTQUFPLElBQUUsQ0FBRixHQUFJLENBQUosR0FBTSxDQUFOLEdBQVUsQ0FBakI7QUFDRCxDQUhEOztBQU1BOzs7Ozs7OztBQVFBLEtBQUssT0FBTCxHQUFlLFVBQVMsQ0FBVCxFQUFZLENBQVosRUFBZSxDQUFmLEVBQWtCLENBQWxCLEVBQW9CO0FBQ2pDLE9BQUssQ0FBTDtBQUNBO0FBQ0EsU0FBTyxLQUFHLElBQUUsQ0FBRixHQUFJLENBQUosR0FBUSxDQUFYLElBQWdCLENBQXZCO0FBQ0QsQ0FKRDs7QUFPQTs7Ozs7Ozs7QUFRQSxLQUFLLFNBQUwsR0FBaUIsVUFBUyxDQUFULEVBQVksQ0FBWixFQUFlLENBQWYsRUFBa0IsQ0FBbEIsRUFBb0I7QUFDbkMsT0FBSyxJQUFFLENBQVA7QUFDQSxNQUFJLElBQUksQ0FBUixFQUFXLE9BQU8sSUFBRSxDQUFGLEdBQUksQ0FBSixHQUFNLENBQU4sR0FBUSxDQUFSLEdBQVksQ0FBbkI7QUFDWCxPQUFLLENBQUw7QUFDQSxTQUFPLElBQUUsQ0FBRixJQUFLLElBQUUsQ0FBRixHQUFJLENBQUosR0FBUSxDQUFiLElBQWtCLENBQXpCO0FBQ0QsQ0FMRDs7QUFRQTs7Ozs7Ozs7O0FBU0EsU0FBUyxLQUFULENBQWUsU0FBZixFQUEwQixPQUExQixFQUFtQyxRQUFuQyxFQUE2QyxNQUE3QyxFQUFxRCxFQUFyRCxFQUF3RDtBQUN0RCxNQUFJLElBQUo7QUFBQSxNQUNJLEVBREo7QUFBQSxNQUVJLFFBQVEsSUFBSSxJQUFKLEdBQVcsT0FBWCxFQUZaO0FBQUEsTUFHSSxJQUhKO0FBQUEsTUFJSSxTQUpKO0FBQUEsTUFLSSxRQUxKOztBQU9BO0FBQ0EsTUFBRyxRQUFRLFlBQVIsQ0FBcUIsV0FBckIsQ0FBSCxFQUFxQztBQUNuQyxXQUFPLEtBQVA7QUFDRDtBQUNELFVBQVEsWUFBUixDQUFxQixXQUFyQixFQUFrQyxJQUFsQzs7QUFFQTtBQUNBLFVBQU8sTUFBUDtBQUNFLFNBQUssUUFBTDtBQUNFLGVBQVMsS0FBSyxXQUFkO0FBQ0E7O0FBRUYsU0FBSyxRQUFMO0FBQ0UsZUFBUyxLQUFLLE1BQWQ7QUFDQTs7QUFFRixTQUFLLFdBQUw7QUFDRSxlQUFTLEtBQUssU0FBZDtBQUNBOztBQUVGO0FBQVM7QUFDUCxlQUFTLEtBQUssT0FBZDtBQUNBO0FBZko7O0FBa0JBO0FBQ0EsTUFBRyxDQUFDLFFBQVEsS0FBUixDQUFjLE9BQWxCLEVBQTBCO0FBQ3hCLFlBQVEsS0FBUixDQUFjLE9BQWQsR0FBeUIsYUFBYSxJQUFiLEdBQW9CLENBQXBCLEdBQXdCLENBQWpEO0FBQ0Q7O0FBRUQ7QUFDQSxTQUFPLENBQVA7QUFDQSxPQUFPLENBQVA7QUFDQSxNQUFHLGFBQWEsS0FBaEIsRUFBc0I7QUFDcEIsZ0JBQXdCLElBQXhCO0FBQ0EsWUFBUSxLQUFSLENBQWMsT0FBZCxHQUF3QixPQUF4QjtBQUNBLFdBQXdCLENBQXhCO0FBQ0EsU0FBd0IsQ0FBeEI7QUFDRDtBQUNELFNBQU8sS0FBSyxJQUFaOztBQUVBO0FBQ0EsTUFBRyxPQUFPLFFBQVAsS0FBb0IsUUFBdkIsRUFBZ0M7QUFDOUIsZUFBVyxHQUFYO0FBQ0Q7QUFDRCxhQUFXLEtBQUssR0FBTCxDQUFTLFFBQVQsQ0FBWDs7QUFFQTtBQUNBLGFBQVcsb0JBQVU7QUFDbkIsWUFBUSxlQUFSLENBQXdCLFdBQXhCO0FBQ0EsUUFBRyxPQUFPLEVBQVAsS0FBYyxVQUFqQixFQUE0QjtBQUMxQixTQUFHLElBQUgsQ0FBUSxPQUFSO0FBQ0Q7QUFDRixHQUxEOztBQU9BO0FBQ0EsY0FBVyxvQkFBVTtBQUNuQjtBQUNBLFFBQUksVUFBVSxJQUFJLElBQUosR0FBVyxPQUFYLEtBQXVCLEtBQXJDO0FBQUEsUUFDSSxTQUFVLE9BQU8sT0FBUCxFQUFnQixJQUFoQixFQUFzQixJQUF0QixFQUE0QixRQUE1QixDQURkO0FBRUEsWUFBUSxLQUFSLENBQWMsT0FBZCxHQUF3QixNQUF4Qjs7QUFFQSxRQUFHLFVBQVUsRUFBVixJQUFnQixXQUFXLFFBQTlCLEVBQXVDO0FBQ3JDO0FBQ0EsVUFBRyxhQUFhLEtBQWhCLEVBQXNCO0FBQ3BCO0FBQ0EsZ0JBQVEsS0FBUixDQUFjLE9BQWQsR0FBd0IsTUFBeEI7QUFDRDs7QUFFRDtBQUNBLGNBQVEsS0FBUixDQUFjLE9BQWQsR0FBd0IsRUFBeEI7O0FBRUE7QUFDQTtBQUNBLGFBQU8sSUFBUDtBQUNEO0FBQ0QsMEJBQXNCLFNBQXRCO0FBQ0QsR0FyQkQ7O0FBdUJBO0FBQ0E7QUFDRDs7QUFHRDs7Ozs7Ozs7O0FBU0EsU0FBUyxNQUFULENBQWdCLFNBQWhCLEVBQTJCLE9BQTNCLEVBQW9DLFFBQXBDLEVBQThDLE1BQTlDLEVBQXNELGNBQXRELEVBQXNFLEVBQXRFLEVBQXlFO0FBQ3ZFLE1BQUksSUFBSjtBQUFBLE1BQ0ksRUFESjtBQUFBLE1BRUksUUFBUSxJQUFJLElBQUosR0FBVyxPQUFYLEVBRlo7QUFBQSxNQUdJLElBSEo7QUFBQSxNQUlJLFVBSko7QUFBQSxNQUtJLFFBTEo7O0FBT0E7QUFDQSxNQUFHLFFBQVEsWUFBUixDQUFxQixZQUFyQixDQUFILEVBQXNDO0FBQ3BDLFdBQU8sS0FBUDtBQUNEO0FBQ0QsVUFBUSxZQUFSLENBQXFCLFlBQXJCLEVBQW1DLElBQW5DOztBQUVBO0FBQ0EsTUFBRyxjQUFILEVBQWtCO0FBQ2hCLFlBQVEsS0FBUixDQUFjLE9BQWQsR0FBeUIsT0FBekI7QUFDRDtBQUNELFVBQVEsS0FBUixDQUFjLFFBQWQsR0FBeUIsUUFBekI7QUFDQSxVQUFRLEtBQVIsQ0FBYyxNQUFkLEdBQTBCLGFBQWEsSUFBYixHQUFvQixRQUFRLFlBQVIsR0FBdUIsSUFBM0MsR0FBa0QsSUFBSSxJQUFoRjs7QUFFQTtBQUNBLFNBQU8sUUFBUSxZQUFmO0FBQ0EsT0FBTyxDQUFQO0FBQ0EsTUFBRyxhQUFhLElBQWhCLEVBQXFCO0FBQ25CLGdCQUFZLE1BQVo7QUFDQSxXQUFZLENBQVo7QUFDQSxTQUFZLFFBQVEsWUFBcEI7QUFDRDtBQUNELFNBQU8sS0FBSyxJQUFaOztBQUVBO0FBQ0EsTUFBRyxPQUFPLFFBQVAsS0FBb0IsUUFBdkIsRUFBZ0M7QUFDOUIsZUFBVyxHQUFYO0FBQ0Q7QUFDRCxhQUFXLEtBQUssR0FBTCxDQUFTLFFBQVQsQ0FBWDs7QUFFQTtBQUNBLFVBQU8sTUFBUDtBQUNFLFNBQUssUUFBTDtBQUNFLGVBQVMsS0FBSyxXQUFkO0FBQ0E7O0FBRUYsU0FBSyxRQUFMO0FBQ0UsZUFBUyxLQUFLLE1BQWQ7QUFDQTs7QUFFRixTQUFLLFdBQUw7QUFDRSxlQUFTLEtBQUssU0FBZDtBQUNBOztBQUVGO0FBQVM7QUFDUCxlQUFTLEtBQUssT0FBZDtBQUNBO0FBZko7O0FBa0JBO0FBQ0EsYUFBVyxvQkFBVTtBQUNuQixZQUFRLGVBQVIsQ0FBd0IsWUFBeEI7QUFDQSxRQUFHLE9BQU8sRUFBUCxLQUFjLFVBQWpCLEVBQTRCO0FBQzFCLFNBQUcsSUFBSCxDQUFRLE9BQVI7QUFDRDtBQUNGLEdBTEQ7O0FBT0E7QUFDQSxlQUFXLG9CQUFVO0FBQ25CO0FBQ0EsUUFBSSxVQUFVLElBQUksSUFBSixHQUFXLE9BQVgsS0FBdUIsS0FBckM7QUFBQSxRQUNJLFNBQVUsT0FBTyxPQUFQLEVBQWdCLElBQWhCLEVBQXNCLElBQXRCLEVBQTRCLFFBQTVCLENBRGQ7QUFFQSxZQUFRLEtBQVIsQ0FBYyxNQUFkLEdBQXVCLFNBQVMsSUFBaEM7O0FBRUEsUUFBRyxVQUFVLEVBQVYsSUFBZ0IsV0FBVyxRQUE5QixFQUF1QztBQUNyQztBQUNBLFVBQUcsYUFBYSxJQUFoQixFQUFxQjtBQUNuQjtBQUNBLFlBQUcsY0FBSCxFQUFrQjtBQUNoQixrQkFBUSxLQUFSLENBQWMsT0FBZCxHQUF3QixNQUF4QjtBQUNEO0FBQ0Y7O0FBRUQ7QUFDQSxjQUFRLEtBQVIsQ0FBYyxRQUFkLEdBQXlCLEVBQXpCO0FBQ0EsVUFBRyxjQUFILEVBQWtCO0FBQ2hCLGdCQUFRLEtBQVIsQ0FBYyxNQUFkLEdBQXVCLEVBQXZCO0FBQ0Q7O0FBRUQ7QUFDQTtBQUNBLGFBQU8sSUFBUDtBQUNEO0FBQ0QsMEJBQXNCLFVBQXRCO0FBQ0QsR0ExQkQ7O0FBNEJBO0FBQ0E7QUFDRDs7QUFHRDs7Ozs7Ozs7QUFRQSxTQUFTLFNBQVQsQ0FBbUIsV0FBbkIsRUFBZ0MsUUFBaEMsRUFBMEMsTUFBMUMsRUFBa0QsRUFBbEQsRUFBcUQ7QUFDbkQsTUFBSSxJQUFKO0FBQUEsTUFDSSxFQURKO0FBQUEsTUFFSSxRQUFRLElBQUksSUFBSixHQUFXLE9BQVgsRUFGWjtBQUFBLE1BR0ksSUFISjtBQUFBLE1BSUksVUFKSjtBQUFBLE1BS0ksUUFMSjtBQUFBLE1BTUksVUFBVSxTQUFTLElBTnZCOztBQVFBO0FBQ0EsTUFBRyxRQUFRLFlBQVIsQ0FBcUIsZ0JBQXJCLENBQUgsRUFBMEM7QUFDeEMsV0FBTyxLQUFQO0FBQ0Q7QUFDRCxVQUFRLFlBQVIsQ0FBcUIsZ0JBQXJCLEVBQXVDLElBQXZDOztBQUVBO0FBQ0EsaUJBQWMsV0FBZCx5Q0FBYyxXQUFkO0FBQ0UsU0FBSyxRQUFMO0FBQ0Usb0JBQWMsV0FBZDtBQUNBOztBQUVGLFNBQUssUUFBTDtBQUNFLFVBQUksT0FBTyxTQUFTLGFBQVQsQ0FBdUIsV0FBdkIsQ0FBWDtBQUNBLFVBQUcsQ0FBQyxJQUFKLEVBQVM7QUFBRSxlQUFPLEtBQVA7QUFBZTtBQUMxQixvQkFBYyxLQUFLLHFCQUFMLEdBQTZCLEdBQTNDO0FBQ0E7O0FBRUYsU0FBSyxRQUFMO0FBQ0UsVUFBRyxDQUFDLFlBQVksUUFBaEIsRUFBeUI7QUFBRSxlQUFPLEtBQVA7QUFBZTtBQUMxQyxvQkFBYyxZQUFZLHFCQUFaLEdBQW9DLEdBQWxEO0FBQ0E7O0FBRUY7QUFDRSxhQUFPLEtBQVA7QUFqQko7O0FBb0JBO0FBQ0EsTUFBSSxRQUFVLFNBQVMsSUFBdkI7QUFBQSxNQUNJLFFBQVUsU0FBUyxlQUR2QjtBQUFBLE1BRUksVUFBVSxLQUFLLEdBQUwsQ0FBUyxNQUFNLFlBQWYsRUFBNkIsTUFBTSxZQUFuQyxFQUFpRCxNQUFNLFlBQXZELEVBQXFFLE1BQU0sWUFBM0UsRUFBeUYsTUFBTSxZQUEvRixDQUZkO0FBR0EsZ0JBQWMsS0FBSyxHQUFMLENBQVMsQ0FBVCxFQUFZLEtBQUssR0FBTCxDQUFTLFdBQVQsRUFBc0IsVUFBVSxPQUFPLFdBQXZDLENBQVosQ0FBZDs7QUFFQTtBQUNBLFNBQU8sT0FBTyxPQUFkO0FBQ0EsT0FBTyxXQUFQO0FBQ0EsU0FBTyxLQUFLLElBQVo7O0FBRUE7QUFDQSxNQUFHLE9BQU8sUUFBUCxLQUFvQixRQUF2QixFQUFnQztBQUM5QixlQUFXLElBQVg7QUFDRDtBQUNELGFBQVcsS0FBSyxHQUFMLENBQVMsUUFBVCxDQUFYOztBQUVBO0FBQ0EsVUFBTyxNQUFQO0FBQ0UsU0FBSyxRQUFMO0FBQ0UsZUFBUyxLQUFLLFdBQWQ7QUFDQTs7QUFFRixTQUFLLFFBQUw7QUFDRSxlQUFTLEtBQUssTUFBZDtBQUNBOztBQUVGLFNBQUssV0FBTDtBQUNFLGVBQVMsS0FBSyxTQUFkO0FBQ0E7O0FBRUY7QUFBUztBQUNQLGVBQVMsS0FBSyxPQUFkO0FBQ0E7QUFmSjs7QUFrQkE7QUFDQSxhQUFXLG9CQUFVO0FBQ25CLFlBQVEsZUFBUixDQUF3QixnQkFBeEI7QUFDQSxRQUFHLE9BQU8sRUFBUCxLQUFjLFVBQWpCLEVBQTRCO0FBQzFCLFNBQUcsSUFBSCxDQUFRLE9BQVI7QUFDRDtBQUNGLEdBTEQ7O0FBT0E7QUFDQSxlQUFXLG9CQUFVO0FBQ25CO0FBQ0EsUUFBSSxVQUFVLElBQUksSUFBSixHQUFXLE9BQVgsS0FBdUIsS0FBckM7QUFBQSxRQUNJLFNBQVUsT0FBTyxPQUFQLEVBQWdCLElBQWhCLEVBQXNCLElBQXRCLEVBQTRCLFFBQTVCLENBRGQ7QUFFQSxXQUFPLFFBQVAsQ0FBZ0IsT0FBTyxPQUF2QixFQUFnQyxNQUFoQzs7QUFFQSxRQUFHLFVBQVUsRUFBVixJQUFnQixXQUFXLFFBQTlCLEVBQXVDO0FBQ3JDO0FBQ0E7QUFDQSxhQUFPLElBQVA7QUFDRDtBQUNELDBCQUFzQixVQUF0QjtBQUNELEdBWkQ7O0FBY0E7QUFDQTtBQUNEOzs7OztBQ3hYRDtBQUNBO0FBQ0E7OztBQUdBOzs7OztBQUtBLFNBQVMsT0FBVCxDQUFpQixHQUFqQixFQUFxQjtBQUNuQixNQUFJLFFBQVEsU0FBUixLQUFRLENBQVMsR0FBVCxFQUFhO0FBQ3ZCLFFBQUksSUFBSixFQUNJLEdBREo7O0FBR0E7QUFDQSxRQUFHLENBQUMsSUFBSSxHQUFSLEVBQVk7QUFDVixhQUFPLEtBQVA7QUFDRDtBQUNELFFBQUcsSUFBSSxFQUFQLEVBQVU7QUFDUjtBQUNBLGFBQU8sU0FBUyxlQUFULENBQXlCLElBQUksRUFBN0IsRUFBaUMsSUFBSSxHQUFyQyxDQUFQO0FBQ0QsS0FIRCxNQUdLO0FBQ0gsYUFBTyxTQUFTLGFBQVQsQ0FBdUIsSUFBSSxHQUEzQixDQUFQO0FBQ0Q7O0FBRUQ7QUFDQSxRQUFHLFFBQU8sSUFBSSxJQUFYLE1BQW9CLFFBQXZCLEVBQWdDO0FBQzlCLFdBQUksSUFBSSxTQUFSLElBQXFCLElBQUksSUFBekIsRUFBOEI7QUFDNUIsYUFBSyxZQUFMLENBQWtCLFNBQWxCLEVBQTZCLElBQUksSUFBSixDQUFTLFNBQVQsQ0FBN0I7QUFDRDtBQUNGOztBQUVEO0FBQ0EsUUFBRyxRQUFPLElBQUksR0FBWCxNQUFtQixRQUF0QixFQUErQjtBQUM3QixZQUFNLEVBQU47QUFDQSxXQUFJLElBQUksSUFBUixJQUFnQixJQUFJLEdBQXBCLEVBQXdCO0FBQ3RCLGVBQU8sT0FBTyxJQUFQLEdBQWMsSUFBSSxHQUFKLENBQVEsSUFBUixDQUFkLEdBQThCLElBQXJDO0FBQ0Q7QUFDRCxXQUFLLFlBQUwsQ0FBa0IsT0FBbEIsRUFBMkIsR0FBM0I7QUFDRDs7QUFFRDtBQUNBLFFBQUcsSUFBSSxJQUFQLEVBQVk7QUFDVixXQUFLLFNBQUwsR0FBaUIsSUFBSSxJQUFyQjtBQUNEOztBQUVEO0FBQ0EsUUFBRyxRQUFPLElBQUksTUFBWCxNQUFzQixRQUF6QixFQUFrQztBQUNoQyxVQUFJLE1BQUosQ0FBVyxPQUFYLENBQW1CLFVBQVMsTUFBVCxFQUFnQjtBQUNqQyxZQUFJLElBQUksU0FBSixDQUFJLENBQVMsQ0FBVCxFQUFXO0FBQ2pCLGlCQUFPLFFBQVAsQ0FBZ0IsSUFBaEIsQ0FBcUIsSUFBckIsRUFBMkIsQ0FBM0I7QUFDRCxTQUZEO0FBR0EsYUFBSyxnQkFBTCxDQUFzQixPQUFPLElBQTdCLEVBQW1DLENBQW5DO0FBQ0QsT0FMRDtBQU1EOztBQUVEO0FBQ0EsUUFBRyxJQUFJLEtBQVAsRUFBYTtBQUNYLFVBQUksS0FBSixDQUFVLE9BQVYsQ0FBa0IsVUFBUyxJQUFULEVBQWM7QUFDOUIsWUFBSSxRQUFRLE1BQU0sSUFBTixDQUFaO0FBQ0EsYUFBSyxXQUFMLENBQWlCLEtBQWpCO0FBQ0QsT0FIRDtBQUlEOztBQUVELFdBQU8sSUFBUDtBQUNELEdBdkREOztBQXlEQTtBQUNBLFNBQU8sTUFBTSxHQUFOLENBQVA7QUFDRDs7Ozs7QUN0RUQ7QUFDQTtBQUNBOzs7QUFHQTs7OztBQUlBLFNBQVMsSUFBVCxDQUFjLE9BQWQsRUFBc0I7QUFDcEIsTUFBSSxHQUFKLEVBQ0ksTUFESixFQUVJLFFBRko7O0FBSUE7QUFDQSxNQUFHLFFBQU8sT0FBUCx5Q0FBTyxPQUFQLE9BQW1CLFFBQXRCLEVBQStCO0FBQzdCLFdBQU8sS0FBUDtBQUNEO0FBQ0QsTUFBRyxPQUFPLFFBQVEsR0FBZixLQUF1QixRQUExQixFQUFtQztBQUNqQyxXQUFPLEtBQVA7QUFDRDs7QUFFRDtBQUNBLE1BQUcsT0FBTyxRQUFRLE1BQWYsS0FBMEIsUUFBN0IsRUFBc0M7QUFDcEM7QUFDQSxZQUFRLE1BQVIsR0FBaUIsS0FBakI7QUFDRCxHQUhELE1BR0s7QUFDSCxZQUFRLE1BQVIsR0FBaUIsUUFBUSxNQUFSLENBQWUsV0FBZixFQUFqQjtBQUNEOztBQUVEO0FBQ0EsUUFBTSxJQUFJLGNBQUosRUFBTjtBQUNBLE1BQUksSUFBSixDQUFTLFFBQVEsTUFBakIsRUFBeUIsUUFBUSxHQUFqQyxFQUFzQyxRQUFRLEtBQTlDOztBQUVBO0FBQ0EsTUFBRyxRQUFPLFFBQVEsSUFBZixNQUF3QixRQUEzQixFQUFvQztBQUNsQyxTQUFJLElBQUksR0FBUixJQUFlLFFBQVEsSUFBdkIsRUFBNEI7QUFDMUIsZ0JBQVUsTUFBTSxtQkFBbUIsR0FBbkIsQ0FBTixHQUFnQyxHQUFoQyxHQUFzQyxtQkFBbUIsUUFBUSxJQUFSLENBQWEsR0FBYixDQUFuQixDQUFoRDtBQUNEO0FBQ0QsYUFBUyxPQUFPLE9BQVAsQ0FBZSxJQUFmLEVBQXFCLEdBQXJCLENBQVQ7QUFDRDs7QUFFRDtBQUNBLE1BQUcsUUFBUSxNQUFSLElBQWtCLE1BQXJCLEVBQTRCO0FBQzFCLFFBQUksZ0JBQUosQ0FBcUIsY0FBckIsRUFBcUMsbUNBQXJDO0FBQ0E7QUFDQTtBQUNEO0FBQ0QsTUFBRyxRQUFRLE1BQVIsSUFBa0IsS0FBckIsRUFBMkI7QUFDekIsWUFBUSxHQUFSLElBQWdCLFNBQVMsTUFBVCxHQUFrQixFQUFsQztBQUNEOztBQUVEO0FBQ0EsYUFBVyxvQkFBVTtBQUNuQixRQUFHLE9BQU8sUUFBUSxJQUFmLEtBQXdCLFVBQTNCLEVBQXNDO0FBQ3BDLGNBQVEsSUFBUixDQUFhLElBQWIsQ0FBa0IsSUFBbEI7QUFDRDtBQUNGLEdBSkQ7O0FBTUE7QUFDQSxNQUFHLE9BQU8sUUFBUSxLQUFmLEtBQXlCLFNBQTVCLEVBQXNDO0FBQ3BDO0FBQ0EsWUFBUSxLQUFSLEdBQWdCLElBQWhCO0FBQ0Q7O0FBRUQ7QUFDQSxNQUFJLGtCQUFKLEdBQXlCLFlBQVU7QUFDakMsUUFBRyxJQUFJLFVBQUosSUFBa0IsQ0FBbEIsSUFBdUIsSUFBSSxNQUFKLElBQWMsR0FBeEMsRUFBNEM7QUFDMUMsZUFBUyxJQUFULENBQWMsR0FBZDtBQUNEO0FBQ0YsR0FKRDtBQUtBLE1BQUksSUFBSixDQUFTLE1BQVQ7QUFDRDs7Ozs7QUN4RUQsSUFBSSxNQUFNLEVBQVY7O0FBRUEsSUFBSSxJQUFKLEdBQVcsWUFBVTtBQUNuQixVQUFRLEdBQVIsQ0FBWSxVQUFaO0FBQ0EsVUFBUSxHQUFSLENBQVksSUFBSSxJQUFoQjtBQUNBLE1BQUksRUFBSixDQUFPLElBQVA7QUFDRCxDQUpEOztBQU1BOzs7QUFHQSxJQUFJLEtBQUosR0FBWSxZQUFVO0FBQ3BCO0FBQ0EsTUFBSSxVQUFVO0FBQ1osZUFBVyxLQURDO0FBRVosZUFBVztBQUZDLEdBQWQ7O0FBS0E7QUFDQSxNQUFJLFlBQVksSUFBSSxPQUFKLENBQVksVUFBUyxPQUFULEVBQWtCLE1BQWxCLEVBQXlCOztBQUVuRCxTQUFLO0FBQ0gsV0FBSyxrQkFERjtBQUVILFlBQU0sZ0JBQVU7QUFDZCxZQUFJLE9BQU8sS0FBSyxLQUFMLENBQVcsS0FBSyxZQUFoQixDQUFYO0FBQ0EsWUFBRyxLQUFLLE1BQUwsS0FBZ0IsR0FBaEIsSUFBdUIsUUFBTyxJQUFQLHlDQUFPLElBQVAsT0FBZ0IsUUFBMUMsRUFBbUQ7QUFDakQsa0JBQVEsSUFBUjtBQUNEO0FBQ0Y7QUFQRSxLQUFMO0FBVUQsR0FaZSxDQUFoQjs7QUFjQTtBQUNBLE1BQUksWUFBWSxJQUFJLE9BQUosQ0FBWSxVQUFTLE9BQVQsRUFBa0IsTUFBbEIsRUFBeUI7O0FBRW5ELFNBQUs7QUFDSCxXQUFLLDZCQURGO0FBRUgsWUFBTSxnQkFBVTtBQUNkLFlBQUksT0FBTyxLQUFLLEtBQUwsQ0FBVyxLQUFLLFlBQWhCLENBQVg7QUFDQSxZQUFHLEtBQUssTUFBTCxLQUFnQixHQUFoQixJQUF1QixRQUFPLElBQVAseUNBQU8sSUFBUCxPQUFnQixRQUExQyxFQUFtRDtBQUNqRCxrQkFBUSxJQUFSO0FBQ0Q7QUFDRjtBQVBFLEtBQUw7QUFVRCxHQVplLENBQWhCOztBQWNBO0FBQ0EsWUFBVSxJQUFWLENBQWUsVUFBUyxJQUFULEVBQWM7QUFDM0IsWUFBUSxTQUFSLEdBQW9CLElBQXBCO0FBQ0E7QUFDRCxHQUhEO0FBSUEsWUFBVSxJQUFWLENBQWUsVUFBUyxJQUFULEVBQWM7QUFDM0IsWUFBUSxTQUFSLEdBQW9CLElBQXBCO0FBQ0E7QUFDRCxHQUhEO0FBSUEsTUFBSSxRQUFRLFNBQVIsS0FBUSxHQUFVO0FBQ3BCLFFBQUcsUUFBUSxTQUFSLElBQXFCLFFBQVEsU0FBaEMsRUFBMEM7QUFDeEM7QUFDQSxVQUFJLElBQUosR0FBVyxPQUFYO0FBQ0EsVUFBSSxJQUFKO0FBQ0Q7QUFDRixHQU5EO0FBUUQsQ0F0REQ7OztBQ1hBLElBQUksRUFBSixHQUFTO0FBQ1AsU0FBTyxFQURBO0FBRVAsaUJBQWU7QUFGUixDQUFUOztBQU1BOzs7QUFHQSxJQUFJLEVBQUosQ0FBTyxJQUFQLEdBQWMsWUFBVTtBQUN0QixVQUFRLEdBQVIsQ0FBWSxhQUFaOztBQUVBO0FBQ0EsTUFBSSxFQUFKLENBQU8sS0FBUCxDQUFhLEtBQWIsR0FBcUIsU0FBUyxJQUFULENBQWMsV0FBZCxDQUEwQixRQUFRO0FBQ3JELFNBQUssS0FEZ0Q7QUFFckQsVUFBTTtBQUNKLFlBQU07QUFERjtBQUYrQyxHQUFSLENBQTFCLENBQXJCOztBQU9BO0FBQ0EsTUFBSSxFQUFKLENBQU8sS0FBUCxDQUFhLGVBQWIsR0FBK0IsSUFBSSxFQUFKLENBQU8sS0FBUCxDQUFhLEtBQWIsQ0FBbUIsV0FBbkIsQ0FBK0IsUUFBUTtBQUNwRSxTQUFLLE1BRCtEO0FBRXBFLFVBQU07QUFDSixZQUFNLHFCQURGO0FBRUosZUFBUztBQUZMLEtBRjhEO0FBTXBFLFlBQVEsQ0FDTjtBQUNFLFlBQU0sT0FEUjtBQUVFLGdCQUFVLElBQUksRUFBSixDQUFPLE1BQVAsQ0FBYztBQUYxQixLQURNO0FBTjRELEdBQVIsQ0FBL0IsQ0FBL0I7O0FBY0E7QUFDQSxNQUFJLEVBQUosQ0FBTyxLQUFQLENBQWEsYUFBYixHQUE2QixJQUFJLEVBQUosQ0FBTyxLQUFQLENBQWEsS0FBYixDQUFtQixXQUFuQixDQUErQixRQUFRO0FBQ2xFLFNBQUssS0FENkQ7QUFFbEUsVUFBTTtBQUNKLFlBQU07QUFERjtBQUY0RCxHQUFSLENBQS9CLENBQTdCOztBQU9BO0FBQ0EsTUFBSSxFQUFKLENBQU8sS0FBUCxDQUFhLElBQWIsR0FBb0IsSUFBSSxFQUFKLENBQU8sS0FBUCxDQUFhLGFBQWIsQ0FBMkIsV0FBM0IsQ0FBdUMsUUFBUTtBQUNqRSxTQUFLLEtBRDREO0FBRWpFLFVBQU07QUFDSixZQUFNO0FBREYsS0FGMkQ7QUFLakUsU0FBSztBQUNILGlCQUFXO0FBRFI7QUFMNEQsR0FBUixDQUF2QyxDQUFwQjs7QUFVQTtBQUNBLE1BQUksRUFBSixDQUFPLEtBQVAsQ0FBYSxTQUFiLEdBQXlCLElBQUksRUFBSixDQUFPLEtBQVAsQ0FBYSxhQUFiLENBQTJCLFdBQTNCLENBQXVDLFFBQVE7QUFDdEUsU0FBSyxJQURpRTtBQUV0RSxVQUFNO0FBQ0osWUFBTTtBQURGO0FBRmdFLEdBQVIsQ0FBdkMsQ0FBekI7O0FBT0E7OztBQUdBLE1BQUksRUFBSixDQUFPLFFBQVA7QUFDRCxDQXpERDs7QUE0REE7OztBQUdBLElBQUksRUFBSixDQUFPLFFBQVAsR0FBa0IsWUFBVTtBQUMxQixNQUFJLE9BQUosQ0FBWSxJQUFaO0FBQ0EsTUFBSSxXQUFKLENBQWdCLElBQWhCO0FBQ0EsTUFBSSxhQUFKLENBQWtCLElBQWxCO0FBQ0QsQ0FKRDs7QUFRQTtBQUNBO0FBQ0E7QUFDQSxJQUFJLEVBQUosQ0FBTyxNQUFQLEdBQWdCLEVBQWhCOztBQUVBOzs7O0FBSUEsSUFBSSxFQUFKLENBQU8sTUFBUCxDQUFjLGFBQWQsR0FBOEIsWUFBVTtBQUN0QyxNQUFHLElBQUksRUFBSixDQUFPLGFBQVYsRUFBd0I7QUFDdEIsV0FBTyxJQUFQLEVBQWEsSUFBSSxFQUFKLENBQU8sS0FBUCxDQUFhLGFBQTFCLEVBQXlDLEdBQXpDLEVBQThDLGFBQTlDLEVBQTZELElBQTdELEVBQW1FLFlBQVU7QUFDM0UsVUFBSSxFQUFKLENBQU8sYUFBUCxHQUF1QixDQUFDLElBQUksRUFBSixDQUFPLGFBQS9CO0FBQ0EsVUFBSSxFQUFKLENBQU8sS0FBUCxDQUFhLGVBQWIsQ0FBNkIsU0FBN0IsR0FBeUMsWUFBekM7QUFDRCxLQUhEO0FBSUQsR0FMRCxNQUtLO0FBQ0gsV0FBTyxNQUFQLEVBQWUsSUFBSSxFQUFKLENBQU8sS0FBUCxDQUFhLGFBQTVCLEVBQTJDLEdBQTNDLEVBQWdELGFBQWhELEVBQStELElBQS9ELEVBQXFFLFlBQVU7QUFDN0UsVUFBSSxFQUFKLENBQU8sYUFBUCxHQUF1QixDQUFDLElBQUksRUFBSixDQUFPLGFBQS9CO0FBQ0EsVUFBSSxFQUFKLENBQU8sS0FBUCxDQUFhLGVBQWIsQ0FBNkIsU0FBN0IsR0FBeUMsRUFBekM7QUFDRCxLQUhEO0FBSUQ7QUFDRixDQVpEOztBQWdCQTtBQUNBO0FBQ0E7QUFDQSxJQUFJLEVBQUosQ0FBTyxHQUFQLEdBQWEsRUFBYjs7QUFFQTs7Ozs7QUFLQSxJQUFJLEVBQUosQ0FBTyxHQUFQLENBQVcsR0FBWCxHQUFpQixVQUFTLEdBQVQsRUFBYyxJQUFkLEVBQW1CO0FBQ2xDLE1BQUksRUFBSixDQUFPLEtBQVAsQ0FBYSxJQUFiLENBQWtCLFNBQWxCLEdBQThCLGNBQWMsSUFBNUM7QUFDQSxNQUFJLEVBQUosQ0FBTyxLQUFQLENBQWEsSUFBYixDQUFrQixLQUFsQixDQUF3QixPQUF4QixHQUFrQyxPQUFsQztBQUNBLE1BQUksRUFBSixDQUFPLEtBQVAsQ0FBYSxJQUFiLENBQWtCLFNBQWxCLEdBQThCLEdBQTlCO0FBQ0QsQ0FKRDs7QUFNQTs7O0FBR0EsSUFBSSxFQUFKLENBQU8sR0FBUCxDQUFXLE1BQVgsR0FBb0IsWUFBVTtBQUM1QixNQUFJLEVBQUosQ0FBTyxLQUFQLENBQWEsSUFBYixDQUFrQixTQUFsQixHQUE4QixFQUE5QixFQUNBLElBQUksRUFBSixDQUFPLEtBQVAsQ0FBYSxJQUFiLENBQWtCLEtBQWxCLENBQXdCLE9BQXhCLEdBQWtDLE1BRGxDO0FBRUEsTUFBSSxFQUFKLENBQU8sS0FBUCxDQUFhLElBQWIsQ0FBa0IsU0FBbEIsR0FBOEIsRUFBOUI7QUFDRCxDQUpEOzs7QUM1SEE7Ozs7QUFJQSxJQUFJLE9BQUosR0FBYzs7QUFFWjs7O0FBR0EsaUJBQWUsQ0FMSDs7QUFPWjs7OztBQUlBLFNBQU87QUFDTCxXQUFPO0FBQ0wsY0FBUTtBQURILEtBREY7QUFJTCxpQkFBYTtBQUNYLGNBQVE7QUFERztBQUpSLEdBWEs7O0FBb0JaLGdCQUFjLENBQ1o7QUFDRSxXQUFPLG9GQURUO0FBRUUsVUFBTTtBQUZSLEdBRFksRUFLWjtBQUNFLFdBQU8sU0FEVDtBQUVFLFVBQU07QUFGUixHQUxZLEVBU1o7QUFDRSxXQUFPLE1BRFQ7QUFFRSxVQUFNO0FBRlIsR0FUWSxFQWFaO0FBQ0UsV0FBTyxLQURUO0FBRUUsVUFBTTtBQUZSLEdBYlk7O0FBcEJGLENBQWQ7O0FBMENBOzs7QUFHQSxJQUFJLE9BQUosQ0FBWSxJQUFaLEdBQW1CLFlBQVU7QUFDM0IsVUFBUSxHQUFSLENBQVksa0JBQVo7O0FBRUE7QUFDQSxNQUFJLHFCQUFxQixFQUF6QjtBQUNBLE1BQUksSUFBSixDQUFTLFNBQVQsQ0FBbUIsVUFBbkIsQ0FBOEIsT0FBOUIsQ0FBc0MsVUFBUyxTQUFULEVBQW1CO0FBQ3ZELFFBQUksT0FBTztBQUNULFVBQUksVUFBVSxFQURMO0FBRVQsWUFBTSxVQUFVLElBRlA7QUFHVCxXQUFLLFVBQVU7QUFITixLQUFYO0FBS0E7QUFDQSxRQUFHLFVBQVUsa0JBQWIsRUFBZ0M7QUFDOUIsV0FBSyxTQUFMLEdBQWlCLFVBQVUsa0JBQTNCO0FBQ0Q7QUFDRDtBQUNBLHVCQUFtQixVQUFVLEVBQTdCLElBQW1DLElBQW5DO0FBQ0QsR0FaRDs7QUFjQTtBQUNBLE1BQUksbUJBQW1CLEVBQXZCO0FBQ0EsT0FBSSxJQUFJLEdBQVIsSUFBZSxrQkFBZixFQUFrQztBQUNoQztBQUNBLFFBQUksUUFBUSxtQkFBbUIsR0FBbkIsRUFBd0IsSUFBcEM7QUFBQSxRQUNJLFlBQVksS0FEaEI7QUFBQSxRQUVJLGFBQWEsR0FGakI7QUFHQSxXQUFNLENBQUMsU0FBUCxFQUFpQjtBQUNmLFVBQUcsbUJBQW1CLFVBQW5CLEVBQStCLFNBQWxDLEVBQTRDO0FBQzFDO0FBQ0EsZ0JBQVEsbUJBQW1CLG1CQUFtQixVQUFuQixFQUErQixTQUFsRCxFQUE2RCxJQUE3RCxHQUFvRSxLQUFwRSxHQUE0RSxLQUFwRjtBQUNBLHFCQUFhLG1CQUFtQixVQUFuQixFQUErQixTQUE1QztBQUNELE9BSkQsTUFJSztBQUNILG9CQUFZLElBQVo7QUFDRDtBQUNGOztBQUVEO0FBQ0EscUJBQWlCLGlCQUFpQixNQUFsQyxJQUE0QztBQUMxQyxXQUFLLFFBRHFDO0FBRTFDLFlBQU07QUFDSixxQkFBYSxLQURUO0FBRUosb0JBQVksbUJBQW1CLEdBQW5CLEVBQXdCO0FBRmhDLE9BRm9DO0FBTTFDLFlBQU07QUFOb0MsS0FBNUM7QUFRRDs7QUFFRDtBQUNBLE1BQUksZUFBZSxDQUFDO0FBQ2xCLFNBQUssUUFEYTtBQUVsQixVQUFNO0FBQ0osZUFBUztBQURMLEtBRlk7QUFLbEIsVUFBTTtBQUxZLEdBQUQsQ0FBbkI7QUFPQSxPQUFJLElBQUksS0FBUixJQUFpQixJQUFJLE9BQUosQ0FBWSxZQUE3QixFQUEwQztBQUN4QztBQUNBLGlCQUFhLGFBQWEsTUFBMUIsSUFBb0M7QUFDbEMsV0FBSyxRQUQ2QjtBQUVsQyxZQUFNO0FBQ0osaUJBQVMsSUFBSSxPQUFKLENBQVksWUFBWixDQUF5QixLQUF6QixFQUFnQztBQURyQyxPQUY0QjtBQUtsQyxZQUFNLElBQUksT0FBSixDQUFZLFlBQVosQ0FBeUIsS0FBekIsRUFBZ0M7QUFMSixLQUFwQztBQU9EO0FBQ0QsVUFBUSxHQUFSLENBQVksWUFBWjs7QUFFQTtBQUNBLG1CQUFpQixJQUFqQixDQUFzQixVQUFTLENBQVQsRUFBWSxDQUFaLEVBQWM7QUFDbEMsUUFBRyxFQUFFLElBQUYsR0FBUyxFQUFFLElBQWQsRUFBbUI7QUFDakIsYUFBTyxDQUFDLENBQVI7QUFDRDtBQUNELFFBQUcsRUFBRSxJQUFGLEdBQVMsRUFBRSxJQUFkLEVBQW1CO0FBQ2pCLGFBQU8sQ0FBUDtBQUNEO0FBQ0QsV0FBTyxDQUFQO0FBQ0QsR0FSRDs7QUFVQTtBQUNBLE1BQUksRUFBSixDQUFPLEtBQVAsQ0FBYSxRQUFiLEdBQXdCLElBQUksRUFBSixDQUFPLEtBQVAsQ0FBYSxTQUFiLENBQXVCLFdBQXZCLENBQW1DLFFBQVE7QUFDakUsU0FBSyxJQUQ0RDtBQUVqRSxVQUFNO0FBQ0osWUFBTTtBQURGLEtBRjJEO0FBS2pFLFdBQU8sQ0FDTDtBQUNFLFdBQUssSUFEUDtBQUVFLFlBQU07QUFDSixlQUFPO0FBREgsT0FGUjtBQUtFLFlBQU0sY0FMUjtBQU1FLGFBQU8sQ0FDTDtBQUNFLGFBQUssTUFEUDtBQUVFLGNBQU07QUFDSixtQkFBUyxTQURMO0FBRUosbUJBQVM7QUFGTDtBQUZSLE9BREssQ0FOVDtBQWVFLGNBQVEsQ0FDTjtBQUNFLGNBQU0sT0FEUjtBQUVFLGtCQUFVLElBQUksT0FBSixDQUFZO0FBRnhCLE9BRE07QUFmVixLQURLLEVBdUJMO0FBQ0UsV0FBSyxLQURQO0FBRUUsWUFBTTtBQUNKLGNBQU0sdUJBREY7QUFFSixpQkFBUztBQUZMLE9BRlI7QUFNRSxXQUFLO0FBQ0gsbUJBQVc7QUFEUixPQU5QO0FBU0UsYUFBTyxDQUNMO0FBQ0UsYUFBSyxNQURQO0FBRUUsY0FBTTtBQUNKLG1CQUFTLG9CQURMO0FBRUosb0JBQVU7QUFGTixTQUZSO0FBTUUsZ0JBQVEsQ0FDTjtBQUNFLGdCQUFNLFFBRFI7QUFFRSxvQkFBVSxrQkFBUyxDQUFULEVBQVc7QUFDbkIsY0FBRSxjQUFGO0FBQ0EsbUJBQU8sS0FBUDtBQUNEO0FBTEgsU0FETSxDQU5WO0FBZUUsZUFBTyxDQUNMO0FBQ0UsZUFBSyxNQURQO0FBRUUsZ0JBQU07QUFDSixxQkFBUztBQURMLFdBRlI7QUFLRSxnQkFBTTtBQUxSLFNBREssRUFRTDtBQUNFLGVBQUssT0FEUDtBQUVFLGdCQUFNO0FBQ0osb0JBQVEsT0FESjtBQUVKLGtCQUFNLHdDQUZGO0FBR0osb0JBQVEsMkJBSEo7QUFJSixxQkFBUyxhQUpMO0FBS0oscUJBQVMsMkJBTEw7QUFNSix1QkFBVztBQU5QO0FBRlIsU0FSSyxFQW1CTDtBQUNFLGVBQUssT0FEUDtBQUVFLGdCQUFNO0FBQ0oscUJBQVMsMEJBREw7QUFFSixtQkFBTztBQUZILFdBRlI7QUFNRSxnQkFBTTtBQU5SLFNBbkJLLEVBMkJMO0FBQ0UsZUFBSyxPQURQO0FBRUUsZ0JBQU07QUFDSixvQkFBUSxPQURKO0FBRUosa0JBQU0sa0NBRkY7QUFHSixvQkFBUSwyQkFISjtBQUlKLHFCQUFTLE9BSkw7QUFLSixxQkFBUztBQUxMO0FBRlIsU0EzQkssRUFxQ0w7QUFDRSxlQUFLLE9BRFA7QUFFRSxnQkFBTTtBQUNKLHFCQUFTLDBCQURMO0FBRUosbUJBQU87QUFGSCxXQUZSO0FBTUUsZ0JBQU07QUFOUixTQXJDSyxFQTZDTDtBQUNFLGVBQUssUUFEUDtBQUVFLGdCQUFNO0FBQ0osa0JBQU0sK0JBREY7QUFFSixxQkFBUztBQUZMLFdBRlI7QUFNRSxpQkFBTztBQU5ULFNBN0NLLEVBcURMO0FBQ0UsZUFBSyxRQURQO0FBRUUsZ0JBQU07QUFDSixrQkFBTSwyQkFERjtBQUVKLHFCQUFTO0FBRkwsV0FGUjtBQU1FLGlCQUFPO0FBTlQsU0FyREssRUE2REw7QUFDRSxlQUFLLE9BRFA7QUFFRSxnQkFBTTtBQUNKLG9CQUFRLE1BREo7QUFFSixrQkFBTSx1QkFGRjtBQUdKLDJCQUFlO0FBSFg7QUFGUixTQTdESyxHQXFFTDtBQUNFLGVBQUssUUFEUDtBQUVFLGdCQUFNO0FBQ0osa0JBQU07QUFERixXQUZSO0FBS0Usa0JBQVEsQ0FDTjtBQUNFLGtCQUFNLE9BRFI7QUFFRSxzQkFBVSxJQUFJLE9BQUosQ0FBWTtBQUZ4QixXQURNLENBTFY7QUFXRSxnQkFBTTtBQVhSLFNBckVLO0FBZlQsT0FESztBQVRULEtBdkJLO0FBTDBELEdBQVIsQ0FBbkMsQ0FBeEI7QUE4SUQsQ0E3TkQ7O0FBZ09BOzs7QUFHQSxJQUFJLE9BQUosQ0FBWSxTQUFaLEdBQXdCLFlBQVU7QUFDaEMsTUFBSSxRQUFRLElBQUksRUFBSixDQUFPLEtBQVAsQ0FBYSxRQUFiLENBQXNCLGFBQXRCLENBQW9DLHdCQUFwQyxDQUFaO0FBQ0EsTUFBRyxDQUFDLEtBQUosRUFBVTtBQUFFLFdBQU8sS0FBUDtBQUFlO0FBQzNCLE1BQUcsSUFBSSxPQUFKLENBQVksYUFBZixFQUE2QjtBQUMzQixRQUFJLEVBQUosQ0FBTyxHQUFQLENBQVcsTUFBWDtBQUNBLFdBQU8sSUFBUCxFQUFhLEtBQWIsRUFBb0IsR0FBcEIsRUFBeUIsYUFBekIsRUFBd0MsSUFBeEMsRUFBOEMsWUFBVTtBQUN0RCxVQUFJLE9BQUosQ0FBWSxhQUFaLEdBQTRCLENBQUMsSUFBSSxPQUFKLENBQVksYUFBekM7QUFDQSxVQUFJLEVBQUosQ0FBTyxLQUFQLENBQWEsUUFBYixDQUFzQixTQUF0QixHQUFrQyxFQUFsQztBQUNBO0FBQ0EsWUFBTSxhQUFOLENBQW9CLHdCQUFwQixFQUE4QyxLQUE5QyxHQUFzRCxFQUF0RDtBQUNBLFVBQUksRUFBSixDQUFPLEtBQVAsQ0FBYSxRQUFiLENBQXNCLGFBQXRCLENBQW9DLDRCQUFwQyxFQUFrRSxPQUFsRSxDQUEwRSxDQUExRSxFQUE2RSxRQUE3RSxHQUF3RixJQUF4RixDQUE2RjtBQUM3RixVQUFJLEVBQUosQ0FBTyxLQUFQLENBQWEsUUFBYixDQUFzQixhQUF0QixDQUFvQyxnQ0FBcEMsRUFBc0UsT0FBdEUsQ0FBOEUsQ0FBOUUsRUFBaUYsUUFBakYsR0FBNEYsSUFBNUYsQ0FBaUc7QUFDbEcsS0FQRDtBQVFELEdBVkQsTUFVSztBQUNILFdBQU8sTUFBUCxFQUFlLEtBQWYsRUFBc0IsR0FBdEIsRUFBMkIsYUFBM0IsRUFBMEMsSUFBMUMsRUFBZ0QsWUFBVTtBQUN4RCxVQUFJLE9BQUosQ0FBWSxhQUFaLEdBQTRCLENBQUMsSUFBSSxPQUFKLENBQVksYUFBekM7QUFDQSxVQUFJLEVBQUosQ0FBTyxLQUFQLENBQWEsUUFBYixDQUFzQixTQUF0QixHQUFrQyxVQUFsQztBQUNELEtBSEQ7QUFJRDtBQUNGLENBbkJEOztBQXNCQTs7O0FBR0EsSUFBSSxPQUFKLENBQVksYUFBWixHQUE0QixZQUFVO0FBQ3BDO0FBQ0EsTUFBSSxFQUFKLENBQU8sR0FBUCxDQUFXLE1BQVg7O0FBRUE7QUFDQSxNQUFJLE9BQU87QUFDVCxnQkFBWSxJQUFJLElBQUosQ0FBUyxTQUFULENBQW1CLFlBQW5CLENBQWdDLFFBRG5DO0FBRVQsWUFBUSxFQUZDO0FBR1QsbUJBQWU7QUFITixHQUFYOztBQU1BO0FBQ0EsTUFBSSxTQUFTLFNBQVMsYUFBVCxDQUF1Qix3Q0FBdkIsQ0FBYjtBQUNBLE1BQUcsQ0FBQyxNQUFKLEVBQVc7QUFDVCxRQUFJLEVBQUosQ0FBTyxHQUFQLENBQVcsR0FBWCxDQUFlLCtCQUFmLEVBQWdELE9BQWhEO0FBQ0EsV0FBTyxLQUFQO0FBQ0Q7O0FBRUQ7QUFDQSxNQUFJLE1BQU0sSUFBSSxFQUFKLENBQU8sS0FBUCxDQUFhLFFBQWIsQ0FBc0IsYUFBdEIsQ0FBb0Msd0JBQXBDLEVBQThELEtBQTlELENBQW9FLElBQXBFLEVBQVY7O0FBRUE7QUFDQSxVQUFPLElBQUksRUFBSixDQUFPLEtBQVAsQ0FBYSxRQUFiLENBQXNCLGFBQXRCLENBQW9DLHlDQUFwQyxFQUErRSxLQUF0RjtBQUNFO0FBQ0EsU0FBSyxPQUFMO0FBQ0UsVUFBSSxlQUFlLElBQUksRUFBSixDQUFPLEtBQVAsQ0FBYSxRQUFiLENBQXNCLGFBQXRCLENBQW9DLDRCQUFwQyxDQUFuQjtBQUNBLFVBQUcsYUFBYSxhQUFiLEtBQStCLENBQUMsQ0FBbkMsRUFBcUM7QUFDbkMsYUFBSyxhQUFMLElBQXNCLGFBQWEsT0FBYixDQUFxQixhQUFhLGFBQWxDLEVBQWlELEtBQWpELElBQTBELFFBQVEsRUFBUixHQUFhLEdBQWIsR0FBbUIsRUFBN0UsQ0FBdEI7QUFDRDtBQUNELFVBQUcsUUFBUSxFQUFSLElBQWMsYUFBYSxhQUFiLEtBQStCLENBQWhELEVBQWtEO0FBQ2hELFlBQUksRUFBSixDQUFPLEdBQVAsQ0FBVyxHQUFYLENBQWUsK0JBQWYsRUFBZ0QsT0FBaEQ7QUFDQSxlQUFPLEtBQVA7QUFDRDtBQUNELFdBQUssTUFBTCxJQUFlLEdBQWY7QUFDQTtBQUNBLGFBQU8sS0FBUCxJQUFnQixJQUFJLE9BQUosQ0FBWSxlQUFaLENBQTRCLElBQUksT0FBSixDQUFZLEtBQVosQ0FBa0IsS0FBbEIsQ0FBd0IsTUFBcEQsRUFBNEQsSUFBNUQsQ0FBaEI7QUFDQTs7QUFFRjtBQUNBLFNBQUssYUFBTDtBQUNFLFVBQUksZ0JBQWdCLElBQUksRUFBSixDQUFPLEtBQVAsQ0FBYSxRQUFiLENBQXNCLGFBQXRCLENBQW9DLGdDQUFwQyxDQUFwQjtBQUNBLFVBQUcsY0FBYyxhQUFkLEtBQWdDLENBQUMsQ0FBcEMsRUFBc0M7QUFDcEMsWUFBSSxFQUFKLENBQU8sR0FBUCxDQUFXLEdBQVgsQ0FBZSwwQkFBZixFQUEyQyxPQUEzQztBQUNBLGVBQU8sS0FBUDtBQUNEO0FBQ0QsVUFBSSxXQUFXLGNBQWMsT0FBZCxDQUFzQixjQUFjLGFBQXBDLENBQWY7QUFDQSxXQUFLLGVBQUwsSUFBd0IsU0FBUyxZQUFULENBQXNCLFdBQXRCLENBQXhCO0FBQ0EsV0FBSyxjQUFMLElBQXVCLFNBQVMsWUFBVCxDQUFzQixVQUF0QixDQUF2QjtBQUNBO0FBQ0EsV0FBSyxNQUFMLElBQWUsQ0FBQyxRQUFRLEVBQVIsR0FBYSxJQUFiLEdBQW9CLEVBQXJCLElBQTJCLEdBQTFDO0FBQ0E7QUFDQSxhQUFPLEtBQVAsSUFBZ0IsSUFBSSxPQUFKLENBQVksZUFBWixDQUE0QixJQUFJLE9BQUosQ0FBWSxLQUFaLENBQWtCLFdBQWxCLENBQThCLE1BQTFELEVBQWtFLElBQWxFLENBQWhCO0FBQ0E7QUE5Qko7O0FBaUNBO0FBQ0EsU0FBTyxLQUFQO0FBQ0EsYUFBVyxZQUFVO0FBQ25CLFdBQU8sSUFBUDtBQUNELEdBRkQsRUFFRyxFQUZIOztBQUlBO0FBQ0EsTUFBSSxPQUFKLENBQVksU0FBWjtBQUNELENBL0REOztBQWtFQTs7Ozs7O0FBTUEsSUFBSSxPQUFKLENBQVksZUFBWixHQUE4QixVQUFTLFVBQVQsRUFBcUIsSUFBckIsRUFBMEI7QUFDdEQsT0FBSSxJQUFJLE9BQVIsSUFBbUIsSUFBbkIsRUFBd0I7QUFDdEIsaUJBQWEsV0FBVyxPQUFYLENBQW1CLElBQUksTUFBSixDQUFXLFdBQVcsT0FBWCxHQUFxQixLQUFoQyxFQUF1QyxJQUF2QyxDQUFuQixFQUFpRSxLQUFLLE9BQUwsQ0FBakUsQ0FBYjtBQUNEO0FBQ0QsU0FBTyxVQUFQO0FBQ0QsQ0FMRDs7O0FDclhBOzs7O0FBSUEsSUFBSSxXQUFKLEdBQWtCO0FBQ2hCLGdCQUFjO0FBREUsQ0FBbEI7O0FBS0E7OztBQUdBLElBQUksV0FBSixDQUFnQixJQUFoQixHQUF1QixZQUFVO0FBQy9CLFVBQVEsR0FBUixDQUFZLHNCQUFaOztBQUVBO0FBQ0EsTUFBSSxXQUFKLENBQWdCLFFBQWhCLEdBQTJCLHlCQUF5QixJQUFJLElBQUosQ0FBUyxTQUFULENBQW1CLFlBQW5CLENBQWdDLFFBQXBGOztBQUVBO0FBQ0EsTUFBSSxFQUFKLENBQU8sS0FBUCxDQUFhLFlBQWIsR0FBNEIsSUFBSSxFQUFKLENBQU8sS0FBUCxDQUFhLFNBQWIsQ0FBdUIsV0FBdkIsQ0FBbUMsUUFBUTtBQUNyRSxTQUFLLElBRGdFO0FBRXJFLFVBQU07QUFDSixZQUFNO0FBREYsS0FGK0Q7QUFLckUsV0FBTyxDQUNMO0FBQ0UsV0FBSyxHQURQO0FBRUUsWUFBSztBQUNILGdCQUFRLElBQUksV0FBSixDQUFnQixRQURyQjtBQUVILGtCQUFVO0FBRlAsT0FGUDtBQU1FLGFBQU8sQ0FDTDtBQUNFLGFBQUssSUFEUDtBQUVFLGNBQU07QUFDSixpQkFBTztBQURILFNBRlI7QUFLRSxjQUFNLG1CQUxSO0FBTUUsZUFBTyxDQUNMO0FBQ0UsZUFBSyxNQURQO0FBRUUsZ0JBQU07QUFDSixnQkFBSTtBQURBLFdBRlI7QUFLRSxnQkFBTTtBQUxSLFNBREs7QUFOVCxPQURLO0FBTlQsS0FESztBQUw4RCxHQUFSLENBQW5DLENBQTVCOztBQWtDQTtBQUNBLE1BQUcsSUFBSSxJQUFKLENBQVMsU0FBVCxDQUFtQixZQUFuQixDQUFnQyxRQUFuQyxFQUE0QztBQUMxQyxRQUFJLFdBQUosQ0FBZ0IsZUFBaEIsQ0FBZ0MsSUFBaEM7QUFDRDtBQUVGLENBOUNEOztBQWlEQTs7OztBQUlBLElBQUksV0FBSixDQUFnQixlQUFoQixHQUFrQyxVQUFTLElBQVQsRUFBYztBQUM5QyxNQUFHLE9BQU8sSUFBUCxLQUFnQixXQUFuQixFQUErQjtBQUM3QixXQUFPLEtBQVA7QUFDRDs7QUFFRDtBQUNBLE1BQUksa0JBQWtCLElBQUksT0FBSixDQUFZLFVBQVMsT0FBVCxFQUFrQixNQUFsQixFQUF5Qjs7QUFFekQsU0FBSztBQUNILFdBQUssSUFBSSxXQUFKLENBQWdCLFFBRGxCO0FBRUgsWUFBTSxnQkFBVTtBQUNkLFlBQUcsS0FBSyxNQUFMLEtBQWdCLEdBQW5CLEVBQXVCO0FBQ3JCLGtCQUFRLEtBQUssWUFBYjtBQUNELFNBRkQsTUFFSztBQUNILGNBQUcsSUFBSCxFQUFRO0FBQ04sZ0JBQUksV0FBSixDQUFnQixlQUFoQixDQUFnQyxJQUFoQztBQUNEO0FBQ0Y7QUFDRjtBQVZFLEtBQUw7QUFhRCxHQWZxQixDQUF0Qjs7QUFpQkE7QUFDQSxrQkFBZ0IsSUFBaEIsQ0FBcUIsVUFBUyxJQUFULEVBQWM7QUFDakMsUUFBSSxnQkFBZ0IsQ0FBcEI7O0FBRUE7QUFDQSxRQUFJLFVBQVUsSUFBSSxNQUFKLENBQVcseUNBQXlDLElBQUksSUFBSixDQUFTLFNBQVQsQ0FBbUIsWUFBbkIsQ0FBZ0MsUUFBekUsR0FBb0YsNEJBQS9GLEVBQTZILElBQTdILENBQWQ7QUFDQSxRQUFJLFVBQVUsS0FBSyxLQUFMLENBQVcsT0FBWCxDQUFkO0FBQ0EsUUFBRyxRQUFRLENBQVIsS0FBYyxRQUFRLENBQVIsRUFBVyxLQUFYLENBQWlCLEtBQWpCLENBQWpCLEVBQXlDO0FBQ3ZDO0FBQ0Esc0JBQWdCLFFBQVEsQ0FBUixJQUFhLENBQTdCO0FBQ0Q7O0FBRUQ7QUFDQSxRQUFJLFdBQUosQ0FBZ0Isa0JBQWhCLENBQW1DLGFBQW5DOztBQUVBO0FBQ0EsUUFBRyxJQUFILEVBQVE7QUFDTixpQkFBVyxZQUFVO0FBQ25CLFlBQUksV0FBSixDQUFnQixlQUFoQixDQUFnQyxJQUFoQztBQUNELE9BRkQsRUFFRyxJQUFJLFdBQUosQ0FBZ0IsWUFGbkI7QUFHRDtBQUNGLEdBcEJEO0FBc0JELENBOUNEOztBQWlEQTs7OztBQUlBLElBQUksV0FBSixDQUFnQixrQkFBaEIsR0FBcUMsVUFBUyxTQUFULEVBQW1CO0FBQ3RELE1BQUksU0FBUyxJQUFJLEVBQUosQ0FBTyxLQUFQLENBQWEsWUFBYixDQUEwQixhQUExQixDQUF3QywyQkFBeEMsQ0FBYjtBQUNBLE1BQUcsTUFBSCxFQUFVO0FBQ1I7QUFDQSxXQUFPLFNBQVAsR0FBbUIsU0FBbkI7O0FBRUE7QUFDQSxRQUFHLGNBQWMsQ0FBakIsRUFBbUI7QUFDakIsYUFBTyxTQUFQLEdBQW1CLEVBQW5CO0FBQ0QsS0FGRCxNQUVLO0FBQ0gsYUFBTyxTQUFQLEdBQW1CLDBCQUFuQjtBQUNEO0FBRUY7QUFDRixDQWREOzs7QUN0SEE7Ozs7QUFJQSxJQUFJLGFBQUosR0FBb0IsRUFBcEI7O0FBR0E7OztBQUdBLElBQUksYUFBSixDQUFrQixJQUFsQixHQUF5QixZQUFVO0FBQ2pDLFVBQVEsR0FBUixDQUFZLHdCQUFaOztBQUVBO0FBQ0EsTUFBSSxFQUFKLENBQU8sS0FBUCxDQUFhLGNBQWIsR0FBOEIsSUFBSSxFQUFKLENBQU8sS0FBUCxDQUFhLFNBQWIsQ0FBdUIsV0FBdkIsQ0FBbUMsUUFBUTtBQUN2RSxTQUFLLElBRGtFO0FBRXZFLFVBQU07QUFDSixZQUFNO0FBREYsS0FGaUU7QUFLdkUsV0FBTyxDQUNMO0FBQ0UsV0FBSyxHQURQO0FBRUUsWUFBSztBQUNILGdCQUFRO0FBREwsT0FGUDtBQUtFLGFBQU8sQ0FDTDtBQUNFLGFBQUssSUFEUDtBQUVFLGNBQU07QUFDSixpQkFBTztBQURILFNBRlI7QUFLRSxjQUFNO0FBTFIsT0FESztBQUxULEtBREs7QUFMZ0UsR0FBUixDQUFuQyxDQUE5QjtBQXdCRCxDQTVCRDs7O0FDVkEsSUFBSSxLQUFKIiwiZmlsZSI6ImluamVjdC5kZXYuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvLy8vLy8vLy8vLy8vLy8vLy8vLy8vL1xyXG4vLyBDT1JFIC0gQU5JTUFUSU9OUyAvL1xyXG4vLy8vLy8vLy8vLy8vLy8vLy8vLy8vL1xyXG5cclxuXHJcbi8qKlxyXG4gKiBMaW5lYXIgcHJvZ3Jlc3Npb25cclxuICogQHBhcmFtICB7bnVtYmVyfSB0IFRpbWUgZWxhcHNlZFxyXG4gKiBAcGFyYW0gIHtudW1iZXJ9IGIgU3RhcnRpbmcgdmFsdWVcclxuICogQHBhcmFtICB7bnVtYmVyfSBjIFN0YXJ0IGVuZCBkaWZmZXJlbmNlXHJcbiAqIEBwYXJhbSAge251bWJlcn0gZCBEdXJhdGlvblxyXG4gKiBAcmV0dXJuIHtudW1iZXJ9ICAgRGVsdGEgdXBkYXRlZCB2YWx1ZVxyXG4gKi9cclxuTWF0aC5saW5lYXJUd2VlbiA9IGZ1bmN0aW9uKHQsIGIsIGMsIGQpe1xyXG4gIHJldHVybiBjKnQvZCArIGI7XHJcbn07XHJcblxyXG5cclxuLyoqXHJcbiAqIEN1YmljIGVhc2UtaW4gcHJvZ3Jlc3Npb25cclxuICogQHBhcmFtICB7bnVtYmVyfSB0IFRpbWUgZWxhcHNlZFxyXG4gKiBAcGFyYW0gIHtudW1iZXJ9IGIgU3RhcnRpbmcgdmFsdWVcclxuICogQHBhcmFtICB7bnVtYmVyfSBjIFN0YXJ0IGVuZCBkaWZmZXJlbmNlXHJcbiAqIEBwYXJhbSAge251bWJlcn0gZCBEdXJhdGlvblxyXG4gKiBAcmV0dXJuIHtudW1iZXJ9ICAgRGVsdGEgdXBkYXRlZCB2YWx1ZVxyXG4gKi9cclxuTWF0aC5lYXNlSW4gPSBmdW5jdGlvbih0LCBiLCBjLCBkKXtcclxuICB0IC89IGQ7XHJcbiAgcmV0dXJuIGMqdCp0KnQgKyBiO1xyXG59O1xyXG5cclxuXHJcbi8qKlxyXG4gKiBDdWJpYyBlYXNlLW91dCBwcm9ncmVzc2lvblxyXG4gKiBAcGFyYW0gIHtudW1iZXJ9IHQgVGltZSBlbGFwc2VkXHJcbiAqIEBwYXJhbSAge251bWJlcn0gYiBTdGFydGluZyB2YWx1ZVxyXG4gKiBAcGFyYW0gIHtudW1iZXJ9IGMgU3RhcnQgZW5kIGRpZmZlcmVuY2VcclxuICogQHBhcmFtICB7bnVtYmVyfSBkIER1cmF0aW9uXHJcbiAqIEByZXR1cm4ge251bWJlcn0gICBEZWx0YSB1cGRhdGVkIHZhbHVlXHJcbiAqL1xyXG5NYXRoLmVhc2VPdXQgPSBmdW5jdGlvbih0LCBiLCBjLCBkKXtcclxuICB0IC89IGQ7XHJcbiAgdC0tO1xyXG4gIHJldHVybiBjKih0KnQqdCArIDEpICsgYjtcclxufTtcclxuXHJcblxyXG4vKipcclxuICogQ3ViaWMgZWFzZS1pbi1vdXQgcHJvZ3Jlc3Npb25cclxuICogQHBhcmFtICB7bnVtYmVyfSB0IFRpbWUgZWxhcHNlZFxyXG4gKiBAcGFyYW0gIHtudW1iZXJ9IGIgU3RhcnRpbmcgdmFsdWVcclxuICogQHBhcmFtICB7bnVtYmVyfSBjIFN0YXJ0IGVuZCBkaWZmZXJlbmNlXHJcbiAqIEBwYXJhbSAge251bWJlcn0gZCBEdXJhdGlvblxyXG4gKiBAcmV0dXJuIHtudW1iZXJ9ICAgRGVsdGEgdXBkYXRlZCB2YWx1ZVxyXG4gKi9cclxuTWF0aC5lYXNlSW5PdXQgPSBmdW5jdGlvbih0LCBiLCBjLCBkKXtcclxuICB0IC89IGQvMjtcclxuICBpZiAodCA8IDEpIHJldHVybiBjLzIqdCp0KnQgKyBiO1xyXG4gIHQgLT0gMjtcclxuICByZXR1cm4gYy8yKih0KnQqdCArIDIpICsgYjtcclxufTtcclxuXHJcblxyXG4vKipcclxuICogRmFkZSBpbi9vdXQgd2l0aCBkdXJhdGlvbiBhbmQgY2FsbGJhY2sgc2V0dGluZ3NcclxuICogQHBhcmFtICB7c3RyaW5nfSAgIGRpcmVjdGlvbiBpbnxvdXRcclxuICogQHBhcmFtICB7b2JqZWN0fSAgIGVsZW1lbnQgICBET00gbm9kZVxyXG4gKiBAcGFyYW0gIHtudW1iZXJ9ICAgZHVyYXRpb24gIEFuaW1hdGlvbiBpbiBtc1xyXG4gKiBAcGFyYW0gIHtzdHJpbmd9ICAgZWFzaW5nICAgIEVhc2luZyB0aW1pbmcgZnVuY3Rpb24gbGluZWFyfGVhc2VJbnxlYXNlT3V0fGVhc2VJbk91dFxyXG4gKiBAcGFyYW0gIHtmdW5jdGlvbn0gZm4gICAgICAgIENhbGxiYWNrICh3aXRoIGVsZW1lbnQgYXMgc2NvcGUpXHJcbiAqIEByZXR1cm4ge2Jvb2xlYW59XHJcbiAqL1xyXG5mdW5jdGlvbiBfZmFkZShkaXJlY3Rpb24sIGVsZW1lbnQsIGR1cmF0aW9uLCBlYXNpbmcsIGZuKXtcclxuICB2YXIgRlJPTSxcclxuICAgICAgVE8sXHJcbiAgICAgIFNUQVJUID0gbmV3IERhdGUoKS5nZXRUaW1lKCksXHJcbiAgICAgIERJRkYsXHJcbiAgICAgIG1vZFNoaWZ0LFxyXG4gICAgICBjYWxsYmFjaztcclxuXHJcbiAgLy8gUHJldmVudCBwcm9wYWdhdGlvblxyXG4gIGlmKGVsZW1lbnQuZ2V0QXR0cmlidXRlKCdkYXRhLWZhZGUnKSl7XHJcbiAgICByZXR1cm4gZmFsc2U7XHJcbiAgfVxyXG4gIGVsZW1lbnQuc2V0QXR0cmlidXRlKCdkYXRhLWZhZGUnLCB0cnVlKTtcclxuXHJcbiAgLy8gRWFzaW5nIHRpbWluZyBmdW5jdGlvblxyXG4gIHN3aXRjaChlYXNpbmcpe1xyXG4gICAgY2FzZSAnbGluZWFyJzpcclxuICAgICAgZWFzaW5nID0gTWF0aC5saW5lYXJUd2VlbjtcclxuICAgICAgYnJlYWs7XHJcblxyXG4gICAgY2FzZSAnZWFzZUluJzpcclxuICAgICAgZWFzaW5nID0gTWF0aC5lYXNlSW47XHJcbiAgICAgIGJyZWFrO1xyXG5cclxuICAgIGNhc2UgJ2Vhc2VJbk91dCc6XHJcbiAgICAgIGVhc2luZyA9IE1hdGguZWFzZUluT3V0O1xyXG4gICAgICBicmVhaztcclxuXHJcbiAgICBkZWZhdWx0OiAvLyBlYXNlT3V0XHJcbiAgICAgIGVhc2luZyA9IE1hdGguZWFzZU91dDtcclxuICAgICAgYnJlYWs7XHJcbiAgfVxyXG5cclxuICAvLyBTZXQgZGVmYXVsdCBvcGFjaXR5XHJcbiAgaWYoIWVsZW1lbnQuc3R5bGUub3BhY2l0eSl7XHJcbiAgICBlbGVtZW50LnN0eWxlLm9wYWNpdHkgPSAoZGlyZWN0aW9uID09ICdpbicgPyAwIDogMSk7XHJcbiAgfVxyXG5cclxuICAvLyBEaXJlY3Rpb25hbCBkZWZhdWx0c1xyXG4gIEZST00gPSAxO1xyXG4gIFRPICAgPSAwO1xyXG4gIGlmKGRpcmVjdGlvbiAhPSAnb3V0Jyl7XHJcbiAgICBkaXJlY3Rpb24gICAgICAgICAgICAgPSAnaW4nO1xyXG4gICAgZWxlbWVudC5zdHlsZS5kaXNwbGF5ID0gJ2Jsb2NrJztcclxuICAgIEZST00gICAgICAgICAgICAgICAgICA9IDA7XHJcbiAgICBUTyAgICAgICAgICAgICAgICAgICAgPSAxO1xyXG4gIH1cclxuICBESUZGID0gVE8gLSBGUk9NO1xyXG5cclxuICAvLyBEdXJhdGlvblxyXG4gIGlmKHR5cGVvZiBkdXJhdGlvbiAhPT0gJ251bWJlcicpe1xyXG4gICAgZHVyYXRpb24gPSAzMDA7XHJcbiAgfVxyXG4gIGR1cmF0aW9uID0gTWF0aC5hYnMoZHVyYXRpb24pO1xyXG5cclxuICAvLyBDYWxsYmFja1xyXG4gIGNhbGxiYWNrID0gZnVuY3Rpb24oKXtcclxuICAgIGVsZW1lbnQucmVtb3ZlQXR0cmlidXRlKCdkYXRhLWZhZGUnKTtcclxuICAgIGlmKHR5cGVvZiBmbiA9PT0gJ2Z1bmN0aW9uJyl7XHJcbiAgICAgIGZuLmNhbGwoZWxlbWVudCk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICAvLyBNb2RpZmllclxyXG4gIG1vZFNoaWZ0ID0gZnVuY3Rpb24oKXtcclxuICAgIC8vIFVwZGF0ZSB2YWx1ZXNcclxuICAgIHZhciBDVVJSRU5UID0gbmV3IERhdGUoKS5nZXRUaW1lKCkgLSBTVEFSVCxcclxuICAgICAgICBVUERBVEUgID0gZWFzaW5nKENVUlJFTlQsIEZST00sIERJRkYsIGR1cmF0aW9uKTtcclxuICAgIGVsZW1lbnQuc3R5bGUub3BhY2l0eSA9IFVQREFURTtcclxuXHJcbiAgICBpZihVUERBVEUgPT0gVE8gfHwgQ1VSUkVOVCA+PSBkdXJhdGlvbil7XHJcbiAgICAgIC8vIERvbmVcclxuICAgICAgaWYoZGlyZWN0aW9uID09ICdvdXQnKXtcclxuICAgICAgICAvLyBIaWRlIG9uIGZhZGUgb3V0XHJcbiAgICAgICAgZWxlbWVudC5zdHlsZS5kaXNwbGF5ID0gJ25vbmUnO1xyXG4gICAgICB9XHJcblxyXG4gICAgICAvLyBSZXNldCB0cmFuc2l0aW9uYWwgQ1NTXHJcbiAgICAgIGVsZW1lbnQuc3R5bGUub3BhY2l0eSA9ICcnO1xyXG5cclxuICAgICAgLy8gRW5kXHJcbiAgICAgIGNhbGxiYWNrKCk7XHJcbiAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgfVxyXG4gICAgcmVxdWVzdEFuaW1hdGlvbkZyYW1lKG1vZFNoaWZ0KTtcclxuICB9XHJcblxyXG4gIC8vIFRyaWdnZXJcclxuICBtb2RTaGlmdCgpO1xyXG59XHJcblxyXG5cclxuLyoqXHJcbiAqIFNsaWRlIHVwL2Rvd24gd2l0aCBkdXJhdGlvbiBhbmQgY2FsbGJhY2sgc2V0dGluZ3NcclxuICogQHBhcmFtICB7c3RyaW5nfSAgIGRpcmVjdGlvbiB1cHxkb3duXHJcbiAqIEBwYXJhbSAge29iamVjdH0gICBlbGVtZW50ICAgRE9NIG5vZGVcclxuICogQHBhcmFtICB7bnVtYmVyfSAgIGR1cmF0aW9uICBBbmltYXRpb24gaW4gbXNcclxuICogQHBhcmFtICB7c3RyaW5nfSAgIGVhc2luZyAgICBFYXNpbmcgdGltaW5nIGZ1bmN0aW9uIGxpbmVhcnxlYXNlSW58ZWFzZU91dHxlYXNlSW5PdXRcclxuICogQHBhcmFtICB7ZnVuY3Rpb259IGZuICAgICAgICBDYWxsYmFjayAod2l0aCBlbGVtZW50IGFzIHNjb3BlKVxyXG4gKiBAcmV0dXJuIHtib29sZWFufVxyXG4gKi9cclxuZnVuY3Rpb24gX3NsaWRlKGRpcmVjdGlvbiwgZWxlbWVudCwgZHVyYXRpb24sIGVhc2luZywgZGlzcGxheV9yZW1vdmUsIGZuKXtcclxuICB2YXIgRlJPTSxcclxuICAgICAgVE8sXHJcbiAgICAgIFNUQVJUID0gbmV3IERhdGUoKS5nZXRUaW1lKCksXHJcbiAgICAgIERJRkYsXHJcbiAgICAgIG1vZFNoaWZ0LFxyXG4gICAgICBjYWxsYmFjaztcclxuXHJcbiAgLy8gUHJldmVudCBwcm9wYWdhdGlvblxyXG4gIGlmKGVsZW1lbnQuZ2V0QXR0cmlidXRlKCdkYXRhLXNsaWRlJykpe1xyXG4gICAgcmV0dXJuIGZhbHNlO1xyXG4gIH1cclxuICBlbGVtZW50LnNldEF0dHJpYnV0ZSgnZGF0YS1zbGlkZScsIHRydWUpO1xyXG5cclxuICAvLyBTZXQgaW5pdGlhbCBjc3NcclxuICBpZihkaXNwbGF5X3JlbW92ZSl7XHJcbiAgICBlbGVtZW50LnN0eWxlLmRpc3BsYXkgID0gJ2Jsb2NrJztcclxuICB9XHJcbiAgZWxlbWVudC5zdHlsZS5vdmVyZmxvdyA9ICdoaWRkZW4nO1xyXG4gIGVsZW1lbnQuc3R5bGUuaGVpZ2h0ICAgPSAoZGlyZWN0aW9uID09ICd1cCcgPyBlbGVtZW50LnNjcm9sbEhlaWdodCArICdweCcgOiAwICsgJ3B4Jyk7XHJcblxyXG4gIC8vIERpcmVjdGlvbmFsIGRlZmF1bHRzXHJcbiAgRlJPTSA9IGVsZW1lbnQuc2Nyb2xsSGVpZ2h0O1xyXG4gIFRPICAgPSAwO1xyXG4gIGlmKGRpcmVjdGlvbiAhPSAndXAnKXtcclxuICAgIGRpcmVjdGlvbiA9ICdkb3duJztcclxuICAgIEZST00gICAgICA9IDA7XHJcbiAgICBUTyAgICAgICAgPSBlbGVtZW50LnNjcm9sbEhlaWdodDtcclxuICB9XHJcbiAgRElGRiA9IFRPIC0gRlJPTTtcclxuXHJcbiAgLy8gRHVyYXRpb25cclxuICBpZih0eXBlb2YgZHVyYXRpb24gIT09ICdudW1iZXInKXtcclxuICAgIGR1cmF0aW9uID0gMzAwO1xyXG4gIH1cclxuICBkdXJhdGlvbiA9IE1hdGguYWJzKGR1cmF0aW9uKTtcclxuXHJcbiAgLy8gRWFzaW5nIHRpbWluZyBmdW5jdGlvblxyXG4gIHN3aXRjaChlYXNpbmcpe1xyXG4gICAgY2FzZSAnbGluZWFyJzpcclxuICAgICAgZWFzaW5nID0gTWF0aC5saW5lYXJUd2VlbjtcclxuICAgICAgYnJlYWs7XHJcblxyXG4gICAgY2FzZSAnZWFzZUluJzpcclxuICAgICAgZWFzaW5nID0gTWF0aC5lYXNlSW47XHJcbiAgICAgIGJyZWFrO1xyXG5cclxuICAgIGNhc2UgJ2Vhc2VJbk91dCc6XHJcbiAgICAgIGVhc2luZyA9IE1hdGguZWFzZUluT3V0O1xyXG4gICAgICBicmVhaztcclxuXHJcbiAgICBkZWZhdWx0OiAvLyBlYXNlT3V0XHJcbiAgICAgIGVhc2luZyA9IE1hdGguZWFzZU91dDtcclxuICAgICAgYnJlYWs7XHJcbiAgfVxyXG5cclxuICAvLyBDYWxsYmFja1xyXG4gIGNhbGxiYWNrID0gZnVuY3Rpb24oKXtcclxuICAgIGVsZW1lbnQucmVtb3ZlQXR0cmlidXRlKCdkYXRhLXNsaWRlJyk7XHJcbiAgICBpZih0eXBlb2YgZm4gPT09ICdmdW5jdGlvbicpe1xyXG4gICAgICBmbi5jYWxsKGVsZW1lbnQpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgLy8gTW9kaWZpZXJcclxuICBtb2RTaGlmdCA9IGZ1bmN0aW9uKCl7XHJcbiAgICAvLyBVcGRhdGUgdmFsdWVzXHJcbiAgICB2YXIgQ1VSUkVOVCA9IG5ldyBEYXRlKCkuZ2V0VGltZSgpIC0gU1RBUlQsXHJcbiAgICAgICAgVVBEQVRFICA9IGVhc2luZyhDVVJSRU5ULCBGUk9NLCBESUZGLCBkdXJhdGlvbik7XHJcbiAgICBlbGVtZW50LnN0eWxlLmhlaWdodCA9IFVQREFURSArICdweCc7XHJcblxyXG4gICAgaWYoVVBEQVRFID09IFRPIHx8IENVUlJFTlQgPj0gZHVyYXRpb24pe1xyXG4gICAgICAvLyBEb25lXHJcbiAgICAgIGlmKGRpcmVjdGlvbiA9PSAndXAnKXtcclxuICAgICAgICAvLyBIaWRlIG9uIHNsaWRlIHVwXHJcbiAgICAgICAgaWYoZGlzcGxheV9yZW1vdmUpe1xyXG4gICAgICAgICAgZWxlbWVudC5zdHlsZS5kaXNwbGF5ID0gJ25vbmUnO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG5cclxuICAgICAgLy8gUmVzZXQgdHJhbnNpdGlvbmFsIENTU1xyXG4gICAgICBlbGVtZW50LnN0eWxlLm92ZXJmbG93ID0gJyc7XHJcbiAgICAgIGlmKGRpc3BsYXlfcmVtb3ZlKXtcclxuICAgICAgICBlbGVtZW50LnN0eWxlLmhlaWdodCA9ICcnO1xyXG4gICAgICB9XHJcblxyXG4gICAgICAvLyBFbmRcclxuICAgICAgY2FsbGJhY2soKTtcclxuICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICB9XHJcbiAgICByZXF1ZXN0QW5pbWF0aW9uRnJhbWUobW9kU2hpZnQpO1xyXG4gIH1cclxuXHJcbiAgLy8gVHJpZ2dlclxyXG4gIG1vZFNoaWZ0KCk7XHJcbn1cclxuXHJcblxyXG4vKipcclxuICogU2Nyb2xsIHdpbmRvdyB0byBkZXN0aW5hdGlvbiB3aXRoIGR1cmF0aW9uIGFuZCBjYWxsYmFjayBzZXR0aW5nc1xyXG4gKiBAcGFyYW0gIHttaXhlZH0gICAgZGVzdGluYXRpb24gU2Nyb2xsIHRvIHBpeGVsIHZhbHVlLCBzZWxlY3RvciBvciBET00gbm9kZVxyXG4gKiBAcGFyYW0gIHtudW1iZXJ9ICAgZHVyYXRpb24gICAgQW5pbWF0aW9uIGluIG1zXHJcbiAqIEBwYXJhbSAge3N0cmluZ30gICBlYXNpbmcgICAgICBFYXNpbmcgdGltaW5nIGZ1bmN0aW9uIGxpbmVhcnxlYXNlSW58ZWFzZU91dHxlYXNlSW5PdXRcclxuICogQHBhcmFtICB7ZnVuY3Rpb259IGZuICAgICAgICAgIENhbGxiYWNrIGZ1bmN0aW9uXHJcbiAqIEByZXR1cm4ge2Jvb2xlYW59XHJcbiAqL1xyXG5mdW5jdGlvbiBfc2Nyb2xsVG8oZGVzdGluYXRpb24sIGR1cmF0aW9uLCBlYXNpbmcsIGZuKXtcclxuICB2YXIgRlJPTSxcclxuICAgICAgVE8sXHJcbiAgICAgIFNUQVJUID0gbmV3IERhdGUoKS5nZXRUaW1lKCksXHJcbiAgICAgIERJRkYsXHJcbiAgICAgIG1vZFNoaWZ0LFxyXG4gICAgICBjYWxsYmFjayxcclxuICAgICAgZWxlbWVudCA9IGRvY3VtZW50LmJvZHk7XHJcblxyXG4gIC8vIFByZXZlbnQgcHJvcGFnYXRpb25cclxuICBpZihlbGVtZW50LmdldEF0dHJpYnV0ZSgnZGF0YS1zY3JvbGxpbmcnKSl7XHJcbiAgICByZXR1cm4gZmFsc2U7XHJcbiAgfVxyXG4gIGVsZW1lbnQuc2V0QXR0cmlidXRlKCdkYXRhLXNjcm9sbGluZycsIHRydWUpO1xyXG5cclxuICAvLyBEZXN0aW5hdGlvblxyXG4gIHN3aXRjaCh0eXBlb2YgZGVzdGluYXRpb24pe1xyXG4gICAgY2FzZSAnbnVtYmVyJzpcclxuICAgICAgZGVzdGluYXRpb24gPSBkZXN0aW5hdGlvbjtcclxuICAgICAgYnJlYWs7XHJcblxyXG4gICAgY2FzZSAnc3RyaW5nJzpcclxuICAgICAgdmFyIGVsZW0gPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKGRlc3RpbmF0aW9uKTtcclxuICAgICAgaWYoIWVsZW0peyByZXR1cm4gZmFsc2U7IH1cclxuICAgICAgZGVzdGluYXRpb24gPSBlbGVtLmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpLnRvcDtcclxuICAgICAgYnJlYWs7XHJcblxyXG4gICAgY2FzZSAnb2JqZWN0JzpcclxuICAgICAgaWYoIWRlc3RpbmF0aW9uLm5vZGVUeXBlKXsgcmV0dXJuIGZhbHNlOyB9XHJcbiAgICAgIGRlc3RpbmF0aW9uID0gZGVzdGluYXRpb24uZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCkudG9wO1xyXG4gICAgICBicmVhaztcclxuXHJcbiAgICBkZWZhdWx0OlxyXG4gICAgICByZXR1cm4gZmFsc2U7XHJcbiAgfVxyXG5cclxuICAvLyBQcm9jZXNzIGJlbG93IG1heCBmb2xkXHJcbiAgdmFyIF9ib2R5ICAgPSBkb2N1bWVudC5ib2R5LFxyXG4gICAgICBfaHRtbCAgID0gZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LFxyXG4gICAgICBfaGVpZ2h0ID0gTWF0aC5tYXgoX2JvZHkuc2Nyb2xsSGVpZ2h0LCBfYm9keS5vZmZzZXRIZWlnaHQsIF9odG1sLmNsaWVudEhlaWdodCwgX2h0bWwuc2Nyb2xsSGVpZ2h0LCBfaHRtbC5vZmZzZXRIZWlnaHQpO1xyXG4gIGRlc3RpbmF0aW9uID0gTWF0aC5tYXgoMCwgTWF0aC5taW4oZGVzdGluYXRpb24sIF9oZWlnaHQgLSB3aW5kb3cuaW5uZXJIZWlnaHQpKTtcclxuXHJcbiAgLy8gRGlyZWN0aW9uYWwgZGVmYXVsdHNcclxuICBGUk9NID0gd2luZG93LnNjcm9sbFk7XHJcbiAgVE8gICA9IGRlc3RpbmF0aW9uO1xyXG4gIERJRkYgPSBUTyAtIEZST007XHJcblxyXG4gIC8vIER1cmF0aW9uXHJcbiAgaWYodHlwZW9mIGR1cmF0aW9uICE9PSAnbnVtYmVyJyl7XHJcbiAgICBkdXJhdGlvbiA9IDEwMDA7XHJcbiAgfVxyXG4gIGR1cmF0aW9uID0gTWF0aC5hYnMoZHVyYXRpb24pO1xyXG5cclxuICAvLyBFYXNpbmcgdGltaW5nIGZ1bmN0aW9uXHJcbiAgc3dpdGNoKGVhc2luZyl7XHJcbiAgICBjYXNlICdsaW5lYXInOlxyXG4gICAgICBlYXNpbmcgPSBNYXRoLmxpbmVhclR3ZWVuO1xyXG4gICAgICBicmVhaztcclxuXHJcbiAgICBjYXNlICdlYXNlSW4nOlxyXG4gICAgICBlYXNpbmcgPSBNYXRoLmVhc2VJbjtcclxuICAgICAgYnJlYWs7XHJcblxyXG4gICAgY2FzZSAnZWFzZUluT3V0JzpcclxuICAgICAgZWFzaW5nID0gTWF0aC5lYXNlSW5PdXQ7XHJcbiAgICAgIGJyZWFrO1xyXG5cclxuICAgIGRlZmF1bHQ6IC8vIGVhc2VPdXRcclxuICAgICAgZWFzaW5nID0gTWF0aC5lYXNlT3V0O1xyXG4gICAgICBicmVhaztcclxuICB9XHJcblxyXG4gIC8vIENhbGxiYWNrXHJcbiAgY2FsbGJhY2sgPSBmdW5jdGlvbigpe1xyXG4gICAgZWxlbWVudC5yZW1vdmVBdHRyaWJ1dGUoJ2RhdGEtc2Nyb2xsaW5nJyk7XHJcbiAgICBpZih0eXBlb2YgZm4gPT09ICdmdW5jdGlvbicpe1xyXG4gICAgICBmbi5jYWxsKGVsZW1lbnQpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgLy8gTW9kaWZpZXJcclxuICBtb2RTaGlmdCA9IGZ1bmN0aW9uKCl7XHJcbiAgICAvLyBVcGRhdGUgdmFsdWVzXHJcbiAgICB2YXIgQ1VSUkVOVCA9IG5ldyBEYXRlKCkuZ2V0VGltZSgpIC0gU1RBUlQsXHJcbiAgICAgICAgVVBEQVRFICA9IGVhc2luZyhDVVJSRU5ULCBGUk9NLCBESUZGLCBkdXJhdGlvbik7XHJcbiAgICB3aW5kb3cuc2Nyb2xsVG8od2luZG93LnNjcm9sbFgsIFVQREFURSk7XHJcblxyXG4gICAgaWYoVVBEQVRFID09IFRPIHx8IENVUlJFTlQgPj0gZHVyYXRpb24pe1xyXG4gICAgICAvLyBEb25lXHJcbiAgICAgIGNhbGxiYWNrKCk7XHJcbiAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgfVxyXG4gICAgcmVxdWVzdEFuaW1hdGlvbkZyYW1lKG1vZFNoaWZ0KTtcclxuICB9XHJcblxyXG4gIC8vIFRyaWdnZXJcclxuICBtb2RTaGlmdCgpO1xyXG59IiwiLy8vLy8vLy8vLy8vLy8vL1xyXG4vLyBDT1JFIC0gRE9NIC8vXHJcbi8vLy8vLy8vLy8vLy8vLy9cclxuXHJcblxyXG4vKipcclxuICogRE9NIG9iamVjdCByZW5kZXJlciB3aXRoIGV2ZW50IGhvb2tzXHJcbiAqIEBwYXJhbSAge29iamVjdH0gbWFwIERPTSBtYXAgdGFnKnxhdHRyfGNzc3xodG1sfGV2ZW50c3xub2Rlc1xyXG4gKiBAcmV0dXJuIHtvYmplY3R9ICAgICBET00gb2JqZWN0IGZvciBhcHBlbmQvbWFuaXB1bGF0aW9uXHJcbiAqL1xyXG5mdW5jdGlvbiBfcmVuZGVyKG1hcCl7XHJcbiAgdmFyIGJ1aWxkID0gZnVuY3Rpb24obWFwKXtcclxuICAgIHZhciBlbGVtLFxyXG4gICAgICAgIGNzcztcclxuXHJcbiAgICAvLyBJbml0XHJcbiAgICBpZighbWFwLnRhZyl7XHJcbiAgICAgIHJldHVybiBmYWxzZTtcclxuICAgIH1cclxuICAgIGlmKG1hcC5ucyl7XHJcbiAgICAgIC8vIENyZWF0ZSBlbGVtZW50IHdpdGggbmFtZXNwYWNlXHJcbiAgICAgIGVsZW0gPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50TlMobWFwLm5zLCBtYXAudGFnKTtcclxuICAgIH1lbHNle1xyXG4gICAgICBlbGVtID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudChtYXAudGFnKTtcclxuICAgIH1cclxuXHJcbiAgICAvLyBBdHRyaWJ1dGVzXHJcbiAgICBpZih0eXBlb2YgbWFwLmF0dHIgPT09ICdvYmplY3QnKXtcclxuICAgICAgZm9yKHZhciBhdHRyaWJ1dGUgaW4gbWFwLmF0dHIpe1xyXG4gICAgICAgIGVsZW0uc2V0QXR0cmlidXRlKGF0dHJpYnV0ZSwgbWFwLmF0dHJbYXR0cmlidXRlXSk7XHJcbiAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAvLyBDU1NcclxuICAgIGlmKHR5cGVvZiBtYXAuY3NzID09PSAnb2JqZWN0Jyl7XHJcbiAgICAgIGNzcyA9ICcnO1xyXG4gICAgICBmb3IodmFyIHByb3AgaW4gbWFwLmNzcyl7XHJcbiAgICAgICAgY3NzICs9IHByb3AgKyAnOiAnICsgbWFwLmNzc1twcm9wXSArICc7ICc7XHJcbiAgICAgIH1cclxuICAgICAgZWxlbS5zZXRBdHRyaWJ1dGUoJ3N0eWxlJywgY3NzKTtcclxuICAgIH1cclxuXHJcbiAgICAvLyBJbm5lciBIVE1MXHJcbiAgICBpZihtYXAuaHRtbCl7XHJcbiAgICAgIGVsZW0uaW5uZXJIVE1MID0gbWFwLmh0bWw7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gRXZlbnRzXHJcbiAgICBpZih0eXBlb2YgbWFwLmV2ZW50cyA9PT0gJ29iamVjdCcpe1xyXG4gICAgICBtYXAuZXZlbnRzLmZvckVhY2goZnVuY3Rpb24oX2V2ZW50KXtcclxuICAgICAgICB2YXIgYyA9IGZ1bmN0aW9uKGUpe1xyXG4gICAgICAgICAgX2V2ZW50Lmxpc3RlbmVyLmNhbGwoZWxlbSwgZSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGVsZW0uYWRkRXZlbnRMaXN0ZW5lcihfZXZlbnQudHlwZSwgYyk7XHJcbiAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIENoaWxkcmVuXHJcbiAgICBpZihtYXAubm9kZXMpe1xyXG4gICAgICBtYXAubm9kZXMuZm9yRWFjaChmdW5jdGlvbihub2RlKXtcclxuICAgICAgICB2YXIgY2hpbGQgPSBidWlsZChub2RlKTtcclxuICAgICAgICBlbGVtLmFwcGVuZENoaWxkKGNoaWxkKTtcclxuICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgcmV0dXJuIGVsZW07XHJcbiAgfVxyXG5cclxuICAvLyBSZXR1cm4gcmVuZGVyZWQgZWxlbWVudCBET00gdHJlZVxyXG4gIHJldHVybiBidWlsZChtYXApO1xyXG59IiwiLy8vLy8vLy8vLy8vLy8vLy9cclxuLy8gQ09SRSAtIERBVEEgLy9cclxuLy8vLy8vLy8vLy8vLy8vLy9cclxuXHJcblxyXG4vKipcclxuICogRmxleGlibGUgQUpBWCByZXF1ZXN0IHdpdGggY2FsbGJhY2tcclxuICogQHBhcmFtICB7b2JqZWN0fSBvcHRpb25zIHVybHxkYXRhfG1ldGhvZHxhc3luY3xsb2FkXHJcbiAqL1xyXG5mdW5jdGlvbiBfeGhyKG9wdGlvbnMpe1xyXG4gIHZhciBYSFIsXHJcbiAgICAgIFBBUkFNUyxcclxuICAgICAgY2FsbGJhY2s7XHJcblxyXG4gIC8vIFRocm93IGludmFsaWRcclxuICBpZih0eXBlb2Ygb3B0aW9ucyAhPT0gJ29iamVjdCcpe1xyXG4gICAgcmV0dXJuIGZhbHNlO1xyXG4gIH1cclxuICBpZih0eXBlb2Ygb3B0aW9ucy51cmwgIT09ICdzdHJpbmcnKXtcclxuICAgIHJldHVybiBmYWxzZTtcclxuICB9XHJcblxyXG4gIC8vIE1ldGhvZFxyXG4gIGlmKHR5cGVvZiBvcHRpb25zLm1ldGhvZCAhPT0gJ3N0cmluZycpe1xyXG4gICAgLy8gU2V0IGRlZmF1bHRcclxuICAgIG9wdGlvbnMubWV0aG9kID0gJ0dFVCc7XHJcbiAgfWVsc2V7XHJcbiAgICBvcHRpb25zLm1ldGhvZCA9IG9wdGlvbnMubWV0aG9kLnRvVXBwZXJDYXNlKCk7XHJcbiAgfVxyXG5cclxuICAvLyBJbml0aWFsaXplXHJcbiAgWEhSID0gbmV3IFhNTEh0dHBSZXF1ZXN0KCk7XHJcbiAgWEhSLm9wZW4ob3B0aW9ucy5tZXRob2QsIG9wdGlvbnMudXJsLCBvcHRpb25zLmFzeW5jKTtcclxuXHJcbiAgLy8gRGF0YSBvYmplY3QgdG8gc3RyaW5nXHJcbiAgaWYodHlwZW9mIG9wdGlvbnMuZGF0YSA9PT0gJ29iamVjdCcpe1xyXG4gICAgZm9yKHZhciBrZXkgaW4gb3B0aW9ucy5kYXRhKXtcclxuICAgICAgUEFSQU1TICs9ICcmJyArIGVuY29kZVVSSUNvbXBvbmVudChrZXkpICsgJz0nICsgZW5jb2RlVVJJQ29tcG9uZW50KG9wdGlvbnMuZGF0YVtrZXldKTtcclxuICAgIH1cclxuICAgIFBBUkFNUyA9IFBBUkFNUy5yZXBsYWNlKC9eJi8sICc/Jyk7XHJcbiAgfVxyXG5cclxuICAvLyBEYXRhIGFuZCBoZWFkZXJzXHJcbiAgaWYob3B0aW9ucy5tZXRob2QgPT0gJ1BPU1QnKXtcclxuICAgIFhIUi5zZXRSZXF1ZXN0SGVhZGVyKFwiQ29udGVudC10eXBlXCIsIFwiYXBwbGljYXRpb24veC13d3ctZm9ybS11cmxlbmNvZGVkXCIpO1xyXG4gICAgLy9YSFIuc2V0UmVxdWVzdEhlYWRlcihcIkNvbm5lY3Rpb25cIiwgXCJjbG9zZWRcIik7XHJcbiAgICAvL1hIUi5zZXRSZXF1ZXN0SGVhZGVyKFwiQ29udGVudC1sZW5ndGhcIiwgUEFSQU1TLmxlbmd0aCk7XHJcbiAgfVxyXG4gIGlmKG9wdGlvbnMubWV0aG9kID09ICdHRVQnKXtcclxuICAgIG9wdGlvbnMudXJsICs9IChQQVJBTVMgPyBQQVJBTVMgOiAnJyk7XHJcbiAgfVxyXG5cclxuICAvLyBDYWxscyBjYWxsYmFjayBpZiBhdmFpbGFibGU7XHJcbiAgY2FsbGJhY2sgPSBmdW5jdGlvbigpe1xyXG4gICAgaWYodHlwZW9mIG9wdGlvbnMubG9hZCA9PT0gJ2Z1bmN0aW9uJyl7XHJcbiAgICAgIG9wdGlvbnMubG9hZC5jYWxsKHRoaXMpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgLy8gQXN5bmNocm9ub3VzXHJcbiAgaWYodHlwZW9mIG9wdGlvbnMuYXN5bmMgIT09ICdib29sZWFuJyl7XHJcbiAgICAvLyBTZXQgZGVmYXVsdFxyXG4gICAgb3B0aW9ucy5hc3luYyA9IHRydWU7XHJcbiAgfVxyXG5cclxuICAvLyBFeGVjdXRlXHJcbiAgWEhSLm9ucmVhZHlzdGF0ZWNoYW5nZSA9IGZ1bmN0aW9uKCl7XHJcbiAgICBpZihYSFIucmVhZHlTdGF0ZSA9PSA0ICYmIFhIUi5zdGF0dXMgPT0gMjAwKXtcclxuICAgICAgY2FsbGJhY2suY2FsbChYSFIpO1xyXG4gICAgfVxyXG4gIH1cclxuICBYSFIuc2VuZChQQVJBTVMpO1xyXG59IiwidmFyIHJtdCA9IHt9O1xyXG5cclxucm10LmluaXQgPSBmdW5jdGlvbigpe1xyXG4gIGNvbnNvbGUubG9nKCdybXQgaW5pdCcpO1xyXG4gIGNvbnNvbGUubG9nKHJtdC5kYXRhKTtcclxuICBybXQudWkuaW5pdCgpO1xyXG59XHJcblxyXG4vKipcclxuICogR2V0IHJlcXVpcmVkIGRhdGEgYW5kIGluaXQgb24gcmVhZHlcclxuICovXHJcbnJtdC5zZXR1cCA9IGZ1bmN0aW9uKCl7XHJcbiAgLy8gRGF0YSBvYmpcclxuICB2YXIgYXBpRGF0YSA9IHtcclxuICAgIHNpdGVfZGF0YTogZmFsc2UsXHJcbiAgICB1c2VyX2RhdGE6IGZhbHNlLFxyXG4gIH1cclxuXHJcbiAgLy8gR2V0IHNpdGUgZGF0YVxyXG4gIHZhciBzaXRlX2RhdGEgPSBuZXcgUHJvbWlzZShmdW5jdGlvbihyZXNvbHZlLCByZWplY3Qpe1xyXG5cclxuICAgIF94aHIoe1xyXG4gICAgICB1cmw6ICcvZm9ydW0vc2l0ZS5qc29uJyxcclxuICAgICAgbG9hZDogZnVuY3Rpb24oKXtcclxuICAgICAgICB2YXIgZGF0YSA9IEpTT04ucGFyc2UodGhpcy5yZXNwb25zZVRleHQpO1xyXG4gICAgICAgIGlmKHRoaXMuc3RhdHVzID09PSAyMDAgJiYgdHlwZW9mIGRhdGEgPT09ICdvYmplY3QnKXtcclxuICAgICAgICAgIHJlc29sdmUoZGF0YSk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9KTtcclxuXHJcbiAgfSk7XHJcblxyXG4gIC8vIEdldCB1c2VyIGRhdGFcclxuICB2YXIgdXNlcl9kYXRhID0gbmV3IFByb21pc2UoZnVuY3Rpb24ocmVzb2x2ZSwgcmVqZWN0KXtcclxuXHJcbiAgICBfeGhyKHtcclxuICAgICAgdXJsOiAnL2ZvcnVtL3Nlc3Npb24vY3VycmVudC5qc29uJyxcclxuICAgICAgbG9hZDogZnVuY3Rpb24oKXtcclxuICAgICAgICB2YXIgZGF0YSA9IEpTT04ucGFyc2UodGhpcy5yZXNwb25zZVRleHQpO1xyXG4gICAgICAgIGlmKHRoaXMuc3RhdHVzID09PSAyMDAgJiYgdHlwZW9mIGRhdGEgPT09ICdvYmplY3QnKXtcclxuICAgICAgICAgIHJlc29sdmUoZGF0YSk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9KTtcclxuXHJcbiAgfSk7XHJcblxyXG4gIC8vIFJlc29sdmUgQUpBWCBwcm9taXNlc1xyXG4gIHNpdGVfZGF0YS50aGVuKGZ1bmN0aW9uKGRhdGEpe1xyXG4gICAgYXBpRGF0YS5zaXRlX2RhdGEgPSBkYXRhO1xyXG4gICAgc3RhcnQoKTtcclxuICB9KTtcclxuICB1c2VyX2RhdGEudGhlbihmdW5jdGlvbihkYXRhKXtcclxuICAgIGFwaURhdGEudXNlcl9kYXRhID0gZGF0YTtcclxuICAgIHN0YXJ0KCk7XHJcbiAgfSk7XHJcbiAgdmFyIHN0YXJ0ID0gZnVuY3Rpb24oKXtcclxuICAgIGlmKGFwaURhdGEuc2l0ZV9kYXRhICYmIGFwaURhdGEudXNlcl9kYXRhKXtcclxuICAgICAgLy8gU2V0IGRhdGEgYW5kIGluaXQgd2hlbiBib3RoIGRhdGEgc2V0cyBhcmUgcmV0dXJuZWRcclxuICAgICAgcm10LmRhdGEgPSBhcGlEYXRhO1xyXG4gICAgICBybXQuaW5pdCgpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbn0iLCJybXQudWkgPSB7XHJcbiAgZWxlbXM6IHt9LFxyXG4gIGRpc3BsYXlfc3RhdGU6IDFcclxufVxyXG5cclxuXHJcbi8qKlxyXG4gKiBJbml0aWFsaXplIFVJXHJcbiAqL1xyXG5ybXQudWkuaW5pdCA9IGZ1bmN0aW9uKCl7XHJcbiAgY29uc29sZS5sb2coJ3JtdCB1aSBpbml0Jyk7XHJcblxyXG4gIC8vIFJlbmRlciBtYWluIHdyYXBcclxuICBybXQudWkuZWxlbXMuX3dyYXAgPSBkb2N1bWVudC5ib2R5LmFwcGVuZENoaWxkKF9yZW5kZXIoe1xyXG4gICAgdGFnOiAnZGl2JyxcclxuICAgIGF0dHI6IHtcclxuICAgICAgJ2lkJzogJ3JtdF9fd3JhcCcsXHJcbiAgICB9LFxyXG4gIH0pKTtcclxuXHJcbiAgLy8gUmVuZGVyIHRvZ2dsZVxyXG4gIHJtdC51aS5lbGVtcy5fdG9nZ2xlX2Rpc3BsYXkgPSBybXQudWkuZWxlbXMuX3dyYXAuYXBwZW5kQ2hpbGQoX3JlbmRlcih7XHJcbiAgICB0YWc6ICdzcGFuJyxcclxuICAgIGF0dHI6IHtcclxuICAgICAgJ2lkJzogJ3JtdF9fZGlzcGxheS10b2dnbGUnLFxyXG4gICAgICAndGl0bGUnOiAnVG9nZ2xlIFJlYWxtRXllIE1vZCBUb29scycsXHJcbiAgICB9LFxyXG4gICAgZXZlbnRzOiBbXHJcbiAgICAgIHtcclxuICAgICAgICB0eXBlOiAnY2xpY2snLFxyXG4gICAgICAgIGxpc3RlbmVyOiBybXQudWkuZXZlbnRzLnRvZ2dsZURpc3BsYXksXHJcbiAgICAgIH0sXHJcbiAgICBdLFxyXG4gIH0pKTtcclxuXHJcbiAgLy8gUmVuZGVyIGRpc3BsYXkgd3JhcFxyXG4gIHJtdC51aS5lbGVtcy5fZGlzcGxheV93cmFwID0gcm10LnVpLmVsZW1zLl93cmFwLmFwcGVuZENoaWxkKF9yZW5kZXIoe1xyXG4gICAgdGFnOiAnZGl2JyxcclxuICAgIGF0dHI6IHtcclxuICAgICAgJ2lkJzogJ3JtdF9fZGlzcGxheS13cmFwJyxcclxuICAgIH0sXHJcbiAgfSkpO1xyXG5cclxuICAvLyBSZW5kZXIgbWVzc2FnZSBvdXRwdXRcclxuICBybXQudWkuZWxlbXMuX21zZyA9IHJtdC51aS5lbGVtcy5fZGlzcGxheV93cmFwLmFwcGVuZENoaWxkKF9yZW5kZXIoe1xyXG4gICAgdGFnOiAnZGl2JyxcclxuICAgIGF0dHI6IHtcclxuICAgICAgJ2lkJzogJ3JtdF9fbXNnJyxcclxuICAgIH0sXHJcbiAgICBjc3M6IHtcclxuICAgICAgJ2Rpc3BsYXknOiAnbm9uZScsXHJcbiAgICB9LFxyXG4gIH0pKTtcclxuXHJcbiAgLy8gUmVuZGVyIG9wdCBsaXN0XHJcbiAgcm10LnVpLmVsZW1zLl9vcHRfbGlzdCA9IHJtdC51aS5lbGVtcy5fZGlzcGxheV93cmFwLmFwcGVuZENoaWxkKF9yZW5kZXIoe1xyXG4gICAgdGFnOiAndWwnLFxyXG4gICAgYXR0cjoge1xyXG4gICAgICAnaWQnOiAncm10X19vcHQtbGlzdCcsXHJcbiAgICB9LFxyXG4gIH0pKTtcclxuXHJcbiAgLyoqXHJcbiAgICogSW5pdCBvcHRpb25zXHJcbiAgICovXHJcbiAgcm10LnVpLmluaXRPcHRzKCk7XHJcbn1cclxuXHJcblxyXG4vKipcclxuICogSW5pdGlhbGl6ZSBpbmRpdmlkdWFsIG9wdGlvbnNcclxuICovXHJcbnJtdC51aS5pbml0T3B0cyA9IGZ1bmN0aW9uKCl7XHJcbiAgcm10Lm1vZG5vdGUuaW5pdCgpO1xyXG4gIHJtdC5yZV9tZXNzYWdlcy5pbml0KCk7XHJcbiAgcm10LmxhdGVzdF90b3BpY3MuaW5pdCgpO1xyXG59XHJcblxyXG5cclxuXHJcbi8vLy8vLy8vLy8vLy8vLy8vXHJcbi8vIFVJIC0gRVZFTlRTIC8vXHJcbi8vLy8vLy8vLy8vLy8vLy8vXHJcbnJtdC51aS5ldmVudHMgPSB7fVxyXG5cclxuLyoqXHJcbiAqIFRvZ2dsZXMgbWFpbiB3cmFwIGRpc3BsYXlcclxuICogQHJldHVybiB7W3R5cGVdfSBbZGVzY3JpcHRpb25dXHJcbiAqL1xyXG5ybXQudWkuZXZlbnRzLnRvZ2dsZURpc3BsYXkgPSBmdW5jdGlvbigpe1xyXG4gIGlmKHJtdC51aS5kaXNwbGF5X3N0YXRlKXtcclxuICAgIF9zbGlkZSgndXAnLCBybXQudWkuZWxlbXMuX2Rpc3BsYXlfd3JhcCwgMzAwLCAnZWFzZS1pbi1vdXQnLCB0cnVlLCBmdW5jdGlvbigpe1xyXG4gICAgICBybXQudWkuZGlzcGxheV9zdGF0ZSA9ICFybXQudWkuZGlzcGxheV9zdGF0ZTtcclxuICAgICAgcm10LnVpLmVsZW1zLl90b2dnbGVfZGlzcGxheS5jbGFzc05hbWUgPSAncm10LWhpZGRlbic7XHJcbiAgICB9KTtcclxuICB9ZWxzZXtcclxuICAgIF9zbGlkZSgnZG93bicsIHJtdC51aS5lbGVtcy5fZGlzcGxheV93cmFwLCAzMDAsICdlYXNlLWluLW91dCcsIHRydWUsIGZ1bmN0aW9uKCl7XHJcbiAgICAgIHJtdC51aS5kaXNwbGF5X3N0YXRlID0gIXJtdC51aS5kaXNwbGF5X3N0YXRlO1xyXG4gICAgICBybXQudWkuZWxlbXMuX3RvZ2dsZV9kaXNwbGF5LmNsYXNzTmFtZSA9ICcnO1xyXG4gICAgfSk7XHJcbiAgfVxyXG59XHJcblxyXG5cclxuXHJcbi8vLy8vLy8vLy8vLy8vXHJcbi8vIE1FU1NBR0VTIC8vXHJcbi8vLy8vLy8vLy8vLy8vXHJcbnJtdC51aS5tc2cgPSB7fVxyXG5cclxuLyoqXHJcbiAqIFNldCBVSSBtZXNzYWdlXHJcbiAqIEBwYXJhbSB7c3RyaW5nfSBtc2dcclxuICogQHBhcmFtIHtzdHJpbmd9IHR5cGVcclxuICovXHJcbnJtdC51aS5tc2cuc2V0ID0gZnVuY3Rpb24obXNnLCB0eXBlKXtcclxuICBybXQudWkuZWxlbXMuX21zZy5jbGFzc05hbWUgPSAncm10LW1zZy0tJyArIHR5cGU7XHJcbiAgcm10LnVpLmVsZW1zLl9tc2cuc3R5bGUuZGlzcGxheSA9ICdibG9jayc7XHJcbiAgcm10LnVpLmVsZW1zLl9tc2cuaW5uZXJIVE1MID0gbXNnO1xyXG59XHJcblxyXG4vKipcclxuICogUmVtb3ZlIGFuZCByZXNldCBVSSBtZXNzYWdlXHJcbiAqL1xyXG5ybXQudWkubXNnLnJlbW92ZSA9IGZ1bmN0aW9uKCl7XHJcbiAgcm10LnVpLmVsZW1zLl9tc2cuY2xhc3NOYW1lID0gJycsXHJcbiAgcm10LnVpLmVsZW1zLl9tc2cuc3R5bGUuZGlzcGxheSA9ICdub25lJztcclxuICBybXQudWkuZWxlbXMuX21zZy5pbm5lckhUTUwgPSAnJztcclxufSIsIi8qKlxyXG4gKiBNb2QgTm90ZSBjb21wb25lbnRcclxuICogQHR5cGUge09iamVjdH1cclxuICovXHJcbnJtdC5tb2Rub3RlID0ge1xyXG5cclxuICAvKipcclxuICAgKiBVSSBzdGF0ZVxyXG4gICAqL1xyXG4gIGRpc3BsYXlfc3RhdGU6IDAsXHJcblxyXG4gIC8qKlxyXG4gICAqIE1vZCBub3RlIHN0cmluZ3NcclxuICAgKiBAdHlwZSB7T2JqZWN0fVxyXG4gICAqL1xyXG4gIG5vdGVzOiB7XHJcbiAgICBvdGhlcjoge1xyXG4gICAgICBzdHJpbmc6ICdcXG5cXG4tLS1cXG5cXG4qKipNb2Qgbm90ZToqKiB7JHByZXNldF9ub3RlfXskbm90ZX0gfiB7JHVzZXJuYW1lfSonLFxyXG4gICAgfSxcclxuICAgIHRocmVhZF9tb3ZlOiB7XHJcbiAgICAgIHN0cmluZzogJ1xcblxcbi0tLVxcblxcbioqKk1vZCBub3RlOioqIE1vdmVkIHRvIFt7JGNhdGVnb3J5X25hbWV9XSh7JGNhdGVnb3J5X3VybH0peyRub3RlfSB+IHskdXNlcm5hbWV9KicsXHJcbiAgICB9LFxyXG4gIH0sXHJcblxyXG4gIHByZXNldF9ub3RlczogW1xyXG4gICAge1xyXG4gICAgICBsYWJlbDogJ1dlXFwncmUgZW5jb3VyYWdpbmcgY29tcGxldGVkIGFuZCBleHRlbnNpdmUgaWRlYXMuIFBsZWFzZSB3b3JrIG9uIGl0IGFuZCBjb21lIGJhY2shJyxcclxuICAgICAgbm90ZTogJ1dlXFwncmUgZW5jb3VyYWdpbmcgY29tcGxldGVkIGFuZCBleHRlbnNpdmUgaWRlYXMuIFBsZWFzZSB3b3JrIG9uIGl0IGFuZCBjb21lIGJhY2shJyxcclxuICAgIH0sXHJcbiAgICB7XHJcbiAgICAgIGxhYmVsOiAnL3RocmVhZCcsXHJcbiAgICAgIG5vdGU6ICcmbHQ7L3RocmVhZCZndDsnLFxyXG4gICAgfSxcclxuICAgIHtcclxuICAgICAgbGFiZWw6ICdGcm9nJyxcclxuICAgICAgbm90ZTogJ1tpbWddaHR0cDovL3N0YXRpYy5kcmlwcy5wdy9yb3RtZy93aWtpL1BldHMvRGVtb24lMjBGcm9nJTIwR2VuZXJhdG9yLnBuZ1svaW1nXScsXHJcbiAgICB9LFxyXG4gICAge1xyXG4gICAgICBsYWJlbDogJ+CyoF/gsqAnLFxyXG4gICAgICBub3RlOiAn4LKgX+CyoCcsXHJcbiAgICB9LFxyXG4gIF0sXHJcblxyXG59XHJcblxyXG5cclxuLyoqXHJcbiAqIEluaXRpYWxpemUgTW9kIE5vdGUgVUkgYW5kIGV2ZW50c1xyXG4gKi9cclxucm10Lm1vZG5vdGUuaW5pdCA9IGZ1bmN0aW9uKCl7XHJcbiAgY29uc29sZS5sb2coJ3JtdCBtb2Rub3RlIGluaXQnKTtcclxuXHJcbiAgLy8gU2ltcGxpZnkgY2F0ZWdvcmllc1xyXG4gIHZhciBfY2F0ZWdvcmllc19zaW1wbGUgPSB7fTtcclxuICBybXQuZGF0YS5zaXRlX2RhdGEuY2F0ZWdvcmllcy5mb3JFYWNoKGZ1bmN0aW9uKF9jYXRlZ29yeSl7XHJcbiAgICB2YXIgZGF0YSA9IHtcclxuICAgICAgaWQ6IF9jYXRlZ29yeS5pZCxcclxuICAgICAgbmFtZTogX2NhdGVnb3J5Lm5hbWUsXHJcbiAgICAgIHVybDogX2NhdGVnb3J5LnRvcGljX3VybCxcclxuICAgIH1cclxuICAgIC8vIEFwcGx5IHBhcmVudCBJRFxyXG4gICAgaWYoX2NhdGVnb3J5LnBhcmVudF9jYXRlZ29yeV9pZCl7XHJcbiAgICAgIGRhdGEucGFyZW50X2lkID0gX2NhdGVnb3J5LnBhcmVudF9jYXRlZ29yeV9pZDtcclxuICAgIH1cclxuICAgIC8vIFB1c2hcclxuICAgIF9jYXRlZ29yaWVzX3NpbXBsZVtfY2F0ZWdvcnkuaWRdID0gZGF0YTtcclxuICB9KTtcclxuXHJcbiAgLy8gQnVpbGQgY2F0ZWdvcnkgb3B0aW9uIGxpc3RcclxuICB2YXIgY2F0ZWdvcnlfb3B0aW9ucyA9IFtdO1xyXG4gIGZvcih2YXIgX2lkIGluIF9jYXRlZ29yaWVzX3NpbXBsZSl7XHJcbiAgICAvLyBDb25zdHJ1Y3Qgam9pbmVkIG5hbWVcclxuICAgIHZhciBfbmFtZSA9IF9jYXRlZ29yaWVzX3NpbXBsZVtfaWRdLm5hbWUsXHJcbiAgICAgICAgbm9fcGFyZW50ID0gZmFsc2UsXHJcbiAgICAgICAgbGFzdF9jaGVjayA9IF9pZDtcclxuICAgIHdoaWxlKCFub19wYXJlbnQpe1xyXG4gICAgICBpZihfY2F0ZWdvcmllc19zaW1wbGVbbGFzdF9jaGVja10ucGFyZW50X2lkKXtcclxuICAgICAgICAvLyBQcmVwZW5kIHBhcmVudCBjYXRlZ29yeSBuYW1lXHJcbiAgICAgICAgX25hbWUgPSBfY2F0ZWdvcmllc19zaW1wbGVbX2NhdGVnb3JpZXNfc2ltcGxlW2xhc3RfY2hlY2tdLnBhcmVudF9pZF0ubmFtZSArICcgPiAnICsgX25hbWU7XHJcbiAgICAgICAgbGFzdF9jaGVjayA9IF9jYXRlZ29yaWVzX3NpbXBsZVtsYXN0X2NoZWNrXS5wYXJlbnRfaWQ7XHJcbiAgICAgIH1lbHNle1xyXG4gICAgICAgIG5vX3BhcmVudCA9IHRydWU7XHJcbiAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAvLyBBcHBlbmQgbm9kZVxyXG4gICAgY2F0ZWdvcnlfb3B0aW9uc1tjYXRlZ29yeV9vcHRpb25zLmxlbmd0aF0gPSB7XHJcbiAgICAgIHRhZzogJ29wdGlvbicsXHJcbiAgICAgIGF0dHI6IHtcclxuICAgICAgICAnZGF0YS1uYW1lJzogX25hbWUsXHJcbiAgICAgICAgJ2RhdGEtdXJsJzogX2NhdGVnb3JpZXNfc2ltcGxlW19pZF0udXJsLFxyXG4gICAgICB9LFxyXG4gICAgICBodG1sOiBfbmFtZSxcclxuICAgIH1cclxuICB9XHJcblxyXG4gIC8vIEJ1aWxkIHByZXNldCBub3RlIG9wdGlvbiBsaXN0XHJcbiAgdmFyIHByZXNldF9ub3RlcyA9IFt7XHJcbiAgICB0YWc6ICdvcHRpb24nLFxyXG4gICAgYXR0cjoge1xyXG4gICAgICAndmFsdWUnOiAnJyxcclxuICAgIH0sXHJcbiAgICBodG1sOiAnTm8gUHJlc2V0IE5vdGUnLFxyXG4gIH1dO1xyXG4gIGZvcih2YXIgX3Byb3AgaW4gcm10Lm1vZG5vdGUucHJlc2V0X25vdGVzKXtcclxuICAgIC8vIEFwcGVuZCBub2RlXHJcbiAgICBwcmVzZXRfbm90ZXNbcHJlc2V0X25vdGVzLmxlbmd0aF0gPSB7XHJcbiAgICAgIHRhZzogJ29wdGlvbicsXHJcbiAgICAgIGF0dHI6IHtcclxuICAgICAgICAndmFsdWUnOiBybXQubW9kbm90ZS5wcmVzZXRfbm90ZXNbX3Byb3BdLm5vdGUsXHJcbiAgICAgIH0sXHJcbiAgICAgIGh0bWw6IHJtdC5tb2Rub3RlLnByZXNldF9ub3Rlc1tfcHJvcF0ubGFiZWwsXHJcbiAgICB9XHJcbiAgfVxyXG4gIGNvbnNvbGUubG9nKHByZXNldF9ub3Rlcyk7XHJcblxyXG4gIC8vIFNvcnQgYWxwaGFiZXRpY2FsbHlcclxuICBjYXRlZ29yeV9vcHRpb25zLnNvcnQoZnVuY3Rpb24oYSwgYil7XHJcbiAgICBpZihhLmh0bWwgPCBiLmh0bWwpe1xyXG4gICAgICByZXR1cm4gLTE7XHJcbiAgICB9XHJcbiAgICBpZihhLmh0bWwgPiBiLmh0bWwpe1xyXG4gICAgICByZXR1cm4gMTtcclxuICAgIH1cclxuICAgIHJldHVybiAwO1xyXG4gIH0pO1xyXG5cclxuICAvLyBSZW5kZXIgb3B0aW9uXHJcbiAgcm10LnVpLmVsZW1zLl9tb2Rub3RlID0gcm10LnVpLmVsZW1zLl9vcHRfbGlzdC5hcHBlbmRDaGlsZChfcmVuZGVyKHtcclxuICAgIHRhZzogJ2xpJyxcclxuICAgIGF0dHI6IHtcclxuICAgICAgJ2lkJzogJ3JtdF9fbW9kbm90ZScsXHJcbiAgICB9LFxyXG4gICAgbm9kZXM6IFtcclxuICAgICAge1xyXG4gICAgICAgIHRhZzogJ2g0JyxcclxuICAgICAgICBhdHRyOiB7XHJcbiAgICAgICAgICBjbGFzczogJ3JtdF9fb3B0LWhlYWRpbmcnLFxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgaHRtbDogJ0FkZCBNb2QgTm90ZScsXHJcbiAgICAgICAgbm9kZXM6IFtcclxuICAgICAgICAgIHtcclxuICAgICAgICAgICAgdGFnOiAnc3BhbicsXHJcbiAgICAgICAgICAgIGF0dHI6IHtcclxuICAgICAgICAgICAgICAnY2xhc3MnOiAncm10LWFkZCcsXHJcbiAgICAgICAgICAgICAgJ3RpdGxlJzogJ0FkZCBNb2QgTm90ZScsXHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICB9LFxyXG4gICAgICAgIF0sXHJcbiAgICAgICAgZXZlbnRzOiBbXHJcbiAgICAgICAgICB7XHJcbiAgICAgICAgICAgIHR5cGU6ICdjbGljaycsXHJcbiAgICAgICAgICAgIGxpc3RlbmVyOiBybXQubW9kbm90ZS50b2dnbGVBZGQsXHJcbiAgICAgICAgICB9LFxyXG4gICAgICAgIF0sXHJcbiAgICAgIH0sXHJcbiAgICAgIHtcclxuICAgICAgICB0YWc6ICdkaXYnLFxyXG4gICAgICAgIGF0dHI6IHtcclxuICAgICAgICAgICdpZCc6ICdybXRfX21vZG5vdGVfX29wdGlvbnMnLFxyXG4gICAgICAgICAgJ2NsYXNzJzogJ3JtdC1vcHQtZGlzcGxheScsXHJcbiAgICAgICAgfSxcclxuICAgICAgICBjc3M6IHtcclxuICAgICAgICAgICdkaXNwbGF5JzogJ25vbmUnLFxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgbm9kZXM6IFtcclxuICAgICAgICAgIHtcclxuICAgICAgICAgICAgdGFnOiAnZm9ybScsXHJcbiAgICAgICAgICAgIGF0dHI6IHtcclxuICAgICAgICAgICAgICAnY2xhc3MnOiAncm10X19tb2Rub3RlX190eXBlJyxcclxuICAgICAgICAgICAgICAnYWN0aW9uJzogJyMnLFxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBldmVudHM6IFtcclxuICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICB0eXBlOiAnc3VibWl0JyxcclxuICAgICAgICAgICAgICAgIGxpc3RlbmVyOiBmdW5jdGlvbihlKXtcclxuICAgICAgICAgICAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIF0sXHJcbiAgICAgICAgICAgIG5vZGVzOiBbXHJcbiAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgdGFnOiAnc3BhbicsXHJcbiAgICAgICAgICAgICAgICBhdHRyOiB7XHJcbiAgICAgICAgICAgICAgICAgICdjbGFzcyc6ICdybXRfX21vZG5vdGVfX2xhYmVsJ1xyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIGh0bWw6ICdNb2QgTm90ZSBUeXBlOicsXHJcbiAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICB0YWc6ICdpbnB1dCcsXHJcbiAgICAgICAgICAgICAgICBhdHRyOiB7XHJcbiAgICAgICAgICAgICAgICAgICd0eXBlJzogJ3JhZGlvJyxcclxuICAgICAgICAgICAgICAgICAgJ2lkJzogJ3JtdF9fbW9kbm90ZV9fdHlwZS1zZWxlY3QtLXRocmVhZC1tb3ZlJyxcclxuICAgICAgICAgICAgICAgICAgJ25hbWUnOiAncm10X19tb2Rub3RlX190eXBlLXNlbGVjdCcsXHJcbiAgICAgICAgICAgICAgICAgICd2YWx1ZSc6ICd0aHJlYWQtbW92ZScsXHJcbiAgICAgICAgICAgICAgICAgICdjbGFzcyc6ICdybXRfX21vZG5vdGVfX3R5cGUtc2VsZWN0JyxcclxuICAgICAgICAgICAgICAgICAgJ2NoZWNrZWQnOiAnY2hlY2tlZCcsXHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgdGFnOiAnbGFiZWwnLFxyXG4gICAgICAgICAgICAgICAgYXR0cjoge1xyXG4gICAgICAgICAgICAgICAgICAnY2xhc3MnOiAncm10X19tb2Rub3RlX190eXBlLWxhYmVsJyxcclxuICAgICAgICAgICAgICAgICAgJ2Zvcic6ICdybXRfX21vZG5vdGVfX3R5cGUtc2VsZWN0LS10aHJlYWQtbW92ZScsXHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgaHRtbDogJ1RocmVhZCBNb3ZlJyxcclxuICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgIHRhZzogJ2lucHV0JyxcclxuICAgICAgICAgICAgICAgIGF0dHI6IHtcclxuICAgICAgICAgICAgICAgICAgJ3R5cGUnOiAncmFkaW8nLFxyXG4gICAgICAgICAgICAgICAgICAnaWQnOiAncm10X19tb2Rub3RlX190eXBlLXNlbGVjdC0tb3RoZXInLFxyXG4gICAgICAgICAgICAgICAgICAnbmFtZSc6ICdybXRfX21vZG5vdGVfX3R5cGUtc2VsZWN0JyxcclxuICAgICAgICAgICAgICAgICAgJ3ZhbHVlJzogJ290aGVyJyxcclxuICAgICAgICAgICAgICAgICAgJ2NsYXNzJzogJ3JtdF9fbW9kbm90ZV9fdHlwZS1zZWxlY3QnLFxyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgIHRhZzogJ2xhYmVsJyxcclxuICAgICAgICAgICAgICAgIGF0dHI6IHtcclxuICAgICAgICAgICAgICAgICAgJ2NsYXNzJzogJ3JtdF9fbW9kbm90ZV9fdHlwZS1sYWJlbCcsXHJcbiAgICAgICAgICAgICAgICAgICdmb3InOiAncm10X19tb2Rub3RlX190eXBlLXNlbGVjdC0tb3RoZXInLFxyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIGh0bWw6ICdPdGhlcicsXHJcbiAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICB0YWc6ICdzZWxlY3QnLFxyXG4gICAgICAgICAgICAgICAgYXR0cjoge1xyXG4gICAgICAgICAgICAgICAgICAnaWQnOiAncm10X19tb2Rub3RlX190aHJlYWQtbW92ZS1sb2MnLFxyXG4gICAgICAgICAgICAgICAgICAnY2xhc3MnOiAncm10X19tb2Rub3RlX19vcHRpb25hbCcsXHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgbm9kZXM6IGNhdGVnb3J5X29wdGlvbnMsXHJcbiAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICB0YWc6ICdzZWxlY3QnLFxyXG4gICAgICAgICAgICAgICAgYXR0cjoge1xyXG4gICAgICAgICAgICAgICAgICAnaWQnOiAncm10X19tb2Rub3RlX19wcmVzZXQtbm90ZScsXHJcbiAgICAgICAgICAgICAgICAgICdjbGFzcyc6ICdybXRfX21vZG5vdGVfX29wdGlvbmFsJyxcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICBub2RlczogcHJlc2V0X25vdGVzLFxyXG4gICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgdGFnOiAnaW5wdXQnLFxyXG4gICAgICAgICAgICAgICAgYXR0cjoge1xyXG4gICAgICAgICAgICAgICAgICAndHlwZSc6ICd0ZXh0JyxcclxuICAgICAgICAgICAgICAgICAgJ2lkJzogJ3JtdF9fbW9kbm90ZV9fbWVzc2FnZScsXHJcbiAgICAgICAgICAgICAgICAgICdwbGFjZWhvbGRlcic6ICdNb2QgTm90ZS4uLicsXHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgIH0sLFxyXG4gICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgIHRhZzogJ2J1dHRvbicsXHJcbiAgICAgICAgICAgICAgICBhdHRyOiB7XHJcbiAgICAgICAgICAgICAgICAgICdpZCc6ICdybXRfX21vZG5vdGVfX3N1Ym1pdCcsXHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgZXZlbnRzOiBbXHJcbiAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICB0eXBlOiAnY2xpY2snLFxyXG4gICAgICAgICAgICAgICAgICAgIGxpc3RlbmVyOiBybXQubW9kbm90ZS5hcHBlbmRNZXNzYWdlLFxyXG4gICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgXSxcclxuICAgICAgICAgICAgICAgIGh0bWw6ICdBZGQgdG8gRWRpdCcsXHJcbiAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgXSxcclxuICAgICAgICAgIH0sXHJcbiAgICAgICAgXSxcclxuICAgICAgfSxcclxuICAgIF0sXHJcbiAgfSkpO1xyXG5cclxufVxyXG5cclxuXHJcbi8qKlxyXG4gKiBUb2dnbGUgZGlzcGxheSBvZiBVSVxyXG4gKi9cclxucm10Lm1vZG5vdGUudG9nZ2xlQWRkID0gZnVuY3Rpb24oKXtcclxuICB2YXIgX2VsZW0gPSBybXQudWkuZWxlbXMuX21vZG5vdGUucXVlcnlTZWxlY3RvcignI3JtdF9fbW9kbm90ZV9fb3B0aW9ucycpO1xyXG4gIGlmKCFfZWxlbSl7IHJldHVybiBmYWxzZTsgfVxyXG4gIGlmKHJtdC5tb2Rub3RlLmRpc3BsYXlfc3RhdGUpe1xyXG4gICAgcm10LnVpLm1zZy5yZW1vdmUoKTtcclxuICAgIF9zbGlkZSgndXAnLCBfZWxlbSwgMzAwLCAnZWFzZS1pbi1vdXQnLCB0cnVlLCBmdW5jdGlvbigpe1xyXG4gICAgICBybXQubW9kbm90ZS5kaXNwbGF5X3N0YXRlID0gIXJtdC5tb2Rub3RlLmRpc3BsYXlfc3RhdGU7XHJcbiAgICAgIHJtdC51aS5lbGVtcy5fbW9kbm90ZS5jbGFzc05hbWUgPSAnJztcclxuICAgICAgLy8gUmVzZXQgdmFsdWVzXHJcbiAgICAgIF9lbGVtLnF1ZXJ5U2VsZWN0b3IoJyNybXRfX21vZG5vdGVfX21lc3NhZ2UnKS52YWx1ZSA9ICcnO1xyXG4gICAgICBybXQudWkuZWxlbXMuX21vZG5vdGUucXVlcnlTZWxlY3RvcignI3JtdF9fbW9kbm90ZV9fcHJlc2V0LW5vdGUnKS5vcHRpb25zWzBdLnNlbGVjdGVkID0gdHJ1ZTs7XHJcbiAgICAgIHJtdC51aS5lbGVtcy5fbW9kbm90ZS5xdWVyeVNlbGVjdG9yKCcjcm10X19tb2Rub3RlX190aHJlYWQtbW92ZS1sb2MnKS5vcHRpb25zWzBdLnNlbGVjdGVkID0gdHJ1ZTs7XHJcbiAgICB9KTtcclxuICB9ZWxzZXtcclxuICAgIF9zbGlkZSgnZG93bicsIF9lbGVtLCAzMDAsICdlYXNlLWluLW91dCcsIHRydWUsIGZ1bmN0aW9uKCl7XHJcbiAgICAgIHJtdC5tb2Rub3RlLmRpc3BsYXlfc3RhdGUgPSAhcm10Lm1vZG5vdGUuZGlzcGxheV9zdGF0ZTtcclxuICAgICAgcm10LnVpLmVsZW1zLl9tb2Rub3RlLmNsYXNzTmFtZSA9ICdybXQtb3Blbic7XHJcbiAgICB9KTtcclxuICB9XHJcbn1cclxuXHJcblxyXG4vKipcclxuICogQXBwZW5kIG1lc3NhZ2UgdG8gZWRpdG9yIGJhc2VkIG9uIG5vdGUgdHlwZVxyXG4gKi9cclxucm10Lm1vZG5vdGUuYXBwZW5kTWVzc2FnZSA9IGZ1bmN0aW9uKCl7XHJcbiAgLy8gUmVzZXQgbXNnXHJcbiAgcm10LnVpLm1zZy5yZW1vdmUoKTtcclxuXHJcbiAgLy8gRGVmYXVsdCBkYXRhXHJcbiAgdmFyIGRhdGEgPSB7XHJcbiAgICAndXNlcm5hbWUnOiBybXQuZGF0YS51c2VyX2RhdGEuY3VycmVudF91c2VyLnVzZXJuYW1lLFxyXG4gICAgJ25vdGUnOiAnJyxcclxuICAgICdwcmVzZXRfbm90ZSc6ICcnLFxyXG4gIH1cclxuXHJcbiAgLy8gR2V0IGVkaXRvclxyXG4gIHZhciB0YXJnZXQgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcjcmVwbHktY29udHJvbCB0ZXh0YXJlYS5kLWVkaXRvci1pbnB1dCcpO1xyXG4gIGlmKCF0YXJnZXQpe1xyXG4gICAgcm10LnVpLm1zZy5zZXQoJ05vIGNvbW1lbnQvdG9waWMgZWRpdG9yIGZvdW5kJywgJ2Vycm9yJyk7XHJcbiAgICByZXR1cm4gZmFsc2U7XHJcbiAgfVxyXG5cclxuICAvLyBNZXNzYWdlXHJcbiAgdmFyIG1zZyA9IHJtdC51aS5lbGVtcy5fbW9kbm90ZS5xdWVyeVNlbGVjdG9yKCcjcm10X19tb2Rub3RlX19tZXNzYWdlJykudmFsdWUudHJpbSgpO1xyXG5cclxuICAvLyBNb2Qgbm90ZSB0eXBlXHJcbiAgc3dpdGNoKHJtdC51aS5lbGVtcy5fbW9kbm90ZS5xdWVyeVNlbGVjdG9yKCdpbnB1dC5ybXRfX21vZG5vdGVfX3R5cGUtc2VsZWN0OmNoZWNrZWQnKS52YWx1ZSl7XHJcbiAgICAvLyBPdGhlclxyXG4gICAgY2FzZSAnb3RoZXInOlxyXG4gICAgICB2YXIgcHJlc2V0X25vdGVzID0gcm10LnVpLmVsZW1zLl9tb2Rub3RlLnF1ZXJ5U2VsZWN0b3IoJyNybXRfX21vZG5vdGVfX3ByZXNldC1ub3RlJyk7XHJcbiAgICAgIGlmKHByZXNldF9ub3Rlcy5zZWxlY3RlZEluZGV4ICE9PSAtMSl7XHJcbiAgICAgICAgZGF0YVsncHJlc2V0X25vdGUnXSA9IHByZXNldF9ub3Rlcy5vcHRpb25zW3ByZXNldF9ub3Rlcy5zZWxlY3RlZEluZGV4XS52YWx1ZSArIChtc2cgIT09ICcnID8gJyAnIDogJycpO1xyXG4gICAgICB9XHJcbiAgICAgIGlmKG1zZyA9PT0gJycgJiYgcHJlc2V0X25vdGVzLnNlbGVjdGVkSW5kZXggPT09IDApe1xyXG4gICAgICAgIHJtdC51aS5tc2cuc2V0KCdQbGVhc2UgZW50ZXIgb3Igc2VsZWN0IGEgbm90ZScsICdlcnJvcicpO1xyXG4gICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgfVxyXG4gICAgICBkYXRhWydub3RlJ10gPSBtc2c7XHJcbiAgICAgIC8vIEFwcGVuZCBtZXNzYWdlIHRvIHRhcmdldFxyXG4gICAgICB0YXJnZXQudmFsdWUgKz0gcm10Lm1vZG5vdGUuZ2VuZXJhdGVNZXNzYWdlKHJtdC5tb2Rub3RlLm5vdGVzLm90aGVyLnN0cmluZywgZGF0YSk7XHJcbiAgICAgIGJyZWFrO1xyXG5cclxuICAgIC8vIFRocmVhZCBtb3ZlXHJcbiAgICBjYXNlICd0aHJlYWQtbW92ZSc6XHJcbiAgICAgIHZhciBjYXRlZ29yeV9saXN0ID0gcm10LnVpLmVsZW1zLl9tb2Rub3RlLnF1ZXJ5U2VsZWN0b3IoJyNybXRfX21vZG5vdGVfX3RocmVhZC1tb3ZlLWxvYycpO1xyXG4gICAgICBpZihjYXRlZ29yeV9saXN0LnNlbGVjdGVkSW5kZXggPT09IC0xKXtcclxuICAgICAgICBybXQudWkubXNnLnNldCgnUGxlYXNlIHNlbGVjdCBhIGNhdGVnb3J5JywgJ2Vycm9yJyk7XHJcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICB9XHJcbiAgICAgIHZhciBjYXRlZ29yeSA9IGNhdGVnb3J5X2xpc3Qub3B0aW9uc1tjYXRlZ29yeV9saXN0LnNlbGVjdGVkSW5kZXhdO1xyXG4gICAgICBkYXRhWydjYXRlZ29yeV9uYW1lJ10gPSBjYXRlZ29yeS5nZXRBdHRyaWJ1dGUoJ2RhdGEtbmFtZScpO1xyXG4gICAgICBkYXRhWydjYXRlZ29yeV91cmwnXSA9IGNhdGVnb3J5LmdldEF0dHJpYnV0ZSgnZGF0YS11cmwnKTtcclxuICAgICAgLy8gQXBwZW5kIG5vdGVcclxuICAgICAgZGF0YVsnbm90ZSddID0gKG1zZyAhPT0gJycgPyAnLiAnIDogJycpICsgbXNnO1xyXG4gICAgICAvLyBBcHBlbmQgbWVzc2FnZSB0byB0YXJnZXRcclxuICAgICAgdGFyZ2V0LnZhbHVlICs9IHJtdC5tb2Rub3RlLmdlbmVyYXRlTWVzc2FnZShybXQubW9kbm90ZS5ub3Rlcy50aHJlYWRfbW92ZS5zdHJpbmcsIGRhdGEpO1xyXG4gICAgICBicmVhaztcclxuICB9XHJcblxyXG4gIC8vIFVwZGF0ZSB0YXJnZXRcclxuICB0YXJnZXQuZm9jdXMoKTtcclxuICBzZXRUaW1lb3V0KGZ1bmN0aW9uKCl7XHJcbiAgICB0YXJnZXQuYmx1cigpO1xyXG4gIH0sIDE2KTtcclxuXHJcbiAgLy8gQ2xvc2UgVUlcclxuICBybXQubW9kbm90ZS50b2dnbGVBZGQoKTtcclxufVxyXG5cclxuXHJcbi8qKlxyXG4gKiBQYXJzZSB0ZW1wbGF0ZSBzdHJpbmcgYW5kIHJlcGxhY2Ugd2l0aCBkYXRhIHZhcmlhYmxlc1xyXG4gKiBAcGFyYW0gIHtzdHJpbmd9IG1zZ19zdHJpbmdcclxuICogQHBhcmFtICB7b2JqZWN0fSBkYXRhXHJcbiAqIEByZXR1cm4ge3N0cmluZ31cclxuICovXHJcbnJtdC5tb2Rub3RlLmdlbmVyYXRlTWVzc2FnZSA9IGZ1bmN0aW9uKG1zZ19zdHJpbmcsIGRhdGEpe1xyXG4gIGZvcih2YXIgX3NlYXJjaCBpbiBkYXRhKXtcclxuICAgIG1zZ19zdHJpbmcgPSBtc2dfc3RyaW5nLnJlcGxhY2UobmV3IFJlZ0V4cCgnXFxcXHtcXFxcJCcgKyBfc2VhcmNoICsgJ1xcXFx9JywgJ2dtJyksIGRhdGFbX3NlYXJjaF0pO1xyXG4gIH1cclxuICByZXR1cm4gbXNnX3N0cmluZztcclxufSIsIi8qKlxyXG4gKiBMYXRlc3QgVG9waWNzIGNvbXBvbmVudFxyXG4gKiBAdHlwZSB7T2JqZWN0fVxyXG4gKi9cclxucm10LnJlX21lc3NhZ2VzID0ge1xyXG4gIGxvb2t1cF9kZWxheTogNTAwMFxyXG59XHJcblxyXG5cclxuLyoqXHJcbiAqIEluaXRpYWxpemUgUmVhbG1FeWUgTWVzc2FnZXMgVUkgYW5kIGV2ZW50c1xyXG4gKi9cclxucm10LnJlX21lc3NhZ2VzLmluaXQgPSBmdW5jdGlvbigpe1xyXG4gIGNvbnNvbGUubG9nKCdybXQgcmVfbWVzc2FnZXMgaW5pdCcpO1xyXG5cclxuICAvLyBQcmVwIGRhdGFcclxuICBybXQucmVfbWVzc2FnZXMudXJsX2Jhc2UgPSAnL21lc3NhZ2VzLW9mLXBsYXllci8nICsgcm10LmRhdGEudXNlcl9kYXRhLmN1cnJlbnRfdXNlci51c2VybmFtZTtcclxuXHJcbiAgLy8gUmVuZGVyIG9wdGlvblxyXG4gIHJtdC51aS5lbGVtcy5fcmVfbWVzc2FnZXMgPSBybXQudWkuZWxlbXMuX29wdF9saXN0LmFwcGVuZENoaWxkKF9yZW5kZXIoe1xyXG4gICAgdGFnOiAnbGknLFxyXG4gICAgYXR0cjoge1xyXG4gICAgICAnaWQnOiAncm10X19yZV9tZXNzYWdlcycsXHJcbiAgICB9LFxyXG4gICAgbm9kZXM6IFtcclxuICAgICAge1xyXG4gICAgICAgIHRhZzogJ2EnLFxyXG4gICAgICAgIGF0dHI6e1xyXG4gICAgICAgICAgJ2hyZWYnOiBybXQucmVfbWVzc2FnZXMudXJsX2Jhc2UsXHJcbiAgICAgICAgICAndGFyZ2V0JzogJ19ibGFuaycsXHJcbiAgICAgICAgfSxcclxuICAgICAgICBub2RlczogW1xyXG4gICAgICAgICAge1xyXG4gICAgICAgICAgICB0YWc6ICdoNCcsXHJcbiAgICAgICAgICAgIGF0dHI6IHtcclxuICAgICAgICAgICAgICBjbGFzczogJ3JtdF9fb3B0LWhlYWRpbmcnLFxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBodG1sOiAnUmVhbG1FeWUgTWVzc2FnZXMnLFxyXG4gICAgICAgICAgICBub2RlczogW1xyXG4gICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgIHRhZzogJ3NwYW4nLFxyXG4gICAgICAgICAgICAgICAgYXR0cjoge1xyXG4gICAgICAgICAgICAgICAgICBpZDogJ3JtdF9fcmVfbWVzc2FnZXNfX3RhcmdldCcsXHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgaHRtbDogJzAnLFxyXG4gICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIF0sXHJcbiAgICAgICAgICB9LFxyXG4gICAgICAgIF0sXHJcbiAgICAgIH0sXHJcbiAgICBdLFxyXG4gIH0pKTtcclxuXHJcbiAgLy8gU3RhcnQgbWVzc2FnZSBsb29wXHJcbiAgaWYocm10LmRhdGEudXNlcl9kYXRhLmN1cnJlbnRfdXNlci51c2VybmFtZSl7XHJcbiAgICBybXQucmVfbWVzc2FnZXMuZ2V0TWVzc2FnZUNvdW50KHRydWUpO1xyXG4gIH1cclxuXHJcbn1cclxuXHJcblxyXG4vKipcclxuICogRmV0Y2ggbWVzc2FnZSBjb3VudCB2aWEgQUpBWCBhbmQgSFRNTCBwYXJzZVxyXG4gKiBAcGFyYW0gIHtib29sZWFufSBsb29wIExvb3AgcmVxdWVzdFxyXG4gKi9cclxucm10LnJlX21lc3NhZ2VzLmdldE1lc3NhZ2VDb3VudCA9IGZ1bmN0aW9uKGxvb3Ape1xyXG4gIGlmKHR5cGVvZiBsb29wID09PSAndW5kZWZpbmVkJyl7XHJcbiAgICBsb29wID0gZmFsc2U7XHJcbiAgfVxyXG5cclxuICAvLyBJbml0IEFKQVhcclxuICB2YXIgbG9va3VwX21lc3NhZ2VzID0gbmV3IFByb21pc2UoZnVuY3Rpb24ocmVzb2x2ZSwgcmVqZWN0KXtcclxuXHJcbiAgICBfeGhyKHtcclxuICAgICAgdXJsOiBybXQucmVfbWVzc2FnZXMudXJsX2Jhc2UsXHJcbiAgICAgIGxvYWQ6IGZ1bmN0aW9uKCl7XHJcbiAgICAgICAgaWYodGhpcy5zdGF0dXMgPT09IDIwMCl7XHJcbiAgICAgICAgICByZXNvbHZlKHRoaXMucmVzcG9uc2VUZXh0KTtcclxuICAgICAgICB9ZWxzZXtcclxuICAgICAgICAgIGlmKGxvb3Ape1xyXG4gICAgICAgICAgICBybXQucmVfbWVzc2FnZXMuZ2V0TWVzc2FnZUNvdW50KGxvb3ApO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfSk7XHJcblxyXG4gIH0pO1xyXG5cclxuICAvLyBSZXNvbHZlIHByb21pc2VcclxuICBsb29rdXBfbWVzc2FnZXMudGhlbihmdW5jdGlvbihkYXRhKXtcclxuICAgIHZhciBtZXNzYWdlX2NvdW50ID0gMDtcclxuXHJcbiAgICAvLyBNYXRjaCB0byBmaW5kIGJ1dHRvbiBsaW5rXHJcbiAgICB2YXIgcGF0dGVybiA9IG5ldyBSZWdFeHAoJyg/OjxhIGhyZWY9XCJcXFxcL21lc3NhZ2VzLW9mLXBsYXllclxcXFwvJyArIHJtdC5kYXRhLnVzZXJfZGF0YS5jdXJyZW50X3VzZXIudXNlcm5hbWUgKyAnXCI+TWVzc2FnZXNcXFxccylcXFxcKChcXFxcZCspXFxcXCknLCAnaW0nKTtcclxuICAgIHZhciBtYXRjaGVzID0gZGF0YS5tYXRjaChwYXR0ZXJuKTtcclxuICAgIGlmKG1hdGNoZXNbMV0gJiYgbWF0Y2hlc1sxXS5tYXRjaCgvXFxkKy8pKXtcclxuICAgICAgLy8gQ291bnQgYXMgaW50XHJcbiAgICAgIG1lc3NhZ2VfY291bnQgPSBtYXRjaGVzWzFdICogMTtcclxuICAgIH1cclxuXHJcbiAgICAvLyBSZW5kZXJcclxuICAgIHJtdC5yZV9tZXNzYWdlcy5yZW5kZXJNZXNzYWdlQ291bnQobWVzc2FnZV9jb3VudCk7XHJcblxyXG4gICAgLy8gTG9vcCBpZiBuZWVkZWRcclxuICAgIGlmKGxvb3Ape1xyXG4gICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uKCl7XHJcbiAgICAgICAgcm10LnJlX21lc3NhZ2VzLmdldE1lc3NhZ2VDb3VudChsb29wKTtcclxuICAgICAgfSwgcm10LnJlX21lc3NhZ2VzLmxvb2t1cF9kZWxheSk7XHJcbiAgICB9XHJcbiAgfSk7XHJcblxyXG59XHJcblxyXG5cclxuLyoqXHJcbiAqIFJlbmRlciBtZXNzYWdlIHRvIHRhcmdldFxyXG4gKiBAcGFyYW0gIHtudW1iZXJ9IG1zZ19jb3VudFxyXG4gKi9cclxucm10LnJlX21lc3NhZ2VzLnJlbmRlck1lc3NhZ2VDb3VudCA9IGZ1bmN0aW9uKG1zZ19jb3VudCl7XHJcbiAgdmFyIHRhcmdldCA9IHJtdC51aS5lbGVtcy5fcmVfbWVzc2FnZXMucXVlcnlTZWxlY3RvcignI3JtdF9fcmVfbWVzc2FnZXNfX3RhcmdldCcpO1xyXG4gIGlmKHRhcmdldCl7XHJcbiAgICAvLyBEaXNwbGF5IGNvdW50XHJcbiAgICB0YXJnZXQuaW5uZXJIVE1MID0gbXNnX2NvdW50O1xyXG5cclxuICAgIC8vIEhhbmRsZSBjbGFzc1xyXG4gICAgaWYobXNnX2NvdW50ID09PSAwKXtcclxuICAgICAgdGFyZ2V0LmNsYXNzTmFtZSA9ICcnO1xyXG4gICAgfWVsc2V7XHJcbiAgICAgIHRhcmdldC5jbGFzc05hbWUgPSAncm10X19yZV9tZXNzYWdlcy0tdW5yZWFkJztcclxuICAgIH1cclxuXHJcbiAgfVxyXG59IiwiLyoqXHJcbiAqIExhdGVzdCBUb3BpY3MgY29tcG9uZW50XHJcbiAqIEB0eXBlIHtPYmplY3R9XHJcbiAqL1xyXG5ybXQubGF0ZXN0X3RvcGljcyA9IHt9XHJcblxyXG5cclxuLyoqXHJcbiAqIEluaXRpYWxpemUgTGF0ZXN0IFRvcGljcyBVSVxyXG4gKi9cclxucm10LmxhdGVzdF90b3BpY3MuaW5pdCA9IGZ1bmN0aW9uKCl7XHJcbiAgY29uc29sZS5sb2coJ3JtdCBsYXRlc3RfdG9waWNzIGluaXQnKTtcclxuXHJcbiAgLy8gUmVuZGVyIG9wdGlvblxyXG4gIHJtdC51aS5lbGVtcy5fbGF0ZXN0X3RvcGljcyA9IHJtdC51aS5lbGVtcy5fb3B0X2xpc3QuYXBwZW5kQ2hpbGQoX3JlbmRlcih7XHJcbiAgICB0YWc6ICdsaScsXHJcbiAgICBhdHRyOiB7XHJcbiAgICAgICdpZCc6ICdybXRfX2xhdGVzdF90b3BpY3MnLFxyXG4gICAgfSxcclxuICAgIG5vZGVzOiBbXHJcbiAgICAgIHtcclxuICAgICAgICB0YWc6ICdhJyxcclxuICAgICAgICBhdHRyOntcclxuICAgICAgICAgICdocmVmJzogJy9mb3J1bS9sYXRlc3QnLFxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgbm9kZXM6IFtcclxuICAgICAgICAgIHtcclxuICAgICAgICAgICAgdGFnOiAnaDQnLFxyXG4gICAgICAgICAgICBhdHRyOiB7XHJcbiAgICAgICAgICAgICAgY2xhc3M6ICdybXRfX29wdC1oZWFkaW5nJyxcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgaHRtbDogJ0xhdGVzdCBUb3BpY3MnLFxyXG4gICAgICAgICAgfSxcclxuICAgICAgICBdLFxyXG4gICAgICB9LFxyXG4gICAgXSxcclxuICB9KSk7XHJcblxyXG59Iiwicm10LnNldHVwKCk7Il0sInNvdXJjZVJvb3QiOiIvc291cmNlLyJ9
