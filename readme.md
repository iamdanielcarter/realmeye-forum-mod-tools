#RealmEye Mod Tools

###RealmEye Messages

Pulls unread message count from RealmEye.

---

###Mod Notes

Append mod note to reply box.

####Thread Moved:

* Select a category.
* Generated string:

`***Mod note:** Moved to [{category_name}]({category_url}) {custom_msg} ~ {username}*`

####Other:

* Enter note.
* Generated string:

`***Mod note:** {preset_note} {custom_msg} ~ {username}*`

---

###Latest topics

Quick link to **Latest Topics**.