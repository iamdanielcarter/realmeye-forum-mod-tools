/**
 * Mod Note component
 * @type {Object}
 */
rmt.modnote = {

  /**
   * UI state
   */
  display_state: 0,

  /**
   * Mod note strings
   * @type {Object}
   */
  notes: {
    other: {
      string: '\n\n---\n\n***Mod note:** {$preset_note}{$note} ~ {$username}*',
    },
    thread_move: {
      string: '\n\n---\n\n***Mod note:** Moved to [{$category_name}]({$category_url}){$note} ~ {$username}*',
    },
  },

  preset_notes: [
    {
      label: 'We\'re encouraging completed and extensive ideas. Please work on it and come back!',
      note: 'We\'re encouraging completed and extensive ideas. Please work on it and come back!',
    },
    {
      label: '/thread',
      note: '&lt;/thread&gt;',
    },
    {
      label: 'Frog',
      note: '[img]http://static.drips.pw/rotmg/wiki/Pets/Demon%20Frog%20Generator.png[/img]',
    },
    {
      label: 'ಠ_ಠ',
      note: 'ಠ_ಠ',
    },
  ],

}


/**
 * Initialize Mod Note UI and events
 */
rmt.modnote.init = function(){
  console.log('rmt modnote init');

  // Simplify categories
  var _categories_simple = {};
  rmt.data.site_data.categories.forEach(function(_category){
    var data = {
      id: _category.id,
      name: _category.name,
      url: _category.topic_url,
    }
    // Apply parent ID
    if(_category.parent_category_id){
      data.parent_id = _category.parent_category_id;
    }
    // Push
    _categories_simple[_category.id] = data;
  });

  // Build category option list
  var category_options = [];
  for(var _id in _categories_simple){
    // Construct joined name
    var _name = _categories_simple[_id].name,
        no_parent = false,
        last_check = _id;
    while(!no_parent){
      if(_categories_simple[last_check].parent_id){
        // Prepend parent category name
        _name = _categories_simple[_categories_simple[last_check].parent_id].name + ' > ' + _name;
        last_check = _categories_simple[last_check].parent_id;
      }else{
        no_parent = true;
      }
    }

    // Append node
    category_options[category_options.length] = {
      tag: 'option',
      attr: {
        'data-name': _name,
        'data-url': _categories_simple[_id].url,
      },
      html: _name,
    }
  }

  // Build preset note option list
  var preset_notes = [{
    tag: 'option',
    attr: {
      'value': '',
    },
    html: 'No Preset Note',
  }];
  for(var _prop in rmt.modnote.preset_notes){
    // Append node
    preset_notes[preset_notes.length] = {
      tag: 'option',
      attr: {
        'value': rmt.modnote.preset_notes[_prop].note,
      },
      html: rmt.modnote.preset_notes[_prop].label,
    }
  }
  console.log(preset_notes);

  // Sort alphabetically
  category_options.sort(function(a, b){
    if(a.html < b.html){
      return -1;
    }
    if(a.html > b.html){
      return 1;
    }
    return 0;
  });

  // Render option
  rmt.ui.elems._modnote = rmt.ui.elems._opt_list.appendChild(_render({
    tag: 'li',
    attr: {
      'id': 'rmt__modnote',
    },
    nodes: [
      {
        tag: 'h4',
        attr: {
          class: 'rmt__opt-heading',
        },
        html: 'Add Mod Note',
        nodes: [
          {
            tag: 'span',
            attr: {
              'class': 'rmt-add',
              'title': 'Add Mod Note',
            },
          },
        ],
        events: [
          {
            type: 'click',
            listener: rmt.modnote.toggleAdd,
          },
        ],
      },
      {
        tag: 'div',
        attr: {
          'id': 'rmt__modnote__options',
          'class': 'rmt-opt-display',
        },
        css: {
          'display': 'none',
        },
        nodes: [
          {
            tag: 'form',
            attr: {
              'class': 'rmt__modnote__type',
              'action': '#',
            },
            events: [
              {
                type: 'submit',
                listener: function(e){
                  e.preventDefault();
                  return false;
                },
              },
            ],
            nodes: [
              {
                tag: 'span',
                attr: {
                  'class': 'rmt__modnote__label'
                },
                html: 'Mod Note Type:',
              },
              {
                tag: 'input',
                attr: {
                  'type': 'radio',
                  'id': 'rmt__modnote__type-select--thread-move',
                  'name': 'rmt__modnote__type-select',
                  'value': 'thread-move',
                  'class': 'rmt__modnote__type-select',
                  'checked': 'checked',
                },
              },
              {
                tag: 'label',
                attr: {
                  'class': 'rmt__modnote__type-label',
                  'for': 'rmt__modnote__type-select--thread-move',
                },
                html: 'Thread Move',
              },
              {
                tag: 'input',
                attr: {
                  'type': 'radio',
                  'id': 'rmt__modnote__type-select--other',
                  'name': 'rmt__modnote__type-select',
                  'value': 'other',
                  'class': 'rmt__modnote__type-select',
                },
              },
              {
                tag: 'label',
                attr: {
                  'class': 'rmt__modnote__type-label',
                  'for': 'rmt__modnote__type-select--other',
                },
                html: 'Other',
              },
              {
                tag: 'select',
                attr: {
                  'id': 'rmt__modnote__thread-move-loc',
                  'class': 'rmt__modnote__optional',
                },
                nodes: category_options,
              },
              {
                tag: 'select',
                attr: {
                  'id': 'rmt__modnote__preset-note',
                  'class': 'rmt__modnote__optional',
                },
                nodes: preset_notes,
              },
              {
                tag: 'input',
                attr: {
                  'type': 'text',
                  'id': 'rmt__modnote__message',
                  'placeholder': 'Mod Note...',
                },
              },,
              {
                tag: 'button',
                attr: {
                  'id': 'rmt__modnote__submit',
                },
                events: [
                  {
                    type: 'click',
                    listener: rmt.modnote.appendMessage,
                  },
                ],
                html: 'Add to Edit',
              },
            ],
          },
        ],
      },
    ],
  }));

}


/**
 * Toggle display of UI
 */
rmt.modnote.toggleAdd = function(){
  var _elem = rmt.ui.elems._modnote.querySelector('#rmt__modnote__options');
  if(!_elem){ return false; }
  if(rmt.modnote.display_state){
    rmt.ui.msg.remove();
    _slide('up', _elem, 300, 'ease-in-out', true, function(){
      rmt.modnote.display_state = !rmt.modnote.display_state;
      rmt.ui.elems._modnote.className = '';
      // Reset values
      _elem.querySelector('#rmt__modnote__message').value = '';
      rmt.ui.elems._modnote.querySelector('#rmt__modnote__preset-note').options[0].selected = true;;
      rmt.ui.elems._modnote.querySelector('#rmt__modnote__thread-move-loc').options[0].selected = true;;
    });
  }else{
    _slide('down', _elem, 300, 'ease-in-out', true, function(){
      rmt.modnote.display_state = !rmt.modnote.display_state;
      rmt.ui.elems._modnote.className = 'rmt-open';
    });
  }
}


/**
 * Append message to editor based on note type
 */
rmt.modnote.appendMessage = function(){
  // Reset msg
  rmt.ui.msg.remove();

  // Default data
  var data = {
    'username': rmt.data.user_data.current_user.username,
    'note': '',
    'preset_note': '',
  }

  // Get editor
  var target = document.querySelector('#reply-control textarea.d-editor-input');
  if(!target){
    rmt.ui.msg.set('No comment/topic editor found', 'error');
    return false;
  }

  // Message
  var msg = rmt.ui.elems._modnote.querySelector('#rmt__modnote__message').value.trim();

  // Mod note type
  switch(rmt.ui.elems._modnote.querySelector('input.rmt__modnote__type-select:checked').value){
    // Other
    case 'other':
      var preset_notes = rmt.ui.elems._modnote.querySelector('#rmt__modnote__preset-note');
      if(preset_notes.selectedIndex !== -1){
        data['preset_note'] = preset_notes.options[preset_notes.selectedIndex].value + (msg !== '' ? ' ' : '');
      }
      if(msg === '' && preset_notes.selectedIndex === 0){
        rmt.ui.msg.set('Please enter or select a note', 'error');
        return false;
      }
      data['note'] = msg;
      // Append message to target
      target.value += rmt.modnote.generateMessage(rmt.modnote.notes.other.string, data);
      break;

    // Thread move
    case 'thread-move':
      var category_list = rmt.ui.elems._modnote.querySelector('#rmt__modnote__thread-move-loc');
      if(category_list.selectedIndex === -1){
        rmt.ui.msg.set('Please select a category', 'error');
        return false;
      }
      var category = category_list.options[category_list.selectedIndex];
      data['category_name'] = category.getAttribute('data-name');
      data['category_url'] = category.getAttribute('data-url');
      // Append note
      data['note'] = (msg !== '' ? '. ' : '') + msg;
      // Append message to target
      target.value += rmt.modnote.generateMessage(rmt.modnote.notes.thread_move.string, data);
      break;
  }

  // Update target
  target.focus();
  setTimeout(function(){
    target.blur();
  }, 16);

  // Close UI
  rmt.modnote.toggleAdd();
}


/**
 * Parse template string and replace with data variables
 * @param  {string} msg_string
 * @param  {object} data
 * @return {string}
 */
rmt.modnote.generateMessage = function(msg_string, data){
  for(var _search in data){
    msg_string = msg_string.replace(new RegExp('\\{\\$' + _search + '\\}', 'gm'), data[_search]);
  }
  return msg_string;
}