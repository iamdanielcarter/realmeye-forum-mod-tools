/**
 * Latest Topics component
 * @type {Object}
 */
rmt.re_messages = {
  lookup_delay: 5000
}


/**
 * Initialize RealmEye Messages UI and events
 */
rmt.re_messages.init = function(){
  console.log('rmt re_messages init');

  // Prep data
  rmt.re_messages.url_base = '/messages-of-player/' + rmt.data.user_data.current_user.username;

  // Render option
  rmt.ui.elems._re_messages = rmt.ui.elems._opt_list.appendChild(_render({
    tag: 'li',
    attr: {
      'id': 'rmt__re_messages',
    },
    nodes: [
      {
        tag: 'a',
        attr:{
          'href': rmt.re_messages.url_base,
          'target': '_blank',
        },
        nodes: [
          {
            tag: 'h4',
            attr: {
              class: 'rmt__opt-heading',
            },
            html: 'RealmEye Messages',
            nodes: [
              {
                tag: 'span',
                attr: {
                  id: 'rmt__re_messages__target',
                },
                html: '0',
              },
            ],
          },
        ],
      },
    ],
  }));

  // Start message loop
  if(rmt.data.user_data.current_user.username){
    rmt.re_messages.getMessageCount(true);
  }

}


/**
 * Fetch message count via AJAX and HTML parse
 * @param  {boolean} loop Loop request
 */
rmt.re_messages.getMessageCount = function(loop){
  if(typeof loop === 'undefined'){
    loop = false;
  }

  // Init AJAX
  var lookup_messages = new Promise(function(resolve, reject){

    _xhr({
      url: rmt.re_messages.url_base,
      load: function(){
        if(this.status === 200){
          resolve(this.responseText);
        }else{
          if(loop){
            rmt.re_messages.getMessageCount(loop);
          }
        }
      }
    });

  });

  // Resolve promise
  lookup_messages.then(function(data){
    var message_count = 0;

    // Match to find button link
    var pattern = new RegExp('(?:<a href="\\/messages-of-player\\/' + rmt.data.user_data.current_user.username + '">Messages\\s)\\((\\d+)\\)', 'im');
    var matches = data.match(pattern);
    if(matches[1] && matches[1].match(/\d+/)){
      // Count as int
      message_count = matches[1] * 1;
    }

    // Render
    rmt.re_messages.renderMessageCount(message_count);

    // Loop if needed
    if(loop){
      setTimeout(function(){
        rmt.re_messages.getMessageCount(loop);
      }, rmt.re_messages.lookup_delay);
    }
  });

}


/**
 * Render message to target
 * @param  {number} msg_count
 */
rmt.re_messages.renderMessageCount = function(msg_count){
  var target = rmt.ui.elems._re_messages.querySelector('#rmt__re_messages__target');
  if(target){
    // Display count
    target.innerHTML = msg_count;

    // Handle class
    if(msg_count === 0){
      target.className = '';
    }else{
      target.className = 'rmt__re_messages--unread';
    }

  }
}