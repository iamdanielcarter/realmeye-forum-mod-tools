/**
 * Latest Topics component
 * @type {Object}
 */
rmt.latest_topics = {}


/**
 * Initialize Latest Topics UI
 */
rmt.latest_topics.init = function(){
  console.log('rmt latest_topics init');

  // Render option
  rmt.ui.elems._latest_topics = rmt.ui.elems._opt_list.appendChild(_render({
    tag: 'li',
    attr: {
      'id': 'rmt__latest_topics',
    },
    nodes: [
      {
        tag: 'a',
        attr:{
          'href': '/forum/latest',
        },
        nodes: [
          {
            tag: 'h4',
            attr: {
              class: 'rmt__opt-heading',
            },
            html: 'Latest Topics',
          },
        ],
      },
    ],
  }));

}