rmt.ui = {
  elems: {},
  display_state: 1
}


/**
 * Initialize UI
 */
rmt.ui.init = function(){
  console.log('rmt ui init');

  // Render main wrap
  rmt.ui.elems._wrap = document.body.appendChild(_render({
    tag: 'div',
    attr: {
      'id': 'rmt__wrap',
    },
  }));

  // Render toggle
  rmt.ui.elems._toggle_display = rmt.ui.elems._wrap.appendChild(_render({
    tag: 'span',
    attr: {
      'id': 'rmt__display-toggle',
      'title': 'Toggle RealmEye Mod Tools',
    },
    events: [
      {
        type: 'click',
        listener: rmt.ui.events.toggleDisplay,
      },
    ],
  }));

  // Render display wrap
  rmt.ui.elems._display_wrap = rmt.ui.elems._wrap.appendChild(_render({
    tag: 'div',
    attr: {
      'id': 'rmt__display-wrap',
    },
  }));

  // Render message output
  rmt.ui.elems._msg = rmt.ui.elems._display_wrap.appendChild(_render({
    tag: 'div',
    attr: {
      'id': 'rmt__msg',
    },
    css: {
      'display': 'none',
    },
  }));

  // Render opt list
  rmt.ui.elems._opt_list = rmt.ui.elems._display_wrap.appendChild(_render({
    tag: 'ul',
    attr: {
      'id': 'rmt__opt-list',
    },
  }));

  /**
   * Init options
   */
  rmt.ui.initOpts();
}


/**
 * Initialize individual options
 */
rmt.ui.initOpts = function(){
  rmt.modnote.init();
  rmt.re_messages.init();
  rmt.latest_topics.init();
}



/////////////////
// UI - EVENTS //
/////////////////
rmt.ui.events = {}

/**
 * Toggles main wrap display
 * @return {[type]} [description]
 */
rmt.ui.events.toggleDisplay = function(){
  if(rmt.ui.display_state){
    _slide('up', rmt.ui.elems._display_wrap, 300, 'ease-in-out', true, function(){
      rmt.ui.display_state = !rmt.ui.display_state;
      rmt.ui.elems._toggle_display.className = 'rmt-hidden';
    });
  }else{
    _slide('down', rmt.ui.elems._display_wrap, 300, 'ease-in-out', true, function(){
      rmt.ui.display_state = !rmt.ui.display_state;
      rmt.ui.elems._toggle_display.className = '';
    });
  }
}



//////////////
// MESSAGES //
//////////////
rmt.ui.msg = {}

/**
 * Set UI message
 * @param {string} msg
 * @param {string} type
 */
rmt.ui.msg.set = function(msg, type){
  rmt.ui.elems._msg.className = 'rmt-msg--' + type;
  rmt.ui.elems._msg.style.display = 'block';
  rmt.ui.elems._msg.innerHTML = msg;
}

/**
 * Remove and reset UI message
 */
rmt.ui.msg.remove = function(){
  rmt.ui.elems._msg.className = '',
  rmt.ui.elems._msg.style.display = 'none';
  rmt.ui.elems._msg.innerHTML = '';
}