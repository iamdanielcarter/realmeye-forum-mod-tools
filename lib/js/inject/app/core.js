var rmt = {};

rmt.init = function(){
  console.log('rmt init');
  console.log(rmt.data);
  rmt.ui.init();
}

/**
 * Get required data and init on ready
 */
rmt.setup = function(){
  // Data obj
  var apiData = {
    site_data: false,
    user_data: false,
  }

  // Get site data
  var site_data = new Promise(function(resolve, reject){

    _xhr({
      url: '/forum/site.json',
      load: function(){
        var data = JSON.parse(this.responseText);
        if(this.status === 200 && typeof data === 'object'){
          resolve(data);
        }
      }
    });

  });

  // Get user data
  var user_data = new Promise(function(resolve, reject){

    _xhr({
      url: '/forum/session/current.json',
      load: function(){
        var data = JSON.parse(this.responseText);
        if(this.status === 200 && typeof data === 'object'){
          resolve(data);
        }
      }
    });

  });

  // Resolve AJAX promises
  site_data.then(function(data){
    apiData.site_data = data;
    start();
  });
  user_data.then(function(data){
    apiData.user_data = data;
    start();
  });
  var start = function(){
    if(apiData.site_data && apiData.user_data){
      // Set data and init when both data sets are returned
      rmt.data = apiData;
      rmt.init();
    }
  }

}