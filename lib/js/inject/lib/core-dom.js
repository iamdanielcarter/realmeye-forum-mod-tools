////////////////
// CORE - DOM //
////////////////


/**
 * DOM object renderer with event hooks
 * @param  {object} map DOM map tag*|attr|css|html|events|nodes
 * @return {object}     DOM object for append/manipulation
 */
function _render(map){
  var build = function(map){
    var elem,
        css;

    // Init
    if(!map.tag){
      return false;
    }
    if(map.ns){
      // Create element with namespace
      elem = document.createElementNS(map.ns, map.tag);
    }else{
      elem = document.createElement(map.tag);
    }

    // Attributes
    if(typeof map.attr === 'object'){
      for(var attribute in map.attr){
        elem.setAttribute(attribute, map.attr[attribute]);
      }
    }

    // CSS
    if(typeof map.css === 'object'){
      css = '';
      for(var prop in map.css){
        css += prop + ': ' + map.css[prop] + '; ';
      }
      elem.setAttribute('style', css);
    }

    // Inner HTML
    if(map.html){
      elem.innerHTML = map.html;
    }

    // Events
    if(typeof map.events === 'object'){
      map.events.forEach(function(_event){
        var c = function(e){
          _event.listener.call(elem, e);
        }
        elem.addEventListener(_event.type, c);
      });
    }

    // Children
    if(map.nodes){
      map.nodes.forEach(function(node){
        var child = build(node);
        elem.appendChild(child);
      });
    }

    return elem;
  }

  // Return rendered element DOM tree
  return build(map);
}