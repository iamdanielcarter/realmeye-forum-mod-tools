///////////////////////
// CORE - ANIMATIONS //
///////////////////////


/**
 * Linear progression
 * @param  {number} t Time elapsed
 * @param  {number} b Starting value
 * @param  {number} c Start end difference
 * @param  {number} d Duration
 * @return {number}   Delta updated value
 */
Math.linearTween = function(t, b, c, d){
  return c*t/d + b;
};


/**
 * Cubic ease-in progression
 * @param  {number} t Time elapsed
 * @param  {number} b Starting value
 * @param  {number} c Start end difference
 * @param  {number} d Duration
 * @return {number}   Delta updated value
 */
Math.easeIn = function(t, b, c, d){
  t /= d;
  return c*t*t*t + b;
};


/**
 * Cubic ease-out progression
 * @param  {number} t Time elapsed
 * @param  {number} b Starting value
 * @param  {number} c Start end difference
 * @param  {number} d Duration
 * @return {number}   Delta updated value
 */
Math.easeOut = function(t, b, c, d){
  t /= d;
  t--;
  return c*(t*t*t + 1) + b;
};


/**
 * Cubic ease-in-out progression
 * @param  {number} t Time elapsed
 * @param  {number} b Starting value
 * @param  {number} c Start end difference
 * @param  {number} d Duration
 * @return {number}   Delta updated value
 */
Math.easeInOut = function(t, b, c, d){
  t /= d/2;
  if (t < 1) return c/2*t*t*t + b;
  t -= 2;
  return c/2*(t*t*t + 2) + b;
};


/**
 * Fade in/out with duration and callback settings
 * @param  {string}   direction in|out
 * @param  {object}   element   DOM node
 * @param  {number}   duration  Animation in ms
 * @param  {string}   easing    Easing timing function linear|easeIn|easeOut|easeInOut
 * @param  {function} fn        Callback (with element as scope)
 * @return {boolean}
 */
function _fade(direction, element, duration, easing, fn){
  var FROM,
      TO,
      START = new Date().getTime(),
      DIFF,
      modShift,
      callback;

  // Prevent propagation
  if(element.getAttribute('data-fade')){
    return false;
  }
  element.setAttribute('data-fade', true);

  // Easing timing function
  switch(easing){
    case 'linear':
      easing = Math.linearTween;
      break;

    case 'easeIn':
      easing = Math.easeIn;
      break;

    case 'easeInOut':
      easing = Math.easeInOut;
      break;

    default: // easeOut
      easing = Math.easeOut;
      break;
  }

  // Set default opacity
  if(!element.style.opacity){
    element.style.opacity = (direction == 'in' ? 0 : 1);
  }

  // Directional defaults
  FROM = 1;
  TO   = 0;
  if(direction != 'out'){
    direction             = 'in';
    element.style.display = 'block';
    FROM                  = 0;
    TO                    = 1;
  }
  DIFF = TO - FROM;

  // Duration
  if(typeof duration !== 'number'){
    duration = 300;
  }
  duration = Math.abs(duration);

  // Callback
  callback = function(){
    element.removeAttribute('data-fade');
    if(typeof fn === 'function'){
      fn.call(element);
    }
  }

  // Modifier
  modShift = function(){
    // Update values
    var CURRENT = new Date().getTime() - START,
        UPDATE  = easing(CURRENT, FROM, DIFF, duration);
    element.style.opacity = UPDATE;

    if(UPDATE == TO || CURRENT >= duration){
      // Done
      if(direction == 'out'){
        // Hide on fade out
        element.style.display = 'none';
      }

      // Reset transitional CSS
      element.style.opacity = '';

      // End
      callback();
      return true;
    }
    requestAnimationFrame(modShift);
  }

  // Trigger
  modShift();
}


/**
 * Slide up/down with duration and callback settings
 * @param  {string}   direction up|down
 * @param  {object}   element   DOM node
 * @param  {number}   duration  Animation in ms
 * @param  {string}   easing    Easing timing function linear|easeIn|easeOut|easeInOut
 * @param  {function} fn        Callback (with element as scope)
 * @return {boolean}
 */
function _slide(direction, element, duration, easing, display_remove, fn){
  var FROM,
      TO,
      START = new Date().getTime(),
      DIFF,
      modShift,
      callback;

  // Prevent propagation
  if(element.getAttribute('data-slide')){
    return false;
  }
  element.setAttribute('data-slide', true);

  // Set initial css
  if(display_remove){
    element.style.display  = 'block';
  }
  element.style.overflow = 'hidden';
  element.style.height   = (direction == 'up' ? element.scrollHeight + 'px' : 0 + 'px');

  // Directional defaults
  FROM = element.scrollHeight;
  TO   = 0;
  if(direction != 'up'){
    direction = 'down';
    FROM      = 0;
    TO        = element.scrollHeight;
  }
  DIFF = TO - FROM;

  // Duration
  if(typeof duration !== 'number'){
    duration = 300;
  }
  duration = Math.abs(duration);

  // Easing timing function
  switch(easing){
    case 'linear':
      easing = Math.linearTween;
      break;

    case 'easeIn':
      easing = Math.easeIn;
      break;

    case 'easeInOut':
      easing = Math.easeInOut;
      break;

    default: // easeOut
      easing = Math.easeOut;
      break;
  }

  // Callback
  callback = function(){
    element.removeAttribute('data-slide');
    if(typeof fn === 'function'){
      fn.call(element);
    }
  }

  // Modifier
  modShift = function(){
    // Update values
    var CURRENT = new Date().getTime() - START,
        UPDATE  = easing(CURRENT, FROM, DIFF, duration);
    element.style.height = UPDATE + 'px';

    if(UPDATE == TO || CURRENT >= duration){
      // Done
      if(direction == 'up'){
        // Hide on slide up
        if(display_remove){
          element.style.display = 'none';
        }
      }

      // Reset transitional CSS
      element.style.overflow = '';
      if(display_remove){
        element.style.height = '';
      }

      // End
      callback();
      return true;
    }
    requestAnimationFrame(modShift);
  }

  // Trigger
  modShift();
}


/**
 * Scroll window to destination with duration and callback settings
 * @param  {mixed}    destination Scroll to pixel value, selector or DOM node
 * @param  {number}   duration    Animation in ms
 * @param  {string}   easing      Easing timing function linear|easeIn|easeOut|easeInOut
 * @param  {function} fn          Callback function
 * @return {boolean}
 */
function _scrollTo(destination, duration, easing, fn){
  var FROM,
      TO,
      START = new Date().getTime(),
      DIFF,
      modShift,
      callback,
      element = document.body;

  // Prevent propagation
  if(element.getAttribute('data-scrolling')){
    return false;
  }
  element.setAttribute('data-scrolling', true);

  // Destination
  switch(typeof destination){
    case 'number':
      destination = destination;
      break;

    case 'string':
      var elem = document.querySelector(destination);
      if(!elem){ return false; }
      destination = elem.getBoundingClientRect().top;
      break;

    case 'object':
      if(!destination.nodeType){ return false; }
      destination = destination.getBoundingClientRect().top;
      break;

    default:
      return false;
  }

  // Process below max fold
  var _body   = document.body,
      _html   = document.documentElement,
      _height = Math.max(_body.scrollHeight, _body.offsetHeight, _html.clientHeight, _html.scrollHeight, _html.offsetHeight);
  destination = Math.max(0, Math.min(destination, _height - window.innerHeight));

  // Directional defaults
  FROM = window.scrollY;
  TO   = destination;
  DIFF = TO - FROM;

  // Duration
  if(typeof duration !== 'number'){
    duration = 1000;
  }
  duration = Math.abs(duration);

  // Easing timing function
  switch(easing){
    case 'linear':
      easing = Math.linearTween;
      break;

    case 'easeIn':
      easing = Math.easeIn;
      break;

    case 'easeInOut':
      easing = Math.easeInOut;
      break;

    default: // easeOut
      easing = Math.easeOut;
      break;
  }

  // Callback
  callback = function(){
    element.removeAttribute('data-scrolling');
    if(typeof fn === 'function'){
      fn.call(element);
    }
  }

  // Modifier
  modShift = function(){
    // Update values
    var CURRENT = new Date().getTime() - START,
        UPDATE  = easing(CURRENT, FROM, DIFF, duration);
    window.scrollTo(window.scrollX, UPDATE);

    if(UPDATE == TO || CURRENT >= duration){
      // Done
      callback();
      return true;
    }
    requestAnimationFrame(modShift);
  }

  // Trigger
  modShift();
}